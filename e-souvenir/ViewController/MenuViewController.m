//
//  ViewController.m
//  e-souvenir
//
//  Created by Dynseo on 18/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "MenuViewController.h"createProfilToDababase
#import "DatabaseManager.h"
#import "AppDelegate.h"
#import "SynchronisationView.h"
#import "SynchronisationResources.h"
#import "SynchronisationData.h"
#import "Synchronisation.h"
#import "SouvenirInAppPurchaseHelper.h"
#import "DDCoachMarksView.h"
#import "SettingViewController.h"
#import "URLFactory.h"

#define kIdentifyDeviceTimeOut 5
#define kDefaultTimeOut 60
#define kMaxProfileNumberForIndividualAccount 3
#define kUserDefaultsKeySubscriptionPurchaseNotSentToServerProductId @"subscription_not_sent_to_server_product_id"

@interface MenuViewController () {
    DatabaseManager * databaseManager;
    AFHTTPRequestOperationManager * requestManager;
    
    int indexProfile;
    
    int currentStep;
    int STEP_START;
    int STEP_VERIFICATION;
    int STEP_VERIFICATION_SUCCESS;
    int STEP_SUBSCRIPTION;
    int STEP_IDENTIFY_DEVICE;
    int STEP_IDENTIFY_DEVICE_SUCCESS;
    int STEP_IDENTIFY_DEVICE_FAILURE;
    int STEP_CREATE_ACCOUNT_SUCCESS;
    int STEP_LOGIN_SUCCESS;
    int STEP_SYNCHRO;
    int STEP_SIGN_UP_OR_CONNECT;
    int STEP_VISIT;
    int STEP_ALL_DONE;
    int STEP_QUIT_ON_SUBSCRIPTION_EXPIRED;
    
    UIImageView * backgroundImage;
    UIButton * btnCreateProfil;
    DimensionAdapter * dimensionAdapter;
    NSString *stringMenuButtonColorVeryLight, *stringMenuButtonColorLight, *stringMenuButtonColorDark;
    NSString * currentDateFormatDatebase;
    ActionSheetDatePicker * pickerDate;
    NSArray * allPersons;
    BOOL isSynchronizing;
    int numberPlayersToSend;
    int countPlayerToSend;
    BOOL notificationLaunched;
    Synchronisation * synchro;
}

@end

@implementation MenuViewController

@synthesize dialogIdentification, dialogViewCreateProfil, viewCreateProfile, buttonCreateProfileSave, buttonCreateProfileCancel, labelCreateProfile, sexSegmentedControl, firstnameTextField, nameTextField, dateOfBirthTextField, dialogChooseProfile, pickerProfile,dialogAskForAccountCreation, dialogCreateOrConnectInstitution,dialogCreateInstitution, synchronisation, dialogSetting, buttonPurchase, dialogGameBlocked, dialogAboutUs, buttonChooseProfil, buttonCreateProfil, buttonTuto,dialogCreateProfessional;

- (void)viewDidLoad {
    [super viewDidLoad];
    // for test bugsnag
    //[Bugsnag addAttribute:@"Serial Number" withValue:[FCUUID uuidForDevice] toTabWithName:@"User"];
    //[Bugsnag notify:[NSException exceptionWithName:@"ExceptionName" reason:@"Test Error" userInfo:nil]];

    
    
    BASE_URL = @"https://www.stimart.com/app?newPilotSession=true";
    dimensionAdapter = [[DimensionAdapter alloc] initDimensionAdapter];
    currentStep = 0;
    STEP_START = 0;
    STEP_VERIFICATION = 1;
    STEP_VERIFICATION_SUCCESS = 10;
    STEP_CREATE_ACCOUNT_SUCCESS = 13;
    STEP_LOGIN_SUCCESS = 13;
    STEP_SYNCHRO = 2;
    STEP_SIGN_UP_OR_CONNECT = 3;
    STEP_SUBSCRIPTION = 7;
    STEP_IDENTIFY_DEVICE = 8;
    STEP_IDENTIFY_DEVICE_SUCCESS = 11;
    STEP_IDENTIFY_DEVICE_FAILURE = 12;
    STEP_VISIT = 5;
    STEP_ALL_DONE = 6;
    STEP_QUIT_ON_SUBSCRIPTION_EXPIRED = 20;
    [self.buttonPurchase setHidden:YES];
    [self loadParams];
    [self setupCreateProfileViewContent];
    [self manageLaunchSteps];
}

- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear");
//    if ([Account isVisitMode] || ![Subscription isSubscriptionValid]) {
//        [self.buttonPurchase setHidden:NO];
//    }
//    else {
//        ((AppDelegate *)[[UIApplication sharedApplication]delegate]).onReturn = ON_RETURN_AUTHORIZED;
//    }
//    
//    if (((AppDelegate *)[[UIApplication sharedApplication]delegate]).onReturn == ON_RETURN_NOT_AUTHORIZED) {
//        
//        [self showDialogBlocked];
//    }
//    else {
//        [self manageLaunchSteps];
//    }
    if ([Account isVisitMode] || ![Subscription isSubscriptionValid]) {
        [self.buttonPurchase setHidden:NO];
    }
}

/* show this dialog when game blocked (visit/test mode) */
- (void)showDialogBlocked
{
    NSLog(@"showDialogGameBlocked");
    if (!dialogGameBlocked) {
        dialogGameBlocked = [[STMDialogView alloc]initWithType:STMDialogTypeTextWithTwoButtons andStyle:2 andBackground:@"dialog.png"];
        
        NSString *stringTitle;
        NSString *stringTextView;
        NSString *titleButton1;
        NSString *titleButton2;
        
        stringTitle = NSLocalizedString(@"your_visit", nil);
        stringTextView = NSLocalizedString(@"visit_mode_text", nil);
        titleButton1 = NSLocalizedString(@"return", nil);
        titleButton2 = NSLocalizedString(@"buy", nil);
        [dialogGameBlocked setTarget:self forButton:1 action:@selector(hideDialogGameBlockedAction:)];
        [dialogGameBlocked setTarget:self forButton:2 action:@selector(buyAction:)];
        [dialogGameBlocked setTitleUpText:stringTitle];
        [dialogGameBlocked setTextViewText:stringTextView];
        [dialogGameBlocked.textView setTextAlignment:NSTextAlignmentJustified];
        
        [dialogGameBlocked setupButtonFor:1 andText:titleButton1 andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
        [dialogGameBlocked setupButtonFor:2 andText:titleButton2 andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
        
        
        
        [self.view addSubview:dialogGameBlocked];
    }
    else {
        [dialogGameBlocked setHidden:NO];
        [self.view bringSubviewToFront:dialogGameBlocked];
    }
}

- (void)hideDialogGameBlockedAction:(id)sender
{
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if (dialogGameBlocked) {
        [dialogGameBlocked setHidden:YES];
    }
    if (dialogIdentification) {
        [dialogIdentification setHidden:YES];
    }
}


- (void)loadParams
{
    /* index of profile of current player */
    countPlayerToSend = 0;
    numberPlayersToSend = 0;
    isSynchronizing = NO;
    indexProfile = -1;
    stringMenuButtonColorLight = NSLocalizedString(@"menu_light_button_code", nil);
    stringMenuButtonColorDark = NSLocalizedString(@"menu_dark_button_code", nil);
    
    databaseManager = [DatabaseManager getSharedInstance];
    NSLog(@"created bdd");

    requestManager = [AFHTTPRequestOperationManager manager];
    requestManager.responseSerializer.acceptableContentTypes = [requestManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    //If the list of products is not inited, request Apple Store (only once)
    if( ![[SouvenirInAppPurchaseHelper sharedHelper] isProductsListInited ] ) {
        [[SouvenirInAppPurchaseHelper sharedHelper] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
            if (success) {
                NSLog(@"size products : %lu", (unsigned long)products.count);
                [[SouvenirInAppPurchaseHelper sharedHelper] setRealProductPrices];
            }
        }];
    }
    [buttonChooseProfil.layer setCornerRadius:buttonChooseProfil.frame.size.height*0.5];
    [buttonCreateProfil.layer setCornerRadius:buttonChooseProfil.frame.size.height*0.5];
}


-(void) manageLaunchSteps {
    NSLog(@"MenuViewController - manageLaunchSteps");
    NSLog(@"currentStep : %d", currentStep);
    BOOL doDeviceVerify = false;
    BOOL doIdentify = false;
    BOOL doSubscription = false;
    BOOL doSubscriptionEnd = false;
    BOOL doResources = false;

    
    // ------- Determine what you have to do
    
    if (currentStep == STEP_START) {
        doDeviceVerify = true;
    }
    
    // exist account
    else if (currentStep == STEP_VERIFICATION_SUCCESS) {
        doResources = true;
    }
    
    else if (currentStep == STEP_IDENTIFY_DEVICE) {
        doIdentify = true;
    }
    
    else if (currentStep == STEP_IDENTIFY_DEVICE_SUCCESS) {
        if (![Subscription isSubscriptionValid])
            doSubscription = true;
        else {
            doResources = true;
        }
//        doResources = true;
    }
    
    else if (currentStep == STEP_IDENTIFY_DEVICE_FAILURE) {
        NSLog(@"identifyDevice - device unknown");
        if (dialogIdentification == nil || [dialogIdentification isHidden]) {
            [self showDialogIdentification];
            dialogIdentification.isLaunchDialog = YES;
        }
        return;
    }
    
    else if (currentStep == STEP_VISIT) {
        doResources = true;
    }
    
    else if (currentStep == STEP_LOGIN_SUCCESS) {
        if (![Subscription isSubscriptionValid]) {
            doSubscription = true;
        }
        else {
            doResources = true;
        }
    }
    
    else if (currentStep == STEP_CREATE_ACCOUNT_SUCCESS) {
        doResources = true;
    }
    
    else if (currentStep == STEP_SUBSCRIPTION) {
        doSubscription = true;
    }
    
    else if (currentStep == STEP_SIGN_UP_OR_CONNECT) {
        NSLog(@"create an account");
        if (dialogIdentification == nil || [dialogIdentification isHidden]) {
            [self showDialogAskForAccountCreation];
            return;
        }
    }
    
    else if (currentStep == STEP_SYNCHRO) {
        doResources = true;
    }
    
    else if (currentStep == STEP_QUIT_ON_SUBSCRIPTION_EXPIRED) {
        doSubscriptionEnd = true;
    }
    
    // ------- Now execute what you have to do
    
    if (doDeviceVerify) {
        [self verifyDevice];
    }
    
    else if (doIdentify) {
        [self identifyDevice];
    }
    
    else if (doSubscription) {
        [self verifySubscription];
    }
    
    else if (doSubscriptionEnd) {
        [self showDialogBlocked];
    }
    
    else {
        BOOL okResources = true;
        if (doResources) {
            okResources = [self testResources];
        }
        if (okResources) {
            currentStep = STEP_ALL_DONE;
            if ([STMUtilities isSynchronizationNeeded]) {
                [self synchronizeAll];
            }
        }
    }
}

- (void) verifyDevice {
    NSLog(@"verifyDevice");
    BOOL isInternetConnected = [STMUtilities isInternetConnected];
    if ([Account isAccountExists]) {
        NSLog(@"account exists");
        if (![Subscription isSubscriptionValid]) {
            NSLog(@"subscription invalid, check from server");
            currentStep = STEP_SUBSCRIPTION;
        }
        else {
            if ([Account isVisitorAccount]) {
                if (isInternetConnected) {
                    currentStep = STEP_SIGN_UP_OR_CONNECT;
                }
            }
            else {
                currentStep = STEP_VERIFICATION_SUCCESS;
            }
        }
    }
    else {
        if ([Subscription getDateEnd] != nil) {
            if ([Subscription isSubscriptionValid]) {
                if (isInternetConnected) {
                    currentStep = STEP_SIGN_UP_OR_CONNECT;
                }
            }
            else {
                NSLog(@"subscription invalid, check from server");
                currentStep = STEP_SUBSCRIPTION;
            }
        }
        else {
            currentStep = STEP_IDENTIFY_DEVICE;
        }
    }
    
    [self manageLaunchSteps];
}

- (void) identifyDevice {
    NSLog(@"identityDevice");
    if ([STMUtilities getFirstLaunchDate] == nil) {
        [STMUtilities setFirstLaunchDateWithCurrentDate];
    }
    requestManager.requestSerializer.timeoutInterval = kIdentifyDeviceTimeOut;
    NSString * requestURL = [STMURLFactory getURLVerifyDevice];
    [requestManager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
        NSLog(@"identifyDevice - server response for device identification: %@", responseObject);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        bool isServerResponseUnknown = YES;
        /* a JSON element should be a NSDictionary but not a NSString */
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            
            if ([responseObject objectForKey:kJsonParamInstitutionName] != nil || [responseObject objectForKey:kJsonParamSubscriptionDateEnd] != nil) {
                isServerResponseUnknown = NO;
                // store info from server
                if (!synchro) {
                    synchro = [[Synchronisation alloc] initSynchronisation:NO];
                }
                [synchro updateSubscriptionAndAccountInfoWithJsonString:responseObject];
                // if this tablet is saved in server, we do synchro.
                [self synchronizeAll];
                
                
                NSLog(@"STEP_IDENTIFY_DEVICE_SUCCESS");
                currentStep = STEP_IDENTIFY_DEVICE_SUCCESS;
            }
        }
        /* device unknown */
        if (isServerResponseUnknown) {
            currentStep = STEP_IDENTIFY_DEVICE_FAILURE;
        }
        [self manageLaunchSteps];
    } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
        NSLog(@"ERROR - identifyDevice error = %@, for URL: %@", error, requestURL);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([Account isVisitMode]) {
            if (dialogIdentification == nil || [dialogIdentification isHidden]) {
                currentStep = STEP_IDENTIFY_DEVICE_FAILURE;
                [self manageLaunchSteps];
            }
        }
    }];
    NSLog(@"identifyDevice - timeout reset to default value");
    requestManager.requestSerializer.timeoutInterval = kDefaultTimeOut;
}

/* verify subscription information of the device */
- (void)verifySubscription
{
    NSLog(@"verifySubscription");
    NSString * requestURL = [STMURLFactory getURLVerifySubscription];
    [self showHudWithString:nil];
    [requestManager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
        NSLog(@"verifySubscription - server response for device identification: %@", responseObject);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        bool isServerResponseUnknown = YES;
        /* a JSON element should be a NSDictionary but not a NSString */
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if ([responseObject objectForKey:kJsonParamSubscriptionDateEnd] != nil) {
                isServerResponseUnknown = NO;
                if (!synchro) {
                    synchro = [[Synchronisation alloc] initSynchronisation:NO];
                }
                [synchro updateSubscriptionAndAccountInfoWithJsonString:responseObject];
                if (![Subscription isSubscriptionValid]) {
                    currentStep = STEP_QUIT_ON_SUBSCRIPTION_EXPIRED;
                }
                else {
                    currentStep = STEP_SYNCHRO;
                }
            }
        }
        if (isServerResponseUnknown) {
            NSLog(@"ERROR - verifySubscription failed, for URL: %@", requestURL);
            currentStep = STEP_QUIT_ON_SUBSCRIPTION_EXPIRED;
            //[self showDialogErrors:kErrorSubscriptionExpired];
        }
        [self manageLaunchSteps];
        
    } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
        NSLog(@"ERROR - identifyDevice error = %@, for URL: %@", error, requestURL);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        currentStep = STEP_QUIT_ON_SUBSCRIPTION_EXPIRED;
        //[self showDialogErrors:kErrorSubscriptionExpired];
        [STMUtilities showAlertDialogWithMessageKey:NSLocalizedString(@"internet_message", nil) andDuration:2];
        //[dialogDownloadResources setHidden:YES];
        [self manageLaunchSteps];
    }];
    
}



- (BOOL) testResources {
    NSLog(@"testResources");
    if (![SynchronisationResources hasResources]) {
        [self synchroResourcesOnAuto];
    }
    else {
        NSString * currentVersion = [[NSUserDefaults standardUserDefaults] objectForKey:@"versionNumber"];
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSLog(@"current version : %@", currentVersion);
        NSLog(@"last version : %@", version);
        if (![currentVersion isEqualToString:version]) {
            [self synchroResourcesOnAuto];
        }
        else {
            return true;
        }
    }
    return false;
}

- (void) synchroResourcesOnAuto {
    NSLog(@"synchroResourcesOnAuto");
    synchronisation = [[SynchronisationView alloc] initSynchronisationViewOnAuto : NO];
    [self.view addSubview:synchronisation];
    [self.view bringSubviewToFront:synchronisation];

}

- (void) synchronizeAll {
    NSLog(@"Resources");
    synchro = [[SynchronisationData alloc] initSynchronisationData];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [(SynchronisationData *)synchro startSynchronisationData:^(BOOL success){
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (success) {
                    NSLog(@"success");
                }
                else {
                    NSLog(@"failure");
                }
            });
        }];
    });
}


/* when user click the button "visit stimart" */
- (void)beginVisitMode
{
    NSLog(@"beginVisitMode");
    if ( ![Account isVisitMode] && ![Account isValidUserWithoutAccount] ) {
        [Account setVisitMode:YES];
    }
    [dialogIdentification setHidden:YES];
    currentStep = STEP_VISIT;
    [self manageLaunchSteps];
}

/* show the identification dialog */
- (void)showDialogIdentification
{
    NSLog(@"showDialogIdentification");
    if (!dialogIdentification) {
        dialogIdentification = [[DialogIdentification alloc]initWithStyle:2];
        [self.view addSubview:dialogIdentification];
        dialogIdentification.aDelegate = self;
    }
    else {
        [dialogIdentification changeToPageMain];
        [dialogIdentification setHidden:NO];
        [self.view bringSubviewToFront:dialogIdentification];
    }
}

// dialog for create new profil
/* init the buttons, segmentedControl, textFields on the create profile view */
- (void)setupCreateProfileViewContent
{
    NSLog(@"setupCreateProfileViewContent");
    [buttonCreateProfileCancel setFrame:buttonCreateProfileCancel.frame andType:STMButtonTypeRounded andTitle:NSLocalizedString(@"cancel", nil) andTitleSize:dimensionAdapter.sizey30 andBackgroundColor:stringMenuButtonColorDark andHighlightColor:stringMenuButtonColorLight];
    [buttonCreateProfileSave setFrame:buttonCreateProfileSave.frame andType:STMButtonTypeRounded andTitle:NSLocalizedString(@"save", nil) andTitleSize:dimensionAdapter.sizey30 andBackgroundColor:stringMenuButtonColorDark andHighlightColor:stringMenuButtonColorLight];
    CGRect frame1 = sexSegmentedControl.frame;
    CGRect frame2 = firstnameTextField.frame;
    CGRect frame3 = nameTextField.frame;
    CGRect frame4 = dateOfBirthTextField.frame;
    /* resize UISegmentedControl width */
    
    [sexSegmentedControl setFrame:CGRectMake(frame1.origin.x, frame1.origin.y, frame1.size.width, dimensionAdapter.sizey40)];

    [firstnameTextField setFrame:CGRectMake(frame2.origin.x, frame2.origin.y, frame2.size.width, dimensionAdapter.sizey40)];
    [nameTextField setFrame:CGRectMake(frame3.origin.x, frame3.origin.y, frame3.size.width, dimensionAdapter.sizey40)];
    
    [dateOfBirthTextField setFrame:CGRectMake(frame4.origin.x, frame4.origin.y, frame4.size.width, dimensionAdapter.sizey40)];
    UIFont *font = [UIFont boldSystemFontOfSize:dimensionAdapter.sizey15];
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
    /* resize the UISegmentedControl font */
    [sexSegmentedControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
    
    [sexSegmentedControl setTitle:NSLocalizedString(@"woman", nil) forSegmentAtIndex:0];
    [sexSegmentedControl setTitle:NSLocalizedString(@"man", nil) forSegmentAtIndex:1];
    [sexSegmentedControl setTitle:NSLocalizedString(@"noWantToAnswer", nil) forSegmentAtIndex:2];
    
    [sexSegmentedControl addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventValueChanged];
    
    firstnameTextField.placeholder = NSLocalizedString(@"first_name", nil);
    nameTextField.placeholder = NSLocalizedString(@"name", nil);
    dateOfBirthTextField.placeholder = NSLocalizedString(@"your_date_of_birth", nil);
    firstnameTextField.keyboardType = UIKeyboardTypeASCIICapable;
    nameTextField.keyboardType = UIKeyboardTypeASCIICapable;
    
    [nameTextField addTarget:self action:@selector(moveViewWhenTypeBegins) forControlEvents:UIControlEventEditingDidBegin];
    [nameTextField addTarget:self action:@selector(moveViewWhenTypeEnds) forControlEvents:UIControlEventEditingDidEnd];
    firstnameTextField.font = [UIFont systemFontOfSize:dimensionAdapter.sizey20];
    nameTextField.font = [UIFont systemFontOfSize:dimensionAdapter.sizey20];
    dateOfBirthTextField.font = [UIFont systemFontOfSize:dimensionAdapter.sizey20];
    labelCreateProfile.font = [UIFont boldSystemFontOfSize:dimensionAdapter.sizey50];
    [labelCreateProfile setText:NSLocalizedString(@"new_profile", nil)];
    [viewCreateProfile setHidden:YES];
    NSLog(@"dimen : %f", dimensionAdapter.sizey50);
}


- (void)moveViewWhenTypeBegins {
    self.viewCreateProfile.center = CGPointMake(self.viewCreateProfile.center.x, self.viewCreateProfile.center.y - dimensionAdapter.sizey100);
}

- (void)moveViewWhenTypeEnds {
    self.viewCreateProfile.center = CGPointMake(self.viewCreateProfile.center.x, self.viewCreateProfile.center.y + dimensionAdapter.sizey100);
}

- (void)switchAction
{
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
}

- (IBAction)createProfilToDababase:(id)sender {
    NSLog(@"createProfilToDababase");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    NSString * firstName = [firstnameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString * name = [nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString * birthday = [dateOfBirthTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([firstName isEqualToString:@""]) {
        [STMUtilities showAlertDialogWithMessage:NSLocalizedString(@"please_enter_your_firstname", nil) andDuration:3];
    }
    else if ([name isEqualToString:@""]) {
        [STMUtilities showAlertDialogWithMessage:NSLocalizedString(@"please_enter_your_name", nil) andDuration:3];
    }
    else if ([databaseManager isExistPersonWithFirstName:firstName andName:name]) {
        [STMUtilities showAlertDialogWithMessage:NSLocalizedString(@"profile_exists", nil) andDuration:3];
    }
//    else if ([birthday isEqualToString:@""]) {
//        [STMUtilities showAlertDialogWithMessage:NSLocalizedString(@"please_choose_your_birth_date", nil) andDuration:3];
//    }
    else {
        NSString * sex = nil;
        if (sexSegmentedControl.selectedSegmentIndex == 0)
            sex = kWoman;
        else if (sexSegmentedControl.selectedSegmentIndex == 1)
            sex = kMan;
        
        Person * currentPlayer = [[Person alloc]initWithFirstName:firstName name:name birthdate:currentDateFormatDatebase sex:sex role:@"P"];
        ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson = currentPlayer;
        
        if ([databaseManager insertPerson:currentPlayer]) {
            indexProfile = [databaseManager getNumPersons] - 1;
            NSLog(@"create profil reussi");
            viewCreateProfile.hidden = YES;
            [self synchronizeAll];
            [self startToHomeViewController];
        }
    }
}
    
- (IBAction)cancelCreateProfil:(id)sender {
    NSLog(@"cancelCreateProfileAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [viewCreateProfile setHidden:YES];
    /* disable text fields to avoir strange appearence of keyboard */
    [dateOfBirthTextField setEnabled:NO];
    [firstnameTextField setEnabled:NO];
    [nameTextField setEnabled:NO];
}

/* show date picker when we click on the dateOfBirthTextField */
- (IBAction)showDatePicker:(id)sender
{
    NSLog(@"showDatePicker");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    /* disable the UITextField disable the keyboard */
    [dateOfBirthTextField setEnabled:NO];
    NSDate * date;
    if (pickerDate) {
        date = pickerDate.selectedDate;
    }
    else {
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"MM-dd-yyyy"];
        date = [formatter dateFromString:@"01-01-1930"];
    }
    pickerDate = [ActionSheetDatePicker showPickerWithTitle:nil datePickerMode:UIDatePickerModeDate  selectedDate:date target:self action:@selector(birthDateSelectedAction:) origin:sender];
    [pickerDate setCancelTarget:@selector(cancelSelectionBirthDateAction:)];
}

- (IBAction)chooseProfilAction:(id)sender {
    [self manageProfile];
}

/* when user click on the button "Create Profile" of the home page */
- (IBAction)createProfilAction:(id)sender {
    NSLog(@"createProfileAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if ([Subscription isIndividualSubscription] && [databaseManager getNumPersons] >= kMaxProfileNumberForIndividualAccount) {
        [STMUtilities showAlertDialogWithMessageKey:@"max_profile_created" andDuration:3];
    }
    else {
        [viewCreateProfile setHidden:NO];
        [self.view bringSubviewToFront:viewCreateProfile];
        /* enable the text field */
        if (dateOfBirthTextField) {
            [dateOfBirthTextField setEnabled:YES];
            [firstnameTextField setEnabled:YES];
            [nameTextField setEnabled:YES];
        }
    }
}

- (IBAction)startTutoViewController:(id)sender {
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(570, 260, 262, 262)],
                                @"caption": NSLocalizedString(@"menuTutoText1", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(190, 260, 262, 262)],
                                @"caption": NSLocalizedString(@"menuTutoText2", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(930, 16, 78, 78)],
                                @"caption": NSLocalizedString(@"menuTutoText3", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(16, 16, 78, 78)],
                                @"caption": NSLocalizedString(@"menuTutoText4", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(106, 16, 78, 78)],
                                @"caption": NSLocalizedString(@"menuTutoText5", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            ];
    
    DDCoachMarksView *coachMarksView = [[DDCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    
    [self.view addSubview:coachMarksView];
    [coachMarksView start];
}

- (void)synchroTutoViewController {
    NSLog(@"synchroTutoViewController");
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(synchronisation.frame.size.width/2-131, dimensionAdapter.sizey150, 262, 262)],
                                @"caption": NSLocalizedString(@"synchroTutoText1", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(synchronisation.frame.size.width/2-131, dimensionAdapter.sizey350, 262, 262)],
                                @"caption": NSLocalizedString(@"synchroTutoText2", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            ];
    
    DDCoachMarksView *coachMarksView = [[DDCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    
    [self.view addSubview:coachMarksView];
    [coachMarksView start];
}


- (IBAction)buyAction:(id)sender {
    if (!dialogIdentification) {
        dialogIdentification = [[DialogIdentification alloc]initWithStyle:2];
        [self.view addSubview:dialogIdentification];
        dialogIdentification.aDelegate = self;
    }
    [dialogIdentification changeToPageMain];
    dialogIdentification.hidden = NO;
}

- (void)showProductActionFor:(int)subscriptionType {
    if (subscriptionType == 1) {
        NSLog(@"type particulier");
    }
    
    else if (subscriptionType == 2) {
        NSLog(@"type institution");
    }
    
    else if (subscriptionType == 3) {
        NSLog(@"type professionnel");
    }
    if (!notificationLaunched) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:kProductPurchasedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector: @selector(productPurchaseFailed:) name:kProductPurchaseFailedNotification object: nil];
        notificationLaunched = YES;
    }
    NSString *productIdentifier = [[SouvenirInAppPurchaseHelper sharedHelper]getProductIdentifierByIndex:subscriptionType-1];
    if (productIdentifier != nil) {
        if ([[SouvenirInAppPurchaseHelper sharedHelper]buyProductIdentifier:productIdentifier]) {
            [self showHudWithString:NSLocalizedString(@"accessStore", @"")];
        }
        else {
            [STMUtilities showAlertDialogWithMessageKey:@"error_product_not_found_in_store" andDuration:4];
            [[SouvenirInAppPurchaseHelper sharedHelper]requestProductData];
        }
    }
}

// success
- (void)productPurchased:(NSNotification *)notification {
    NSLog(@"productPurchased %@", notification);
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message: NSLocalizedString(@"Votre achat a été réalisé avec succès", @"")
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK", nil];
    [self notifyServerPurchaseWithProductId:[notification object]];
    [alert show];
    [self dismissViewControllerAnimated:YES completion:nil];
}

// failure
- (void)productPurchaseFailed:(NSNotification *)notification {
    NSLog(@"productPurchaseFailed %@", notification);
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    SKPaymentTransaction * transaction = (SKPaymentTransaction *) notification.object;
    if (transaction.error.code != SKErrorPaymentCancelled) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:transaction.error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

/* send the purchase receipt to server for validation */
- (void)notifyServerPurchaseWithProductId:(NSString *)productId
{
    NSLog(@"notifyServerPurchaseWithProductId");
    NSString * requestURL = [STMURLFactory getURLNotifyServerPurchase];
    NSLog(@"%@", requestURL);
    [requestManager POST:requestURL parameters:[URLFactory getURLNotifyServerPurchaseParametersWithProductId:productId] success:^(AFHTTPRequestOperation * operation, id responseObject) {
        NSLog(@"notifyServerPurchase - server response: %@", responseObject);
        NSString *stringDateEnd = [responseObject objectForKey:kJsonParamSubscriptionDateEnd];
        if (stringDateEnd != nil) {
            [Subscription updateSubscriptionEndDateWithString:stringDateEnd];
            [Subscription updateSubscriptionState:@"OK"];
            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:kUserDefaultsKeySubscriptionPurchaseNotSentToServerProductId];
            if ([Account isVisitMode]) {
                [Account setVisitMode:NO];
                [self showDialogAskForAccountCreation];
            }
            if (dialogIdentification) {
                [dialogIdentification removeFromSuperview];
                dialogIdentification = nil;
            }
            
            if (dialogGameBlocked) {
                [dialogGameBlocked removeFromSuperview];
                dialogGameBlocked = nil;
            }
        }
        else {
            NSString *stringError = [responseObject objectForKey:kJsonParamError];
            if (stringError != nil) {
                NSLog(@"purchase not valided by server, reject subscription, error = %@", stringError);
                [Subscription rejectLastSubscriptionUpdate];
            }
        }
        [[NSUserDefaults standardUserDefaults]synchronize];
        currentStep = STEP_SYNCHRO;
        [self manageLaunchSteps];
    } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
        NSLog(@"ERROR - notifyServerPurchase: error = %@, for URL = %@", error, requestURL);
        [[NSUserDefaults standardUserDefaults]setObject:productId forKey:kUserDefaultsKeySubscriptionPurchaseNotSentToServerProductId];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }];
}

- (IBAction)settingAction:(id)sender {
    NSLog(@"showDialogSettingsAction");
    if (!dialogSetting) {
        BOOL isAccountInfoNeed = !([Account isVisitMode]||[Account isValidUserWithoutAccount]);
        dialogSetting = [[SettingViewController alloc] initDialogSetting:isAccountInfoNeed];
        [self.view addSubview:dialogSetting];
    }
    if (dialogSetting.buttonConnectAccount) {
        [dialogSetting.buttonConnectAccount addTarget:self action:@selector(buttonConnectAction) forControlEvents:UIControlEventTouchUpInside];
    }
    if (dialogSetting.buttonCreateAccount) {
        if ([Subscription isIndividualSubscription]) {
            [dialogSetting.buttonCreateAccount addTarget:self action:@selector(confirmAccountCreationAction) forControlEvents:UIControlEventTouchUpInside];
        }
        else if ([Subscription isInstitutionSubscription]) {
            [dialogSetting.buttonCreateAccount addTarget:self action:@selector(buttonCreateInstitutionAction) forControlEvents:UIControlEventTouchUpInside];
        }
        else if ([Subscription isProfessionalSubscription]) {
            [dialogSetting.buttonCreateAccount addTarget:self action:@selector(buttonCreateProfessionalAction) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    [dialogSetting setHidden:NO];
    [self.view bringSubviewToFront:dialogSetting];
}

- (IBAction)aboutUsAction:(id)sender {
    NSLog(@"showDialogAboutUsAction");
    if (!dialogAboutUs) {
        dialogAboutUs = [[AboutUsViewController alloc] initDialogAboutUs];
        [self.view addSubview:dialogAboutUs];
    }
    [dialogAboutUs setHidden:NO];
    [self.view bringSubviewToFront:dialogAboutUs];
}

/* button restore purchase on the setting dialog */
- (void)buttonConnectAction
{
    NSLog(@"buttonRestorePurchaseAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if ([STMUtilities isInternetConnected]) {
        
        if (!(dialogAskForAccountCreation == nil || [dialogAskForAccountCreation isHidden])) {
            [dialogAskForAccountCreation setHidden:YES];
        }
        
        [self showDialogIdentification];
        [dialogIdentification changeToPageConnectAccountAction];
        [dialogIdentification.buttonRight removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [dialogIdentification.buttonRight addTarget:self action:@selector(hideDialogIdentificationAction) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)restorePurchaseOrLogToInsitutionWithEmailOrPseudo:(NSString *)email andPassword:(NSString *)password
{
    NSLog(@"restorePurchaseOrLogToInsitutionWithEmailOrPseudo");
    if (![Subscription isIndividualSubscription] && ![Account isAccountExists]) {
        [self logToInstitutionWithEmail:email andPassword:password];
    }
    else {
        [self restorePurchaseWithEmailOrPseudo:email andPassword:password];
    }
    
    
}

- (void)restorePurchaseWithEmailOrPseudo:(NSString *)email andPassword:(NSString *)password
{
    NSLog(@"restorePurchaseWithEmailOrPseudo");
        [self showHudWithString:nil];
        NSString *requestURL = [STMURLFactory getURLLogInWithEmail:email andPassword:password];//: andPassword:];
        [requestManager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
            NSLog(@"restorePurchaseWithEmailOrPseudo - server response: %@", responseObject);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            BOOL isServerResponseUnknown = YES;
            /* a JSON element should be a NSDictionary but not a NSString */
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                isServerResponseUnknown = NO;
                /* when server response with error */
                NSString *errorMessage = [responseObject objectForKey:kJsonParamError];
                if (errorMessage != nil) {
                    NSLog(@"restorePurchaseWithEmailOrPseudo - server response error: %@", errorMessage);
                    if ([errorMessage isEqualToString:kJsonErrorUnknownLogin]) {
                        NSLog(@"login not found");
                        [dialogIdentification showErrorLoginNotFound];
                    }
                    else if ([errorMessage isEqualToString:kJsonErrorUnknownPassword]) {
                        NSLog(@"password incorrect");
                        [dialogIdentification showErrorLoginPasswordIncorrect];
                    }
                }
                else {
                    NSLog(@"restorePurchaseWithEmailOrPseudo - ok");
                    NSString *message = NSLocalizedString(@"restore_purchase_done", nil);
                    if ([responseObject objectForKey:kJsonParamSubscriptionDateEnd] != nil) {
                        NSString * previousEstablishmentName = [Subscription getInstitutionName];
                        if (!synchro) {
                            synchro = [[Synchronisation alloc] initSynchronisation:NO];
                        }
                        [synchro updateSubscriptionAndAccountInfoWithJsonString:responseObject];
                        /* trail user */
                        BOOL isVisitMode = [Account isVisitMode];
                        if (isVisitMode) {
                            [Account setVisitMode:NO];
                            if (![Subscription isIndividualSubscription]) {
                                message = NSLocalizedString(@"log_to_institution_done", nil);
                            }
                            [self synchronizeAll];
                        }
                        /* valid user(with or without account) */
                        else {
                            if (![previousEstablishmentName isEqualToString:[Subscription getInstitutionName]]) {
                                NSLog(@"restorePurchaseWithEmailOrPseudo - account changed");
                                [self disconnectProfile];
                                [databaseManager deleteAllPersons];
                                [self synchronizeAll];
                            }
                            /* same account, no need to restore */
                            else {
                                NSLog(@"same account, no need to restore/connect");
                            }
                        }
                        
                        
                        @autoreleasepool {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
                            [alert show];
                        }
                    }
                    if (dialogIdentification && ![dialogIdentification isHidden]) {
                        [dialogIdentification setHidden:YES];
                    }
                    if (dialogSetting) {
                        [dialogSetting removeFromSuperview];
                        dialogSetting = nil;
                    }
                    currentStep = STEP_LOGIN_SUCCESS;
                    [self manageLaunchSteps];
                }
            }
            if (isServerResponseUnknown) {
                NSLog(@"ERROR - restorePurchaseWithEmailOrPseudo unknown server response for URL %@",requestURL);
            }
        } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
            NSLog(@"ERROR - restorePurchaseWithEmailOrPseudo: error = %@, for URL = %@", error, requestURL);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [STMUtilities showAlertDialogWithMessageKey:NSLocalizedString(@"internet_message", nil) andDuration:2];
        }];
}

- (void)logToInstitutionWithEmail:(NSString *)email andPassword:(NSString *)password
{
    NSLog(@"logToInstitutionWithEmail");
        [self showHudWithString:nil];
        NSString *requestURL;
        requestURL = [STMURLFactory getURLConnectToInstitutionWithEmail:email andPassword:password];
        [requestManager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
            NSLog(@"logToInstitutionWithEmail - server response: %@", responseObject);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            BOOL isServerResponseUnknown = YES;
            /* a JSON element should be a NSDictionary but not a NSString */
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                isServerResponseUnknown = NO;
                /* when server response with error */
                NSString *errorMessage = [responseObject objectForKey:kJsonParamError];
                if (errorMessage != nil) {
                    NSLog(@"logToInstitutionWithEmail - server response error: %@", errorMessage);
                    if ([errorMessage isEqualToString:kJsonErrorUnknownLogin]) {
                        NSLog(@"email not found");
                        [dialogIdentification showErrorLoginNotFound];
                    }
                    else if ([errorMessage isEqualToString:kJsonErrorUnknownPassword]) {
                        NSLog(@"password incorrect");
                        [dialogIdentification showErrorLoginPasswordIncorrect];
                    }
                    else if ([errorMessage isEqualToString:kJsonErrorInvalidInstitution]) {
                        NSLog(@"invalid institution");
                        [STMUtilities showAlertDialogWithMessageKey:NSLocalizedString(@"error_invalid_institution", nil) andDuration:3];
                    }
                }
                else {
                    NSLog(@"logToInstitutionWithEmail - ok");
                    if (!synchro) {
                        synchro = [[Synchronisation alloc] initSynchronisation:NO];
                    }
                    [synchro updateSubscriptionAndAccountInfoWithJsonString:responseObject];
                    if (dialogSetting) {
                        [dialogSetting removeFromSuperview];
                        dialogSetting = nil;
                    }
                    [self synchronizeAll];
                    NSString *message = NSLocalizedString(@"log_to_institution_done", @"");
                    @autoreleasepool {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"ok", nil), nil];
                        [alert show];
                    }
                    if (dialogIdentification && ![dialogIdentification isHidden]) {
                        [dialogIdentification setHidden:YES];
                    }
                    if (dialogCreateOrConnectInstitution) {
                        [dialogCreateOrConnectInstitution removeFromSuperview];
                        dialogCreateOrConnectInstitution = nil;
                    }
                    if (dialogAskForAccountCreation) {
                        [dialogAskForAccountCreation removeFromSuperview];
                        dialogAskForAccountCreation = nil;
                    }
                }
            }
            if (isServerResponseUnknown) {
                NSLog(@"ERROR - logToInstitutionWithEmail unknown server response for URL %@",requestURL);
            }
        } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
            NSLog(@"ERROR - logToInstitutionWithEmail: error = %@, for URL = %@", error, requestURL);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [STMUtilities showAlertDialogWithMessageKey:NSLocalizedString(@"internet_message", nil) andDuration:2];
        }];
}



- (void)hideDialogIdentificationAction
{
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if (dialogIdentification) {
        [dialogIdentification setHidden:YES];
    }
}


/* when we click on the button "cancel" of the picker */
- (void)cancelSelectionBirthDateAction:(id)sender
{
    NSLog(@"cancelSelectionBirthDateAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [dateOfBirthTextField setText:nil];
    currentDateFormatDatebase = nil;
    [dateOfBirthTextField setEnabled:YES];
}

/* when we click on the button "OK" of the picker */
- (void)birthDateSelectedAction:(NSDate *)date
{
    NSLog(@"birthDateSelectedAction date = %@", date);
    /* date format of the region of the user */
    NSDateFormatter * dateFormatUser = [[NSDateFormatter alloc]init];
    /* date format unified of the database */
    NSDateFormatter * dateFormatDatabase = [[NSDateFormatter alloc]init];
    [dateFormatUser setDateFormat:NSLocalizedString(@"date_format", nil)];
    [dateFormatDatabase setDateFormat:NSLocalizedString(@"yyyy-MM-dd", nil)];
    [dateOfBirthTextField setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"you_borned_at", nil),[dateFormatUser stringFromDate:date]]];
    currentDateFormatDatebase = [dateFormatDatabase stringFromDate:date];
    [dateOfBirthTextField setEnabled:YES];
}


- (void)showHudWithString:(NSString *)message {
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = message;
}

- (void) startToHomeViewController {
    UIViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"];
    [self.navigationController pushViewController:homeViewController animated:YES];
}

- (void) manageProfile
{
    allPersons = [databaseManager getAllPersons];
    if (allPersons != nil && [allPersons count] > 0) {
        [self showDialogChooseProfile];
    }
    else {
        [viewCreateProfile setHidden:NO];
        [self.view bringSubviewToFront:viewCreateProfile];
        /* enable the text field */
        if (dateOfBirthTextField) {
            [dateOfBirthTextField setEnabled:YES];
            [firstnameTextField setEnabled:YES];
            [nameTextField setEnabled:YES];
        }
    }
}

/* show the connection dialog */
- (void)showDialogChooseProfile
{
    NSLog(@"showDialogChooseProfile");
    if (!dialogChooseProfile) {
        dialogChooseProfile = [[STMDialogView alloc]initWithType:STMDialogTypePickerWithTwoButtons andStyle:2 andBackground:@"dialog.png"];
        [self.view addSubview:dialogChooseProfile];
        //        dialogChooseProfile.frame = self.view.frame;
        
        [dialogChooseProfile setupButtonFor:1 andText:NSLocalizedString(@"valid", nil) andColor:stringMenuButtonColorDark andHighlight:stringMenuButtonColorLight];
        [dialogChooseProfile setupButtonFor:2 andText:NSLocalizedString(@"cancel", nil) andColor:stringMenuButtonColorDark andHighlight:stringMenuButtonColorLight];
        
        
        [dialogChooseProfile.buttonLeft.titleLabel setFont:[UIFont boldSystemFontOfSize:25]];
        [dialogChooseProfile.buttonRight.titleLabel setFont:[UIFont boldSystemFontOfSize:25]];
        [dialogChooseProfile setTarget:self forButton:1 action:@selector(connectToProfileAction)];
        [dialogChooseProfile setTarget:self forButton:2 action:@selector(cancelConnectProfileAction)];
        [dialogChooseProfile setTitleUpText:NSLocalizedString(@"choose_profile", nil)];
        [dialogChooseProfile setTitleLeftText:NSLocalizedString(@"pseudo", nil)];
        if (((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson != nil) {
            [dialogChooseProfile.textFieldPicker setText:((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson.pseudo];
        }
        [dialogChooseProfile setTextFieldPickerPlaceholderText:NSLocalizedString(@"please_choose_your_profile", nil)];
        [dialogChooseProfile.textFieldPicker addTarget:self action:@selector(showProfilePickerAction:) forControlEvents:UIControlEventTouchDown];
    }
    else if ([dialogChooseProfile isHidden]) {
        [dialogChooseProfile setHidden:NO];
        [self.view bringSubviewToFront:dialogChooseProfile];
        if (![dialogChooseProfile.textFieldPicker isEnabled]) {
            [dialogChooseProfile.textFieldPicker setEnabled:YES];
        }
    }
}

- (void)connectToProfileAction
{
    NSLog(@"connectToProfileAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if (indexProfile >= 0) {
        ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson = [allPersons objectAtIndex:indexProfile];
        [dialogChooseProfile setHidden:YES];
        [dialogChooseProfile.textFieldPicker setEnabled:YES];
        [self startToHomeViewController];
    }
}

- (void)cancelConnectProfileAction {
    NSLog(@"cancelConnectProfileAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    indexProfile = -1;
    [dialogChooseProfile setHidden:YES];
    [dialogChooseProfile.textFieldPicker setEnabled:NO];
    [dialogChooseProfile.textFieldPicker setText:nil];
}

/* when user click on text field, show profile picker */
- (void)showProfilePickerAction:(id)sender
{
    NSLog(@"showProfilePickerAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if ([allPersons count] != 0) {
        [dialogChooseProfile.textFieldPicker setEnabled:NO];
        NSMutableArray * allPseudos = [[NSMutableArray alloc]init];
        for (Person * person in allPersons) {
            [allPseudos addObject:person.pseudo];
        }
        
        if (indexProfile == -1) {
            indexProfile = 0;
        }
        pickerProfile = [ActionSheetStringPicker showPickerWithTitle:nil rows:allPseudos initialSelection:indexProfile target:self successAction:@selector(profileSelectedAction:) cancelAction:@selector(cancelProfileSelectionAction:) origin:dialogChooseProfile.textFieldPicker];
    }
    else {
        [STMUtilities showAlertDialogWithMessageKey:@"no_profile_created" andDuration:2];
    }
}

/* when user choosed a profile from the picker */
- (void)profileSelectedAction:(NSNumber *)selectedIndex
{
    NSLog(@"profileSelectedAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    indexProfile = [selectedIndex intValue];
    if (indexProfile < 0 || indexProfile >= allPersons.count) {
        indexProfile = 0;
    }
    [dialogChooseProfile.textFieldPicker setEnabled:YES];
    dialogChooseProfile.textFieldPicker.text = ((Person *)[allPersons objectAtIndex:indexProfile]).pseudo;
}

/* when user canceled the selection of profile */
- (void)cancelProfileSelectionAction:(id)sender
{
    NSLog(@"cancelProfileSelectionAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [dialogChooseProfile.textFieldPicker setEnabled:YES];
    dialogChooseProfile.textFieldPicker.text = nil;
    indexProfile = -1;
}

/* when user click on the button to do synchro / update */
- (IBAction)synchroAndUpdateAction:(id)sender
{
    NSLog(@"synchroAndUpdateAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if (!synchronisation) {
        synchronisation = [[SynchronisationView alloc] initSynchronisationView];
        UIButton * tutoButton;
        tutoButton = [[UIButton alloc] initWithFrame:CGRectMake(synchronisation.frame.size.width*2/12, synchronisation.frame.size.height/7, synchronisation.frame.size.width/13, synchronisation.frame.size.width/13)];
        
        [tutoButton setImage:[UIImage imageNamed:@"button_tuto.png"] forState:UIControlStateNormal];
        [tutoButton addTarget:self action:@selector(synchroTutoViewController) forControlEvents:UIControlEventTouchUpInside];
        [synchronisation addSubview:tutoButton];
        [self.view addSubview:synchronisation];
    }
    else {
        [synchronisation setHidden:NO];
    }
    
}

- (void)disconnectProfile
{
    NSLog(@"disconnectProfile");
}


- (void)showDialogAskForAccountCreation {
    NSLog(@"showDialogAskForAccountCreation");
    if (!dialogAskForAccountCreation) {
        
        dialogAskForAccountCreation = [[STMDialogView alloc]initWithType:STMDialogTypeTextWithTwoButtons andStyle:2 andBackground:@"dialog"];
        
        [self.view addSubview:dialogAskForAccountCreation];
        if ([Subscription isIndividualSubscription]) {
            [dialogAskForAccountCreation setTitleUpText:NSLocalizedString(@"create_account", nil)];
            [dialogAskForAccountCreation setTextViewText:NSLocalizedString(@"ask_create_account_text", nil)];
        }
        else if ([Subscription isInstitutionSubscription]) {
            [dialogAskForAccountCreation setTitleUpText:NSLocalizedString(@"institution_account", nil)];
            [dialogAskForAccountCreation setTextViewText:NSLocalizedString(@"ask_create_institution_text", nil)];
        }
        else if ([Subscription isProfessionalSubscription]) {
            [dialogAskForAccountCreation setTitleUpText:NSLocalizedString(@"professional_account", nil)];
            [dialogAskForAccountCreation setTextViewText:NSLocalizedString(@"ask_create_professional_text", nil)];
        }
        
        [dialogAskForAccountCreation setupButtonFor:1 andText:NSLocalizedString(@"yes", nil) andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
        [dialogAskForAccountCreation setupButtonFor:2 andText:NSLocalizedString(@"no", nil) andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
        [dialogAskForAccountCreation.textView setTextAlignment:NSTextAlignmentJustified];
        [dialogAskForAccountCreation setTarget:self forButton:1 action:@selector(confirmAccountCreationAction)];
        [dialogAskForAccountCreation setTarget:self forButton:2 action:@selector(cancelAccountCreationAction)];
    }
    else {
        [dialogAskForAccountCreation setHidden:NO];
        [self.view bringSubviewToFront:dialogAskForAccountCreation];
    }
}
- (void)confirmAccountCreationAction {
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if (dialogAskForAccountCreation != nil) {
        [dialogAskForAccountCreation setHidden:YES];
    }
    if ([STMUtilities isInternetConnected]) {
        if ([Subscription isIndividualSubscription]) {
            [self showDialogIdentification];
            [dialogIdentification changeToPageCreateAccountAction];
        }
        else {
            [self showDialogCreateOrConnectInstitution];
        }
    }
//    else {
//        [self showDialogErrors:kErrorInternet];
//    }
}

- (void)createAccount:(Account *)account
{
    NSLog(@"createAccount");
        [self showHudWithString:nil];
        NSString * requestURL = [STMURLFactory getURLCreateAccount:account];
        [requestManager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
            NSLog(@"createAccount - server response: %@", responseObject);
            BOOL isServerResponseUnknown = YES;
            /* a JSON element should be a NSDictionary but not a NSString */
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                isServerResponseUnknown = NO;
                /* when server response with error */
                NSString *errorMessage = [responseObject objectForKey:kJsonParamError];
                if (errorMessage != nil) {
                    NSLog(@"createAccount - server response error: %@", errorMessage);
                    if ([errorMessage isEqualToString:kJsonErrorDuplicatePseudo]) {
                        NSLog(@"dulicate pseudo");
                        [dialogIdentification showErrorPseudoDuplicate];
                    }
                    else if ([errorMessage isEqualToString:kJsonErrorDuplicateEmail]) {
                        NSLog(@"dulicate mail");
                        [dialogIdentification showErrorEmailDuplicate];
                    }
                    /* duplicate serial num => tablet already have an account, make synchro to refresh  */
                    else if ([errorMessage isEqualToString:kJsonErrorDuplicateSerialNumber]) {
                        NSLog(@"duplicate serial number");
                        [dialogIdentification showErrorSerialNumberDuplicate];
                    }
                }
                else {
                    NSLog(@"createAccount - ok");
                    if ([responseObject objectForKey:kJsonParamSubscriptionDateEnd] != nil) {
                        [account saveToUserDefaults];
                        if (!synchro) {
                            synchro = [[Synchronisation alloc] initSynchronisation:NO];
                        }
                        [synchro updateSubscriptionAndAccountInfoWithJsonString:responseObject];
                    }
                    if (dialogIdentification) {
                        [dialogIdentification removeFromSuperview];
                        dialogIdentification = nil;
                    }
                    if (dialogCreateInstitution) {
                        [dialogCreateInstitution removeFromSuperview];
                        dialogCreateInstitution = nil;
                    }
                    if (dialogCreateProfessional) {
                        [dialogCreateProfessional removeFromSuperview];
                        dialogCreateProfessional = nil;
                    }
                    if (dialogCreateOrConnectInstitution) {
                        [dialogCreateOrConnectInstitution removeFromSuperview];
                        dialogCreateOrConnectInstitution = nil;
                    }
                    if (dialogSetting) {
                        [dialogSetting removeFromSuperview];
                        dialogSetting = nil;
                    }
        
                    [STMUtilities showAlertDialogWithMessageKey:NSLocalizedString(@"create_account_success", nil) andDuration:2];
                    [self synchronizeAll];
                }
            }
            if (isServerResponseUnknown) {
                NSLog(@"ERROR - createAccount unknown server response for URL %@",requestURL);
            }
        } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
            NSLog(@"ERROR - createAccount: error = %@, for URL = %@", error, requestURL);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [STMUtilities showAlertDialogWithMessageKey:NSLocalizedString(@"internet_message", nil) andDuration:2];
        }];
}



- (void)cancelAccountCreationAction {
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [dialogAskForAccountCreation setHidden:YES];
    currentStep = STEP_SYNCHRO;
    [self manageLaunchSteps];
}

- (void)showDialogCreateOrConnectInstitution {
    NSLog(@"showDialogCreateOrConnectInstitution");
    if (!dialogCreateOrConnectInstitution) {
        dialogCreateOrConnectInstitution = [[STMDialogView alloc]initWithType:STMDialogTypeTwoButtons andStyle:2 andBackground:@"dialog.png"];
        [self.view addSubview:dialogCreateOrConnectInstitution];
        int button1Id = 1;
        int button2Id = 2;
        int button3Id = 3;
        NSString * text;
        if ([Subscription isInstitutionSubscription]) {
            text = NSLocalizedString(@"create_institution_button_text", nil);
        }
        else if ([Subscription isProfessionalSubscription]) {
            text = NSLocalizedString(@"create_professional_button_text", nil);
        }
        [dialogCreateOrConnectInstitution setupButtonFor:button1Id andText:NSLocalizedString(@"connect", nil) andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
        [dialogCreateOrConnectInstitution setupButtonFor:button2Id andText:text andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
        [dialogCreateOrConnectInstitution setupButtonFor:button3Id andText:NSLocalizedString(@"return", nil) andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
        [dialogCreateOrConnectInstitution setTarget:self forButton:button1Id action:@selector(buttonConnectInstitutionAction)];
        [dialogCreateOrConnectInstitution setTarget:self forButton:button2Id action:@selector(buttonCreateInsitutionOrProfessionalAction)];
        [dialogCreateOrConnectInstitution setTarget:self forButton:button3Id action:@selector(buttonQuitDialogCreateOrConnectInstitutionAction)];
    }
    else if ([dialogCreateOrConnectInstitution isHidden]) {
        [dialogCreateOrConnectInstitution setHidden:NO];
        [self.view bringSubviewToFront:dialogCreateOrConnectInstitution];
    }
}

- (void)buttonConnectInstitutionAction
{
    NSLog(@"buttonConnectInstitutionAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if ([STMUtilities isInternetConnected]) {
        [self showDialogIdentification];
        [dialogIdentification changeToPageConnectAccountAction];
        [dialogIdentification.buttonRight removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
        [dialogIdentification.buttonRight addTarget:self action:@selector(showDialogCreateOrConnectInstitutionAction) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)showDialogCreateOrConnectInstitutionAction {
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [self showDialogCreateOrConnectInstitution];
    [self.dialogIdentification setHidden:YES];
}

- (void) buttonCreateInsitutionOrProfessionalAction {
    NSLog(@"buttonCreateInsitutionOrProfessionalAction");
    if ([Subscription isInstitutionSubscription]) {
        [self buttonCreateInstitutionAction];
    }
    else if ([Subscription isProfessionalSubscription]) {
        [self buttonCreateProfessionalAction];
    }
}

- (void)buttonCreateInstitutionAction
{
    NSLog(@"buttonCreateInstitutionAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    NSLog(@"showDialogCreateInstitution");
    if (dialogCreateInstitution == nil) {
        dialogCreateInstitution = [[STMDialogCreateInstitution alloc]initWithStyle:2];
        [self.view addSubview:dialogCreateInstitution];
        dialogCreateInstitution.delegate = self;
    }
    else {
        [dialogCreateInstitution setHidden:NO];
        [self.view bringSubviewToFront:dialogCreateInstitution];
    }
}

- (void) buttonCreateProfessionalAction {
    NSLog(@"buttonCreateInstitutionAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    NSLog(@"showDialogCreateInstitution");
    if (dialogCreateProfessional == nil) {
        dialogCreateProfessional = [[STMDialogCreateProfessional alloc]initWithStyle:2];
        [self.view addSubview:dialogCreateProfessional];
        dialogCreateProfessional.delegate = self;
    }
    else {
        [dialogCreateProfessional setHidden:NO];
        [self.view bringSubviewToFront:dialogCreateProfessional];
    }

}

- (void) buttonQuitDialogCreateOrConnectInstitutionAction {
    NSLog(@"buttonQuitDialogCreateOrConnectInstitutionAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [dialogCreateOrConnectInstitution setHidden:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
