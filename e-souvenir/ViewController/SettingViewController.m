//
//  SettingViewController.m
//  e-souvenir
//
//  Created by Dynseo on 19/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "SettingViewController.h"

@implementation SettingViewController {
    DimensionAdapter * dimensionAdapter;
    int marginLeft, marginTop;
}

@synthesize backgroundView, accountTitle, emailInfo, pseudoInfo, institutionInfo, buttonCreateAccount, buttonConnectAccount, titleUp, buttonBack, titleEmail, titleAccount, titleSectionAccount;
- (id) initDialogSetting : (BOOL) showInfoAccount {
    NSLog(@"initWithAccountInfo");
    dimensionAdapter = [[DimensionAdapter alloc] initDimensionAdapter];
    self = [super initWithFrame:CGRectMake(0, 0, dimensionAdapter.sizex1024, dimensionAdapter.sizey768)];
    [self setBackgroundColor:[UIColor colorWithRed:0.502 green:0.502 blue:0.502 alpha:0.6]];
    
    [self createBackgroundWithColorString:@"#ffffff"];
    marginLeft = dimensionAdapter.sizex222;
    marginTop = dimensionAdapter.sizey187;
    float currentPositionY = marginTop;
    
    
    titleUp = [[UILabel alloc]initWithFrame:CGRectMake(dimensionAdapter.sizex262, dimensionAdapter.sizey120, dimensionAdapter.sizex500, dimensionAdapter.sizey120)];
    [titleUp setFont:[UIFont boldSystemFontOfSize:dimensionAdapter.sizey50]];
    titleUp.lineBreakMode = NSLineBreakByWordWrapping;
    titleUp.numberOfLines = 0;
    titleUp.textAlignment = NSTextAlignmentCenter;
    [titleUp setText:NSLocalizedString(@"settings", nil)];
    [self addSubview:titleUp];
    
    
    currentPositionY += dimensionAdapter.sizey40;
    buttonBack = [[STMButton alloc]initWithFrame:CGRectMake(dimensionAdapter.sizex412, dimensionAdapter.sizey585, dimensionAdapter.sizex200, dimensionAdapter.sizey60) andType:STMButtonTypeRounded andTitle:NSLocalizedString(@"return",nil) andTitleSize:dimensionAdapter.sizey40 andBackgroundColor:NSLocalizedString(@"menu_light_button_code", nil) andHighlightColor:NSLocalizedString(@"menu_dark_button_code", nil)];
    [buttonBack.titleLabel setFont:[UIFont boldSystemFontOfSize:dimensionAdapter.sizey30]];
    [buttonBack addTarget:self action:@selector(hideAction)forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:buttonBack];
    
    if (showInfoAccount) {
        NSLog(@"init infomation of account : particulier ou institution");
        titleSectionAccount = [[UILabel alloc]initWithFrame:CGRectMake(marginLeft, currentPositionY, dimensionAdapter.sizex600, dimensionAdapter.sizey30)];
        currentPositionY += dimensionAdapter.sizey40;
        titleAccount = [[UILabel alloc]initWithFrame:CGRectMake(marginLeft, currentPositionY, dimensionAdapter.sizex600, dimensionAdapter.sizey30)];
        if ([Subscription isIndividualSubscription]) {
            currentPositionY += dimensionAdapter.sizey40;
            titleEmail = [[UILabel alloc]initWithFrame:CGRectMake(marginLeft, currentPositionY, dimensionAdapter.sizex600, dimensionAdapter.sizey30)];
            [self addSubview:titleEmail];
        }
        currentPositionY += dimensionAdapter.sizey45;
    }
    else {
        NSLog(@"init button for ask create a account (if bought) or connect");
        /* user in free trail */
        if ([Account isVisitMode]) {
            NSLog(@"isVisitMode");
            buttonConnectAccount = [self createButtonWithFrame:CGRectMake(marginLeft, currentPositionY, dimensionAdapter.sizex350, dimensionAdapter.sizey40) andTitle:NSLocalizedString(@"login_or_restore_purchase", nil)];
        }
        /* valid user without account */
        else if ([Account isValidUserWithoutAccount]) {
            NSLog(@"isValidUserWithoutAccount");
            /* individual user */
            if ([Subscription isIndividualSubscription]) {
                buttonCreateAccount = [self createButtonWithFrame:CGRectMake(marginLeft, currentPositionY, dimensionAdapter.sizex350, dimensionAdapter.sizey40) andTitle:NSLocalizedString(@"create_account_button_text", nil)];
            }
            /* institution user */
            else if ([Subscription isInstitutionSubscription]) {
                buttonCreateAccount = [self createButtonWithFrame:CGRectMake(marginLeft, currentPositionY, dimensionAdapter.sizex350, dimensionAdapter.sizey40) andTitle:NSLocalizedString(@"create_institution_button_text", nil)];
            }
            
            else if ([Subscription isProfessionalSubscription]) {
                buttonCreateAccount = [self createButtonWithFrame:CGRectMake(marginLeft, currentPositionY, dimensionAdapter.sizex350, dimensionAdapter.sizey40) andTitle:NSLocalizedString(@"create_professional_button_text", nil)];
            }
            currentPositionY += dimensionAdapter.sizey50;
            buttonConnectAccount = [self createButtonWithFrame:CGRectMake(marginLeft, currentPositionY, dimensionAdapter.sizex350, dimensionAdapter.sizey40) andTitle:NSLocalizedString(@"login_or_restore_purchase", nil)];
        }

    }
    
    [titleSectionAccount setFont:[UIFont systemFontOfSize:dimensionAdapter.sizey25]];
    [titleSectionAccount setText:NSLocalizedString(@"account", nil)];
    [self addSubview:titleSectionAccount];
    [self addSubview:titleAccount];
    [self loadAccountInfo];
    return self;
}

- (void)loadAccountInfo {
    if (titleEmail) {
        NSString *stringTitleEmail = NSLocalizedString(@"email", nil);
        NSString *stringEmail = [Account getEmail]?[Account getEmail]:@"";
        NSMutableAttributedString *attributedString1 = [[NSMutableAttributedString alloc]initWithString: [NSString stringWithFormat:@"%@ : %@", stringTitleEmail, stringEmail]];
        [attributedString1 addAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:dimensionAdapter.sizey18]} range:NSMakeRange(0, stringTitleEmail.length+3)];
        [attributedString1 addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:dimensionAdapter.sizey18]} range:NSMakeRange(stringTitleEmail.length+3, stringEmail.length)];
        [titleEmail setAttributedText:attributedString1];
        NSString *stringTitlePseudo = NSLocalizedString(@"pseudo", nil);
        NSString *stringPseudo = [Account getPseudo]?[Account getPseudo]:@"";
        NSMutableAttributedString *attributedString2 = [[NSMutableAttributedString alloc]initWithString: [NSString stringWithFormat:@"%@  %@", stringTitlePseudo,stringPseudo]];
        [attributedString2 addAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:dimensionAdapter.sizey18]} range:NSMakeRange(0, stringTitlePseudo.length+1)];
        [attributedString2 addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:dimensionAdapter.sizey18]} range:NSMakeRange(stringTitlePseudo.length+1, stringPseudo.length)];
        [titleAccount setAttributedText:attributedString2];
    }
    else {
        NSString *stringTitleEstablishmentName = NSLocalizedString(@"establishment_name", nil);
        NSString *stringEstablishmentName = [Subscription getInstitutionName];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString: [NSString stringWithFormat:@"%@ : %@", stringTitleEstablishmentName,stringEstablishmentName]];
        [attributedString addAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:dimensionAdapter.sizey18]} range:NSMakeRange(0, stringTitleEstablishmentName.length+3)];
        [attributedString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:dimensionAdapter.sizey18]} range:NSMakeRange(stringTitleEstablishmentName.length+3, stringEstablishmentName.length)];
        [titleAccount setAttributedText:attributedString];
    }
}

- (void)createBackgroundWithColorString:(NSString *)aColorString
{
    if (!backgroundView) {
        backgroundView = [[UIView alloc]initWithFrame:CGRectMake(dimensionAdapter.sizex128, dimensionAdapter.sizey96, dimensionAdapter.sizex768, dimensionAdapter.sizey576)];
        [backgroundView.layer setCornerRadius:dimensionAdapter.sizey5];
        if ([aColorString hasPrefix:@"#"]) {
            [backgroundView setBackgroundColor:[ColorTools colorFromHexString:aColorString]];
        }
        else {
            [backgroundView setBackgroundColor:[UIColor whiteColor]];
        }
        [self addSubview:backgroundView];
    }
}

- (UIButton *)createButtonWithFrame:(CGRect)aFrame andTitle:(NSString *)aTitle
{
    UIButton *button = [[UIButton alloc]initWithFrame:aFrame];
    button.layer.cornerRadius = dimensionAdapter.sizey10;
    [button setTitle:aTitle forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:dimensionAdapter.sizey18]];
    [button setTitleColor:[ColorTools colorFromHexString:NSLocalizedString(@"color_black", nil)] forState:UIControlStateNormal];
    [button setTitleColor:[ColorTools colorFromHexString:NSLocalizedString(@"menu_light_button_code", nil)] forState:UIControlStateHighlighted];
    button.layer.borderColor = [ColorTools colorFromHexString:NSLocalizedString(@"color_black", nil)].CGColor;//NSLocalizedString(@"menu_dark_button_code", nil)].CGColor;
    [button.layer setBorderWidth:2.0f];
    [self addSubview:button];
    return button;
}

- (void) hideAction {
    [self setHidden:YES];
}
@end
