//
//  SettingViewController.h
//  e-souvenir
//
//  Created by Dynseo on 19/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StimartLib/StimartLib.h>

@interface SettingViewController : UIView

@property (nonatomic) UIView *backgroundView;
@property (nonatomic) UILabel * titleUp;
@property (nonatomic) UILabel * accountTitle;
@property (nonatomic) UILabel * emailInfo;
@property (nonatomic) UILabel * pseudoInfo;
@property (nonatomic) UILabel * institutionInfo;

/* account title */
@property (nonatomic) UILabel *titleAccount;
/* email title */
@property (nonatomic) UILabel *titleEmail;
@property (nonatomic) UILabel *titleSectionAccount;

@property (nonatomic) UIButton * buttonCreateAccount;
@property (nonatomic) UIButton * buttonConnectAccount;
@property (nonatomic) STMButton * buttonBack;
@property (nonatomic) UIButton * buttonRestorePurchase;

- (id) initDialogSetting : (BOOL) showInfoAccount;

@end
