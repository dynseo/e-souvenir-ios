//
//  CategoryViewController.h
//  e-souvenir
//
//  Created by Dynseo on 18/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryViewController : UIViewController
@property NSMutableArray * listCategory;
@property NSString * nameCategory;
@property NSMutableArray * nameCategoryToShow;

- (void) initializeView;
- (void) setTypeSession : (int) position;
@end
