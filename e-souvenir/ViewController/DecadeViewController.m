//
//  DecadeViewController.m
//  e-souvenir
//
//  Created by Dynseo on 18/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "DecadeViewController.h"
#import "Category.h"
#import "Constants.h"

@interface DecadeViewController ()

@end

@implementation DecadeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
    [self initializeView];
    // Do any additional setup after loading the view.
}

- (void) loadData {
    self.nameCategory = @"Decade";
    self.listCategory = [[NSMutableArray alloc] init];
    NSArray * listYears = [NSLocalizedString(@"years", nil) componentsSeparatedByString:@"|"];
    for (int i = 0; i < listYears.count - 1; i++) {
        Category * aCategory = [[Category alloc] initCategoryWithStart:listYears[i] andEnd:listYears[i+1]];
        [self.listCategory addObject:aCategory];
    }
}

- (void) setTypeSession : (int) position {
    NSLog(@"setTypeSession decadeView");
    [super setTypeSession:position];
    Category * aCategory = [self.listCategory objectAtIndex:position];
    NSString * typeSession = [NSString stringWithFormat:@" (%@)", aCategory.start];
    typeSession = [NSLocalizedString(@"byDecade", nil) stringByAppendingString:typeSession];
    [[NSUserDefaults standardUserDefaults] setObject:typeSession forKey:kTypeSession];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
