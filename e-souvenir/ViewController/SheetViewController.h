//
//  SheetViewController.h
//  e-souvenir
//
//  Created by Dynseo on 18/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StimartLib/StimartLib.h>
#import "CreateSheetViewController.h"
#import "Sheet.h"

@interface SheetViewController: UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,CreateSheetViewControllerDelegate>
//MARK: - Properties
@property NSMutableArray * listSheets;
@property (nonatomic) ActionSheetStringPicker * pickerFileType;

//MARK: - Outlets
@property (strong, nonatomic) IBOutlet UIButton *buttonHome;
@property (strong, nonatomic) IBOutlet UIButton *buttonTuto;
@property (strong, nonatomic) IBOutlet UITextField *textFieldPicker;
@property (strong, nonatomic) IBOutlet UILabel *nbFilesPicked;
@property (strong, nonatomic) IBOutlet UIButton *buttonStart;
@property (strong, nonatomic) IBOutlet UICollectionView *filesCollectionView;
@property (strong, nonatomic) IBOutlet UILabel *explanationLabel;

//MARK: - Actions
- (IBAction)onClick:(UIButton *)sender;

@end
