//
//  HomeViewController.h
//  e-souvenir
//
//  Created by Dynseo on 18/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StimartLib/StimartLib.h>
#import "SynchronisationView.h"

@interface HomeViewController : UIViewController
@property (nonatomic) STMDialogCreateUser * dialogModifyProfil;
@property (nonatomic) STMDialogView *paramDialogView;
@property (nonatomic) STMDialogView * dialogSetting;
@property (nonatomic) ActionSheetStringPicker * pickerModeSelection;
@property (nonatomic) STMDialogView *dialogView;
@property (nonatomic) SynchronisationView * synchronisation;
@property (strong, nonatomic) IBOutlet UIButton *quitButton;
@property (strong, nonatomic) IBOutlet UIButton *tutoButton;
@property (strong, nonatomic) IBOutlet UIButton *synchroButton;
@property (strong, nonatomic) IBOutlet UIButton *paramButton;
@property (strong, nonatomic) IBOutlet UILabel *profilLabel;
@property (strong, nonatomic) IBOutlet UIButton *decennieButton;
@property (strong, nonatomic) IBOutlet UIButton *friseButton;
@property (strong, nonatomic) IBOutlet UIButton *themeButton;
@property (strong, nonatomic) IBOutlet UIButton *goutButton;
- (IBAction)onClick:(UIButton *)sender;

@end
