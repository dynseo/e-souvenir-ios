//
//  DataViewController.h
//  e-souvenir
//
//  Created by Dynseo on 01/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FirstView.h"
#import "SecondView.h"
#import "ThirdView.h"

@interface DataViewController : UIViewController<UITabBarDelegate>
@property (strong, nonatomic) IBOutlet UITabBar *tabBar;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (strong, nonatomic) IBOutlet UIButton *tutoButton;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIView *contentView;
- (IBAction)onClicked:(UIButton *)sender;

@end
