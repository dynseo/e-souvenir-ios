//
//  HomeViewController.m
//  e-souvenir
//
//  Created by Dynseo on 18/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//
#import "Constants.h"
#import "HomeViewController.h"
#import "AppDelegate.h"
#import "DatabaseManager.h"
#import "SheetViewController.h"
#import "DDCoachMarksView.h"
#import "Synchronisation.h"
#import "SynchronisationData.h"

@interface HomeViewController ()

@end

@implementation HomeViewController {
    Person * currentPerson;
    int indexMode;
    NSArray * listModes;
    DimensionAdapter * dimensionAdapter;
    ActionSheetDatePicker * pickerDate;
    NSString * currentDateFormatDatebase;
    DatabaseManager * databaseManager;
    Synchronisation *synchro;
    AFHTTPRequestOperationManager * requestManager;
}

@synthesize quitButton, tutoButton, synchroButton, paramButton, profilLabel, decennieButton, friseButton, themeButton, goutButton, dialogView, paramDialogView, dialogSetting, pickerModeSelection, dialogModifyProfil, synchronisation;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // set HomeViewController like a RootViewController
    NSMutableArray* viewControllers = [NSMutableArray arrayWithCapacity:1];
    [viewControllers addObject:self];
    [self.navigationController setViewControllers:[NSArray arrayWithArray:viewControllers] animated:YES];
    dimensionAdapter = [[DimensionAdapter alloc] initDimensionAdapter];
    // Do any additional setup after loading the view.
    currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    databaseManager = [DatabaseManager getSharedInstance];
    
    requestManager = [AFHTTPRequestOperationManager manager];
    requestManager.responseSerializer.acceptableContentTypes = [requestManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    [self initializeView];
}


- (void) initializeView {
    indexMode = -1;
    decennieButton.tag = 1;
    friseButton.tag = 2;
    themeButton.tag = 3;
    goutButton.tag = 4;
    quitButton.tag = 5;
    tutoButton.tag = 6;
    synchroButton.tag = 7;
    paramButton.tag = 8;
    profilLabel.text = [NSString stringWithFormat:@"%@ %@ %@ !",NSLocalizedString(@"hello", nil) ,currentPerson.firstName , currentPerson.name];
    profilLabel.textColor = UIColor.blackColor;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClick:(UIButton *)sender {
    switch (sender.tag) {
        case 1:
            [self startDecadeViewController];
            break;
        case 2:
            [self startFriseViewController];
            break;
        case 3:
            [self startThemeViewController];
            break;
        case 4:
            [self startGoutViewController];
            break;
        case 5:
            [self quitButtonClicked];
            break;
        case 6:
            [self startTutoViewController];
            break;
        case 7:
            [self synchroButtonClicked];
            break;
        case 8:
            [self paramButtonClicked];
            break;
            
        default:
            break;
    }
}

- (void) startDecadeViewController {
    UIViewController *decadeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"decadeViewController"];
    [self.navigationController pushViewController:decadeViewController animated:YES];
}

- (void) startThemeViewController {
    UIViewController *themeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"themeViewController"];
    [self.navigationController pushViewController:themeViewController animated:YES];
}

- (void) startFriseViewController {
    [[NSUserDefaults standardUserDefaults] setObject:NSLocalizedString(@"byFrieze", nil) forKey:kTypeSession];
    SheetViewController *sheetViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"sheetViewController"];
    sheetViewController.listSheets = [databaseManager getSheetsByFrieze];
    [self.navigationController pushViewController:sheetViewController animated:YES];
}

- (void) startGoutViewController {
    [[NSUserDefaults standardUserDefaults] setObject:NSLocalizedString(@"byTaste", nil) forKey:kTypeSession];
    SheetViewController *sheetViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"sheetViewController"];
    sheetViewController.listSheets = [databaseManager getSheetsByTaste:@"true"];
    if (sheetViewController.listSheets == nil || sheetViewController.listSheets.count == 0) {
        NSLog(@"Aucune fiche aimée");
        [STMUtilities showAlertDialogWithMessageKey:@"Aucune fiche aimée" andDuration:2];
    }
    else
        [self.navigationController pushViewController:sheetViewController animated:YES];
}

- (void) startTutoViewController {
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(87, 360, 200, 200)],
                                @"caption": NSLocalizedString(@"homeTutoText1", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(292, 360, 200, 200)],
                                @"caption": NSLocalizedString(@"homeTutoText2", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(536, 360, 200, 200)],
                                @"caption": NSLocalizedString(@"homeTutoText3", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(747, 360, 200, 200)],
                                @"caption": NSLocalizedString(@"homeTutoText4", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(930, 12, 78, 78)],
                                @"caption": NSLocalizedString(@"homeTutoText5", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(826, 12, 78, 78)],
                                @"caption": NSLocalizedString(@"homeTutoText6", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(16, 12, 78, 78)],
                                @"caption": NSLocalizedString(@"homeTutoText7", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(115, 12, 78, 78)],
                                @"caption": NSLocalizedString(@"homeTutoText8", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            ];
    
    DDCoachMarksView *coachMarksView = [[DDCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    
    [self.view addSubview:coachMarksView];
    [coachMarksView start];
}

- (void) quitButtonClicked {
    NSLog(@"quitButtonClicked");
    [self createCustomDialogWithTitle:@"exit" andMiddleText:@"ask_disconnect_profil" andButton1Text:@"yes" andButton2Text:@"no"];
    [self.dialogView setTarget:self forButton:1 action:@selector(actionDialogQuitButtonYes)];
    [self.dialogView setTarget:self forButton:2 action:@selector(actionDialogQuitButtonNo)];
    [self.view bringSubviewToFront:self.dialogView];
}

- (void)synchroTutoViewController {
    NSLog(@"synchroTutoViewController");
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(synchronisation.frame.size.width/2-131, dimensionAdapter.sizey150, 262, 262)],
                                @"caption": NSLocalizedString(@"synchroTutoText1", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(synchronisation.frame.size.width/2-131, dimensionAdapter.sizey350, 262, 262)],
                                @"caption": NSLocalizedString(@"synchroTutoText2", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            ];
    
    DDCoachMarksView *coachMarksView = [[DDCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    
    [self.view addSubview:coachMarksView];
    [coachMarksView start];
}

- (void) synchroButtonClicked {
    NSLog(@"synchroAndUpdateAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if (!synchronisation) {
        synchronisation = [[SynchronisationView alloc] initSynchronisationView];
        UIButton * tutoButton;
        tutoButton = [[UIButton alloc] initWithFrame:CGRectMake(synchronisation.frame.size.width*2/12, synchronisation.frame.size.height/7, synchronisation.frame.size.width/13, synchronisation.frame.size.width/13)];
        [tutoButton setImage:[UIImage imageNamed:@"button_tuto.png"] forState:UIControlStateNormal];
        [tutoButton addTarget:self action:@selector(synchroTutoViewController) forControlEvents:UIControlEventTouchUpInside];
        [synchronisation addSubview:tutoButton];
        [self.view addSubview:synchronisation];
    }
    else {
        [synchronisation setHidden:NO];
    }
}

/* click "yes" on the "play again dialog" */
- (void)actionDialogQuitButtonYes
{
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [self.dialogView setHidden:YES];
    ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson = nil;
    UIViewController *menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuViewController"];
    [self.navigationController pushViewController:menuViewController animated:YES];
}

/* click "no" on the "play again dialog" */
- (void)actionDialogQuitButtonNo
{
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [self.dialogView setHidden:YES];
}

- (void) actionDialogDeleteProfilButtonYes {
    NSLog(@"actionDialogDeleteProfilButtonYes");
    synchro = [[SynchronisationData alloc] initSynchronisationData];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [(SynchronisationData *)synchro startSynchronisationData:^(BOOL success){
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if (success) {
                    NSLog(@"success");
                    Person * person = [databaseManager getPersonWithId:currentPerson.personId];
                    if (person != nil) {
                        [self updatePersonInServer:person :YES];
                    }
                    else {
                        [STMUtilities showAlertDialogWithMessageKey:@"error_person_deleted_from_server" andDuration:2];
                        ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson = nil;
                        UIViewController *menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuViewController"];
                        [self.navigationController pushViewController:menuViewController animated:YES];
                    }
                }
                else {
                    NSLog(@"failure");
                }
            });
        }];
    });
}


- (void)createCustomDialogWithTitle:(NSString*) title andMiddleText:(NSString*) text andButton1Text:(NSString*)button1Text andButton2Text:(NSString*)button2Text
{
    [self createDialog:STMDialogTypeNormal];
    
    int button1Id = 1;
    int button2Id = 2;
    [self.dialogView setTitleUpText:NSLocalizedString(title, nil) andSize:70];
    [self.dialogView setTitleMiddleText:NSLocalizedString(text, nil)];
    
    [self.dialogView setupButtonFor:button1Id andText:NSLocalizedString(button1Text, nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
    [self.dialogView setupButtonFor:button2Id andText:NSLocalizedString(button2Text, nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
}

- (void)createDialog:(int)type
{
    self.dialogView = [[STMDialogView alloc]initWithType:type andStyle:2 andBackground:@"dialog.png"];
    [self.view addSubview:self.dialogView];
    [self.dialogView.buttonLeft.titleLabel setFont:[UIFont boldSystemFontOfSize:30]];
    [self.dialogView.buttonRight.titleLabel setFont:[UIFont boldSystemFontOfSize:30]];
}

- (void) paramButtonClicked {
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if (!self.paramDialogView) {
        //that should be stmdialogtypethreebuttons if wo should add modify button
        self.paramDialogView = [[STMDialogView alloc] initWithType:STMDialogTypeTwoButtons andStyle:2 andBackground:@"dialog.png"];
        // we comment the second button just for hidding modifing profil button and we could use it again in futur
        int button1Id = 1;
        int button2Id = 2;
        int button3Id = 3;
        //int button4Id = 4;
        [self.paramDialogView setupButtonFor:button1Id andText:NSLocalizedString(@"Consulter les séances", nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
        //[self.paramDialogView setupButtonFor:button2Id andText:NSLocalizedString(@"Modifier le profil", nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
        [self.paramDialogView setupButtonFor:button2Id andText:NSLocalizedString(@"Paramètres", nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
        [self.paramDialogView setupButtonFor:button3Id andText:NSLocalizedString(@"return", nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
        
        [self.paramDialogView setTarget:self forButton:1 action:@selector(actionCheckButton)];
        //[self.paramDialogView setTarget:self forButton:2 action:@selector(actionModifyButton)];
        [self.paramDialogView setTarget:self forButton:2 action:@selector(actionSettingButton)];
        [self.paramDialogView setTarget:self forButton:3 action:@selector(actionReturnButton)];
        [self.view addSubview:self.paramDialogView];
        [self.view bringSubviewToFront:self.paramDialogView];
    }
    else {
        [self.paramDialogView setHidden:NO];
    }
}

- (void) actionCheckButton {
    NSLog(@"actionCheckButton");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [paramDialogView setHidden:YES];
    UIViewController *dataViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"dataViewController"];
    [self.navigationController pushViewController:dataViewController animated:YES];
}

- (void) actionModifyButton {
    NSLog(@"actionModifyButton");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if (!dialogModifyProfil) {
        dialogModifyProfil = [[STMDialogCreateUser alloc] initWithStyle:2];
        // just for hidding button modify profil
        //[self.view addSubview:dialogModifyProfil];
        //[self.view bringSubviewToFront:dialogModifyProfil];
        [dialogModifyProfil.nameTextField addTarget:self action:@selector(moveViewWhenTypeBegins) forControlEvents:UIControlEventEditingDidBegin];
        [dialogModifyProfil.nameTextField addTarget:self action:@selector(moveViewWhenTypeEnds) forControlEvents:UIControlEventEditingDidEnd];
        [dialogModifyProfil.buttonSup addTarget:self action:@selector(deleteCurrentProfil) forControlEvents:UIControlEventTouchDown];
        [dialogModifyProfil.buttonLeft addTarget:self action:@selector(modifyCurrentProfil) forControlEvents:UIControlEventTouchDown];
        [dialogModifyProfil.buttonRight addTarget:self action:@selector(cancelDialogModify) forControlEvents:UIControlEventTouchDown];
        dialogModifyProfil.titleUp.text = NSLocalizedString(@"modify_profil", nil);
        [dialogModifyProfil.birthdateTextField addTarget:self action:@selector(pickerDateSelected:) forControlEvents:UIControlEventTouchDown];
    }
    else {
        dialogModifyProfil.hidden = NO;
        [dialogModifyProfil.firstNameTextField setEnabled:YES];
        [dialogModifyProfil.nameTextField setEnabled:YES];
        [dialogModifyProfil.birthdateTextField setEnabled:YES];
    }
    
    if (currentPerson != nil) {
        dialogModifyProfil.firstNameTextField.text = currentPerson.firstName;
        dialogModifyProfil.nameTextField.text = currentPerson.name;
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate * date = [formatter dateFromString:currentPerson.birthdate];
        NSDateFormatter * dateFormatUser = [[NSDateFormatter alloc]init];
        [dateFormatUser setDateFormat:NSLocalizedString(@"date_format", nil)];
        dialogModifyProfil.birthdateTextField.text = (currentPerson.birthdate == nil || [currentPerson.birthdate isEqualToString:@"(null)"]) ? @"" : [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"you_borned_at", nil),[dateFormatUser stringFromDate:date]];
        if ([currentPerson.sex isEqualToString:kMan]) {
            dialogModifyProfil.sexSegmentedControl.selectedSegmentIndex = 1;
        }
        else if ([currentPerson.sex isEqualToString:kWoman]) {
            dialogModifyProfil.sexSegmentedControl.selectedSegmentIndex = 0;
        }
        else {
            dialogModifyProfil.sexSegmentedControl.selectedSegmentIndex = 2;
        }
    }
}

- (void) pickerDateSelected: (id) sender {
    NSLog(@"showDatePicker");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    /* disable the UITextField disable the keyboard */
    [dialogModifyProfil.birthdateTextField setEnabled:NO];
    NSDate * date;
    if (pickerDate) {
        date = pickerDate.selectedDate;
    }
    else {
        NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"MM-dd-yyyy"];
        date = [formatter dateFromString:@"01-01-1930"];
    }
    pickerDate = [ActionSheetDatePicker showPickerWithTitle:nil datePickerMode:UIDatePickerModeDate  selectedDate:date target:self action:@selector(birthDateSelectedAction:) origin:sender];
    [pickerDate setCancelTarget:@selector(cancelSelectionBirthDateAction:)];
}

- (void) birthDateSelectedAction: (NSDate *) date {
    NSLog(@"birthDateSelectedAction date = %@", date);
    /* date format of the region of the user */
    NSDateFormatter * dateFormatUser = [[NSDateFormatter alloc]init];
    /* date format unified of the database */
    NSDateFormatter * dateFormatDatabase = [[NSDateFormatter alloc]init];
    [dateFormatUser setDateFormat:NSLocalizedString(@"date_format", nil)];
    [dateFormatDatabase setDateFormat:NSLocalizedString(@"yyyy-MM-dd", nil)];
    [dialogModifyProfil.birthdateTextField setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"you_borned_at", nil),[dateFormatUser stringFromDate:date]]];
    currentDateFormatDatebase = [dateFormatDatabase stringFromDate:date];
    [dialogModifyProfil.birthdateTextField setEnabled:YES];
}

- (void) cancelSelectionBirthDateAction: (id) sender {
    NSLog(@"cancelSelectionBirthDateAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [dialogModifyProfil.birthdateTextField setText:nil];
    currentDateFormatDatebase = nil;
    [dialogModifyProfil.birthdateTextField setEnabled:YES];
}

- (void) deleteCurrentProfil {
    NSLog(@"quitButtonClicked");
    [self createCustomDialogWithTitle:@"delete" andMiddleText:@"ask_delete_profil" andButton1Text:@"yes" andButton2Text:@"no"];
    [self.dialogView setTarget:self forButton:1 action:@selector(actionDialogDeleteProfilButtonYes)];
    [self.dialogView setTarget:self forButton:2 action:@selector(actionDialogQuitButtonNo)];
    [self.view bringSubviewToFront:self.dialogView];
}

- (void) modifyCurrentProfil {
    
    if ([STMUtilities isInternetConnected]) {
        NSString * firstName = [dialogModifyProfil.firstNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString * name = [dialogModifyProfil.nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString * birthday = [dialogModifyProfil.birthdateTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if ([firstName isEqualToString:@""]) {
            [STMUtilities showAlertDialogWithMessage:NSLocalizedString(@"please_enter_your_firstname", nil) andDuration:3];
        }
        else if ([name isEqualToString:@""]) {
            [STMUtilities showAlertDialogWithMessage:NSLocalizedString(@"please_enter_your_name", nil) andDuration:3];
        }
//        else if ([birthday isEqualToString:@""]) {
//            [STMUtilities showAlertDialogWithMessage:NSLocalizedString(@"please_choose_your_birth_date", nil) andDuration:3];
//        }
        else {
            NSString * sex = nil;
            if (dialogModifyProfil.sexSegmentedControl.selectedSegmentIndex == 0)
                sex = kWoman;
            else if (dialogModifyProfil.sexSegmentedControl.selectedSegmentIndex == 1)
                sex = kMan;
            Person * aPerson = [[Person alloc]initWithFirstName:firstName name:name birthdate:(!currentDateFormatDatebase ? currentPerson.birthdate : currentDateFormatDatebase)  sex:sex role:@"P"];
            aPerson.personId = currentPerson.personId;
            synchro = [[SynchronisationData alloc] initSynchronisationData];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [(SynchronisationData *)synchro startSynchronisationData:^(BOOL success){
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        if (success) {
                            NSLog(@"success");
                            Person * person = [databaseManager getPersonWithId:aPerson.personId];
                            if (person != nil) {
                                aPerson.serverId = person.serverId;
                                [self updatePersonInServer:aPerson :NO];
                            }
                            else {
                                [STMUtilities showAlertDialogWithMessageKey:@"error_person_deleted_from_server" andDuration:2];
                                ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson = nil;
                                UIViewController *menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuViewController"];
                                [self.navigationController pushViewController:menuViewController animated:YES];
                            }
                        }
                        else {
                            NSLog(@"failure");
                        }
                    });
                }];
            });
        }
    }
    else {
        [STMUtilities showAlertDialogWithMessageKey:@"need internet for this functions" andDuration:2];
    }
}

- (void) updatePersonInServer : (Person *) aPerson : (BOOL) isDelete {
    NSLog(@"updatePersonInServer");
    NSString * requestURL = [STMURLFactory getURLUpdatePerson:aPerson :isDelete];
    NSLog(@"requestURL : %@", requestURL);
    [requestManager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
        NSLog(@"updatePersonInServer - server response for device identification: %@", responseObject);
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSString * error = [responseObject objectForKey:kJsonParamError];
            if (error != nil && ![error isEqualToString:@""]) {
                if ([error isEqualToString:@"duplicatePerson"]) {
                    [STMUtilities showAlertDialogWithMessageKey:@"error_duplicate_person" andDuration:2];
                }
                else {
                    [STMUtilities showAlertDialogWithMessageKey:@"error_undefined" andDuration:2];
                }
            }
            else {
                NSLog(@"UpdatePerson : person updated");
                if (isDelete) {
                    [databaseManager deletePersonWithId:aPerson.personId];
                    [STMUtilities showAlertDialogWithMessageKey:@"person_deleted" andDuration:2];
                }
                else {
                    [databaseManager updatePerson:aPerson];
                    [STMUtilities showAlertDialogWithMessageKey:@"person_modif" andDuration:2];
                }
                ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson = nil;
                UIViewController *menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuViewController"];
                    [self.navigationController pushViewController:menuViewController animated:YES];
            }
        }
    } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
        NSLog(@"ERROR - updatePersonInServer error = %@, for URL: %@", error, requestURL);
    }];

}

- (void) cancelDialogModify {
    NSLog(@"actionSettingButton");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    dialogModifyProfil.hidden = YES;
    [dialogModifyProfil.firstNameTextField setEnabled:NO];
    [dialogModifyProfil.nameTextField setEnabled:NO];
    [dialogModifyProfil.birthdateTextField setEnabled:NO];
}

- (void)moveViewWhenTypeBegins {
    self.dialogModifyProfil.center = CGPointMake(self.dialogModifyProfil.center.x, self.dialogModifyProfil.center.y - dimensionAdapter.sizey100);
}

- (void)moveViewWhenTypeEnds {
    self.dialogModifyProfil.center = CGPointMake(self.dialogModifyProfil.center.x, self.dialogModifyProfil.center.y + dimensionAdapter.sizey100);
}

- (void) actionSettingButton {
    NSLog(@"actionSettingButton");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if (!dialogSetting) {
        dialogSetting = [[STMDialogView alloc] initWithType:STMDialogTypePickerWithTwoButtons andStyle:2 andBackground:@"dialog.png"];
        [self.view addSubview:dialogSetting];
        [dialogSetting setupButtonFor:1 andText:NSLocalizedString(@"save", nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
        [dialogSetting setupButtonFor:2 andText:NSLocalizedString(@"cancel", nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
    
    
        [dialogSetting.buttonLeft.titleLabel setFont:[UIFont boldSystemFontOfSize:25]];
        [dialogSetting.buttonRight.titleLabel setFont:[UIFont boldSystemFontOfSize:25]];
        [dialogSetting setTarget:self forButton:1 action:@selector(selectedMode)];
        [dialogSetting setTarget:self forButton:2 action:@selector(cancelSelection)];
        [dialogSetting setTitleUpText:NSLocalizedString(@"Mode de sélection des fiches", nil)];
        [dialogSetting setTitleLeftText:NSLocalizedString(@"Mode", nil)];
        [dialogSetting setTextFieldPickerPlaceholderText:NSLocalizedString(@"Veuillez choisir le mode", nil)];
        [dialogSetting.textFieldPicker addTarget:self action:@selector(showModePickerAction:) forControlEvents:UIControlEventTouchDown];
    }
    else if ([dialogSetting isHidden]) {
        [dialogSetting setHidden:NO];
        [self.view bringSubviewToFront:dialogSetting];
        if (![dialogSetting.textFieldPicker isEnabled]) {
            [dialogSetting.textFieldPicker setEnabled:YES];
        }
    }
}

- (void) actionReturnButton {
    NSLog(@"actionReturnButton");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [self.paramDialogView setHidden:YES];
}

- (void) showModePickerAction:(id)sender {
    NSLog(@"showModePickerAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    listModes = [NSLocalizedString(@"listModes", nil) componentsSeparatedByString:@"|"];
    
    if ([listModes count] != 0) {
        [dialogSetting.textFieldPicker setEnabled:NO];
        
        if (indexMode == -1) {
            indexMode = 0;
        }
        pickerModeSelection = [ActionSheetStringPicker showPickerWithTitle:nil rows:listModes initialSelection:indexMode target:self successAction:@selector(modeSelectedAction:) cancelAction:@selector(cancelModeSelectionAction:) origin:dialogSetting.textFieldPicker];
    }
    else {
        [STMUtilities showAlertDialogWithMessageKey:@"mode dosn't exist" andDuration:2];
    }
}

- (void) modeSelectedAction : (NSNumber *)selectedIndex {
    NSLog(@"profileSelectedAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    indexMode = [selectedIndex intValue];
    if (indexMode < 0 || indexMode >= listModes.count) {
        indexMode = 0;
    }
    [dialogSetting.textFieldPicker setEnabled:YES];
    dialogSetting.textFieldPicker.text = [listModes objectAtIndex:indexMode];
}

- (void) cancelModeSelectionAction:(id)sender {
    NSLog(@"cancelProfileSelectionAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [dialogSetting.textFieldPicker setEnabled:YES];
    dialogSetting.textFieldPicker.text = nil;
    indexMode = -1;
}

- (void) selectedMode {
    NSLog(@"connectToProfileAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if (indexMode >= 0) {
        [[NSUserDefaults standardUserDefaults] setObject:[listModes objectAtIndex:indexMode] forKey:kTypeModeSelection];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:[listModes objectAtIndex:0] forKey:kTypeModeSelection];
    }
    [dialogSetting.textFieldPicker setEnabled:YES];
    [dialogSetting setHidden:YES];
}

- (void) cancelSelection {
    NSLog(@"cancelSelection");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    indexMode = -1;
    [[NSUserDefaults standardUserDefaults] setObject:[listModes objectAtIndex:0] forKey:kTypeModeSelection];
    [dialogSetting setHidden:YES];
    [dialogSetting.textFieldPicker setEnabled:NO];
    [dialogSetting.textFieldPicker setText:nil];
}

@end
