//
//  ScreenSlideSheetViewController.m
//  e-souvenir
//
//  Created by Dynseo on 23/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "AppDelegate.h"
#import "ScreenSlideSheetViewController.h"
#import "Sheet.h"
#import "DatabaseManager.h"
#import "SessionSheet.h"
#import "Constants.h"
#import "DDCoachMarksView.h"
#import <StimartLib/StimartLib.h>

#define kNumRoundsAuthorizedForVisitor 5
@interface ScreenSlideSheetViewController ()

@end

@implementation ScreenSlideSheetViewController {
    DatabaseManager * databaseManager;
    AVAudioPlayer *audioPlayer;
    Sheet * currentSheet;
    UIButton * buttonPlay;
    YTPlayerView * youtubeViewController;
    int sessionId;
    int previousPage;
    NSMutableArray * mSessionSheets;
    STMDialogView * legendDialog;
    UITextView * legendTextView;
    BOOL isChanged;
    NSDate * timeForSession;
    NSDate * timeForSessionSheet;
    
    
    UITapGestureRecognizer *singleTap;
    BOOL isFullScreen;
    UIImageView * imageFullScreen;
    
    
    UIView * popup;
}

@synthesize listSheetsSelected, homeButton, tutoButton, likeButton, unlikeButton, questionButton, legendButton, sheetView, sheetPageControl, dialogQuestion, question1Label, question2Label, question3Label, quitDialogQuestionButton, scroll,sheetLabel, dialogView,descriptionButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    timeForSession = [NSDate date];
    timeForSessionSheet = [NSDate date];
    databaseManager = [DatabaseManager getSharedInstance];
    NSLog(@"currentSessionId : %d" ,[databaseManager getLastSessionId]);
    sessionId = [databaseManager getLastSessionId] + 1;
    mSessionSheets = [[NSMutableArray alloc] init];
    NSLog(@"nombre sheets were selected : %lu", (unsigned long)listSheetsSelected.count);
    previousPage = 0;
    currentSheet = [listSheetsSelected objectAtIndex:0];
    
//    if (![self isAuthorized]) {
//        [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
//        ((AppDelegate *)[[UIApplication sharedApplication]delegate]).onReturn = ON_RETURN_NOT_AUTHORIZED;
//        ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson = nil;
//        UIViewController *menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuViewController"];
//        [self.navigationController pushViewController:menuViewController animated:YES];
//    }
//    else {
//        ((AppDelegate *)[[UIApplication sharedApplication]delegate]).onReturn = ON_RETURN_AUTHORIZED;
//        [self initializeView];
//    }
    
    isFullScreen = false;
    singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [singleTap setNumberOfTapsRequired:1];
    
    
    [self initializeView];
}

-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    [sheetPageControl setTransform:CGAffineTransformMakeScale(2.0, 2.0)];
}

- (void) initializeView {

    imageFullScreen = [[UIImageView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:imageFullScreen];
    [imageFullScreen setHidden:YES];
    homeButton.tag = 1;
    tutoButton.tag = 2;
    questionButton.tag = 3;
    legendButton.tag = 4;
    likeButton.tag = 5;
    unlikeButton.tag = 6;
    quitDialogQuestionButton.tag = 7;
    descriptionButton.tag = 8;
    dialogQuestion.hidden = YES;
    descriptionButton.hidden = YES;
    sheetLabel.text = currentSheet.name;
    
    [sheetPageControl.layer setCornerRadius:sheetPageControl.frame.size.height*0.5];
    sheetPageControl.currentPage = 0;
    sheetPageControl.numberOfPages = listSheetsSelected.count;
    NSLog(@"loadView");
    
    scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 600, 450)];
    scroll.center = CGPointMake(sheetView.frame.size.width/2, sheetView.frame.size.height/2);
    scroll.pagingEnabled = YES;
    
    scroll.userInteractionEnabled = YES;
    [scroll addGestureRecognizer:singleTap];
    
    for (int i = 0 ; i < listSheetsSelected.count ; i++) {
        Sheet * aSheet = [listSheetsSelected objectAtIndex:i];
        [scroll addSubview:[self viewForAddToScrollView:aSheet:i]];
    }
    
    
    scroll.contentSize = CGSizeMake(listSheetsSelected.count * 600, 450);
    [scroll setContentOffset:CGPointMake(0, -self.scroll.contentInset.top) animated:YES];
    self.automaticallyAdjustsScrollViewInsets = false;
    scroll.contentInset = UIEdgeInsetsZero;
    scroll.scrollIndicatorInsets = UIEdgeInsetsZero;
    scroll.contentOffset = CGPointMake(0.0, 0.0);
    scroll.delegate = self;
    [self.sheetView addSubview:scroll];
    
    if ([currentSheet.like isEqualToString:@"true"]) {
        [likeButton setBackgroundImage:[UIImage imageNamed:@"icone_like_on.png"] forState:UIControlStateNormal];
    }
    if (currentSheet.getResourceType == AUDIO) {
        descriptionButton.hidden = NO;
        [self initButtonPlay];
    }
    else if (currentSheet.getResourceType == VIDEO) {
        [self initYoutubeView];
    }
}

//- (BOOL)isAuthorized
//{
//    BOOL isAuthorized = YES;
//    if ([Account isVisitMode] || ![Subscription isSubscriptionValid]) {
//        int resultCount = 0;
//        resultCount = [databaseManager getSessionsWithoutServerId].count;
//        NSLog(@"resultCount : %d",resultCount);
//        if (resultCount >= kNumRoundsAuthorizedForVisitor) {
//            isAuthorized = NO;
//        }
//        NSLog(@"isAuthorized : %d", isAuthorized);
//    }
//    return isAuthorized;
//}

- (UIView *) viewForAddToScrollView : (Sheet *) aSheet : (int) pos {
    UIView * aView = [[UIView alloc] initWithFrame:CGRectMake(600*pos, 0, 600, 450)];
    UIImageView * image;
    UIImageView * imageBackground;
    if (aSheet.getResourceType == IMAGE_V) {
        imageBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 295, 360)];
        imageBackground.image = [UIImage imageNamed:@"picture_background_vertical.png"];
        image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,258,325)];
    }
    else if (aSheet.getResourceType == IMAGE_V_B) {
        imageBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 360)];
        imageBackground.image = [UIImage imageNamed:@"picture_background.png"];
        image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,285,325)];
    }
    else if (aSheet.getResourceType == IMAGE_S) {
        imageBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 360, 360)];
        imageBackground.image = [UIImage imageNamed:@"picture_background.png"];
        image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,325)];
    }
    else if (aSheet.getResourceType == IMAGE_H) {
        imageBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 570, 360)];
        imageBackground.image = [UIImage imageNamed:@"picture_background.png"];
        image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,500,325)];
    }
    else if (aSheet.getResourceType == IMAGE_H_B) {
        imageBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 468, 360)];
        imageBackground.image = [UIImage imageNamed:@"picture_background.png"];
        image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,415,330)];
    }
    else {
        imageBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 600, 400)];
        if (aSheet.getResourceType == VIDEO) {
            imageBackground.image = [UIImage imageNamed:@"cinema_background.png"];
        }
        else if (aSheet.getResourceType == AUDIO) {
            imageBackground.image = [UIImage imageNamed:@"music_background.png"];
        }
        else {
            imageBackground.image = [UIImage imageNamed:@"picture_background.png"];
            image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,525,360)];
        }
        
    }
    image.center = CGPointMake(imageBackground.frame.size.width/2 + 3, imageBackground.frame.size.height/2);
    imageBackground.center = CGPointMake(scroll.frame.size.width/2, scroll.frame.size.height/2 - 20);
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * fileImage = [NSString stringWithFormat:@"%@/%@", path, aSheet.resourceName];
    [image setImage:[UIImage imageNamed:fileImage]];
    [imageBackground addSubview:image];
    
    UILabel * dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, imageBackground.frame.size.height - 65, 80, 40)];
    [dateLabel setBackgroundColor:[ColorTools colorFromHexString:@"#2AA6A0"]];
    dateLabel.textColor = UIColor.whiteColor;
    dateLabel.textAlignment = NSTextAlignmentCenter;
    [imageBackground addSubview:dateLabel];
    if (aSheet.date != nil && aSheet.date.length > 4)
        dateLabel.text = [aSheet.date substringToIndex:4];
    else
        dateLabel.hidden = YES;
    [aView addSubview:imageBackground];
    
    return aView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)onClick:(UIButton *)sender {
    switch (sender.tag) {
        case 1:
            [self quitButtonClicked];
            break;
        case 2:
            [self startTutoViewController];
            break;
        case 3:
            [self questionButtonClicked];
            break;
        case 4:
            [self legendButtonClicked];
            break;
        case 5:
            [self likeButtonClicked];
            break;
        case 6:
            [self unlikeButtonClicked];
            break;
        case 7:
            [self quitDialogQuestionButtonClicked];
            break;
        case 8:
            [self showInfoCurrentSheet];
            break;
            
        default:
            break;
    }
}

- (void) questionButtonClicked {
    dialogQuestion.hidden = NO;
    
    question1Label.textColor = [UIColor blackColor];
    question2Label.textColor = [UIColor blackColor];
    question3Label.textColor = [UIColor blackColor];
    
    question1Label.text = [currentSheet.questions objectAtIndex:0];
    question2Label.text = [currentSheet.questions objectAtIndex:1];
    question3Label.text = [currentSheet.questions objectAtIndex:2];
}

- (void) legendButtonClicked {
    NSLog(@"legendButtonClicked");
    [self createLegendDialog];
}

- (void) likeButtonClicked {
    NSLog(@"likeButtonClicked");
    currentSheet.like = @"true";
    [likeButton setBackgroundImage:[UIImage imageNamed:@"icone_like_on.png"] forState:UIControlStateNormal];
    [unlikeButton setBackgroundImage:[UIImage imageNamed:@"icone_unlike.png"] forState:UIControlStateNormal];
}

- (void) unlikeButtonClicked {
    NSLog(@"unlikeButtonClicked");
    currentSheet.like = @"false";
    [unlikeButton setBackgroundImage:[UIImage imageNamed:@"icone_unlike_on.png"] forState:UIControlStateNormal];
    [likeButton setBackgroundImage:[UIImage imageNamed:@"icone_like.png"] forState:UIControlStateNormal];
}

- (void) quitDialogQuestionButtonClicked {
    dialogQuestion.hidden = YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    if (touch.view != dialogQuestion && dialogQuestion) {
        dialogQuestion.hidden = YES;
//        [dialogQuestion removeFromSuperview];
//        dialogQuestion = nil;
    }
}

- (void) startTutoViewController {
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(205, 100, 620, 620)],
                                @"caption": NSLocalizedString(@"screenSlideTutoText1", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(927, 220, 78, 78)],
                                @"caption": NSLocalizedString(@"screenSlideTutoText2", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(927, 488, 78, 78)],
                                @"caption": NSLocalizedString(@"screenSlideTutoText3", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(385, 660, 78, 78)],
                                @"caption": NSLocalizedString(@"screenSlideTutoText4", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(581, 662, 78, 78)],
                                @"caption": NSLocalizedString(@"screenSlideTutoText5", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(16, 16, 78, 78)],
                                @"caption": NSLocalizedString(@"screenSlideTutoText6", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:CGRectMake(16, 102, 78, 78)],
                                @"caption": NSLocalizedString(@"screenSlideTutoText7", nil),
                                @"shape": @"circle",
                                @"font": [UIFont boldSystemFontOfSize:14.0]
                                }
                            ];
    
    DDCoachMarksView *coachMarksView = [[DDCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    
    [self.view addSubview:coachMarksView];
    [coachMarksView start];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self loadVisiblePages];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSLog(@"scrollViewDidEndScrollingAnimation");
    NSLog(@"page page page : %ld", (long)self.sheetPageControl.currentPage);
    
    currentSheet = [listSheetsSelected objectAtIndex:self.sheetPageControl.currentPage];
    if (currentSheet.like != nil && [currentSheet.like isEqualToString:@"true"]) {
        [likeButton setBackgroundImage:[UIImage imageNamed:@"icone_like_on.png"] forState:UIControlStateNormal];
        [unlikeButton setBackgroundImage:[UIImage imageNamed:@"icone_unlike.png"] forState:UIControlStateNormal];
    }
    else if (currentSheet.like != nil && [currentSheet.like isEqualToString:@"false"]) {
        [likeButton setBackgroundImage:[UIImage imageNamed:@"icone_like.png"] forState:UIControlStateNormal];
        [unlikeButton setBackgroundImage:[UIImage imageNamed:@"icone_unlike_on.png"] forState:UIControlStateNormal];
    }
    else {
        [likeButton setBackgroundImage:[UIImage imageNamed:@"icone_like.png"] forState:UIControlStateNormal];
        [unlikeButton setBackgroundImage:[UIImage imageNamed:@"icone_unlike.png"] forState:UIControlStateNormal];
    }
    
    sheetLabel.text = currentSheet.name;
    if (!dialogQuestion.hidden) {
        [self questionButtonClicked];
    }

    if (currentSheet.getResourceType == AUDIO) {
        youtubeViewController.hidden = YES;
        descriptionButton.hidden = NO;
        [self initButtonPlay];
        audioPlayer = nil;
    }
    else {
        descriptionButton.hidden = YES;
        buttonPlay.hidden = YES;
        if (currentSheet.getResourceType == VIDEO) {
            [self initYoutubeView];
        }
        else {
            youtubeViewController.hidden = YES;
        }
    }
    [self saveSessionSheetToBDD];
    previousPage = (int) self.sheetPageControl.currentPage;
}

- (void) saveSessionSheetToBDD {
    NSLog(@"saveSessionSheetToBDD : %d", sessionId);
    Sheet * aSheet = [listSheetsSelected objectAtIndex:previousPage];
    SessionSheet * aSessionSheet = [[SessionSheet alloc] init];
    if (aSheet.like != nil) {
        aSessionSheet.like = aSheet.like;
    }
    if (aSheet.legend != nil) {
        aSessionSheet.legend = aSheet.legend;
    }
    aSessionSheet.sessionSheetTimer = [NSString stringWithFormat:@"%d", (int) [[NSDate date]timeIntervalSinceDate:timeForSessionSheet]];
    aSessionSheet.sheetId = aSheet.sheetId;
    aSessionSheet.sessionId = sessionId;
    [databaseManager insertSessionSheet:aSessionSheet];
    timeForSessionSheet = [NSDate date];
}

- (void) saveSessionToBDD {
    NSLog(@"saveSessionToBDD");
    Person * currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    NSString * typeSession = [[NSUserDefaults standardUserDefaults]objectForKey:kTypeSession];
    NSString * duration = [NSString stringWithFormat:@"%d", (int) [[NSDate date]timeIntervalSinceDate:timeForSession]];
    Session * aSession = [[Session alloc] initSessionWithPersonId:currentPerson.personId andTimer:duration andSatisfaction:@"Satisfaction" andType:typeSession];
    [databaseManager insertSession:aSession];
}

- (void)loadVisiblePages {
    CGFloat pageWidth = scroll.frame.size.width;
    NSInteger page = (NSInteger)floor((self.scroll.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
    
    if (page < listSheetsSelected.count) {
        self.sheetPageControl.currentPage = page;
    }
}

- (void) initButtonPlay {
    if (!buttonPlay) {
        buttonPlay = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
        buttonPlay.center = CGPointMake(300, 350);
    }
    buttonPlay.hidden = NO;
    [buttonPlay setBackgroundImage:[UIImage imageNamed:@"icone_play.png"] forState:UIControlStateNormal];
    [sheetView addSubview:buttonPlay];
    [buttonPlay addTarget:self action:@selector(buttonPlayClicked:) forControlEvents:UIControlEventTouchUpInside];
    [sheetView bringSubviewToFront:buttonPlay];
}
#pragma mark - audio player delegate:
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    [self changePlayerButtonToPlay];
}

- (void)buttonPlayClicked:(id) sender {
    NSLog(@"buttonPlayClicked");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    NSString  *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * fileAudio = [NSString stringWithFormat:@"%@/%@", path, currentSheet.resourceName];
    NSURL * urlAudio = [NSURL fileURLWithPath:fileAudio];
    NSLog(@"audio file: %@", fileAudio);
    if (audioPlayer == nil) {
        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:urlAudio error:nil];
        [audioPlayer prepareToPlay];
        [audioPlayer setVolume:1];
        [audioPlayer setNumberOfLoops:0];
        audioPlayer.delegate = self;
        [self changePlayerButtonToPause];
        [audioPlayer play];
    }
    
    else {
        if (audioPlayer.isPlaying) {
            [audioPlayer pause];
            [self changePlayerButtonToPlay];
        }
        else {
            [audioPlayer play];
            [self changePlayerButtonToPause];
        }
    }
}

- (void)changePlayerButtonToPlay
{
    if (buttonPlay) {
        [buttonPlay setBackgroundImage:[UIImage imageNamed:@"icone_play.png"] forState:UIControlStateNormal];
    }
}

- (void)changePlayerButtonToPause
{
    if (buttonPlay) {
        [buttonPlay setBackgroundImage:[UIImage imageNamed:@"icone_stop.png"] forState:UIControlStateNormal];
    }
}

- (void) initYoutubeView {
    if (!youtubeViewController) {
        youtubeViewController = [[YTPlayerView alloc] initWithFrame:CGRectMake(0,0,570,300)];
        youtubeViewController.center = CGPointMake(300, 170);
    }
    youtubeViewController.hidden = NO;
    [sheetView addSubview:youtubeViewController];
    [sheetView bringSubviewToFront:youtubeViewController];
    [youtubeViewController loadWithVideoId:currentSheet.resourceName];
    NSLog(@"initYoutubeView : %@", currentSheet.resourceName);
}


- (void) createLegendDialog {
    NSLog(@"currentSheetLegend : %@", currentSheet.legend);
    if (!legendDialog) {
        legendDialog = [[STMDialogView alloc]initWithType:STMDialogTypeNormal andStyle:2 andBackground:@"dialog.png"];
        [self.view addSubview:legendDialog];
        [legendDialog.buttonLeft.titleLabel setFont:[UIFont boldSystemFontOfSize:30]];
        [legendDialog.buttonRight.titleLabel setFont:[UIFont boldSystemFontOfSize:30]];
        
        legendTextView = [[UITextView alloc] initWithFrame:legendDialog.titleMiddle.frame];
        legendTextView.delegate = self;
        [[legendTextView layer] setBorderColor:[[UIColor grayColor] CGColor]];
        [[legendTextView layer] setBorderWidth:2.3];
        [[legendTextView layer] setCornerRadius:15];
        [legendTextView setFont:[UIFont systemFontOfSize:40]];
        legendTextView.textContainer.maximumNumberOfLines = 10;
        legendTextView.center = legendDialog.titleMiddle.center;
        [legendDialog addSubview:legendTextView];
        [legendDialog setTitleUpText:NSLocalizedString(@"La légende", nil) andSize:70];
    }
    legendDialog.hidden = NO;
    legendTextView.hidden = YES;
    legendTextView.text = (currentSheet.legend  != nil && [currentSheet.legend isEqualToString:@""]) ? currentSheet.legend : nil;
    legendDialog.titleMiddle.hidden = NO;
    if (currentSheet.legend == nil || [currentSheet.legend isEqualToString:@""]) {
        [legendDialog setTitleMiddleText:NSLocalizedString(@"Aucune légende à afficher", nil)];
        [legendDialog setupButtonFor:1 andText:NSLocalizedString(@"Ajouter une légende", nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
    }
    else {
        [legendDialog setTitleMiddleText:currentSheet.legend];
        [legendDialog setupButtonFor:1 andText:NSLocalizedString(@"Mofifier la légende", nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
    }
    
    [legendDialog setupButtonFor:2 andText:NSLocalizedString(@"Retour", nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
    [legendDialog setTarget:self forButton:1 action:@selector(actionAddNewLegend)];
    [legendDialog setTarget:self forButton:2 action:@selector(actionReturn)];
    [self.view bringSubviewToFront:legendDialog];
}

- (void) actionAddNewLegend {
    NSLog(@"actionAddNewLegend");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    legendDialog.titleMiddle.hidden = YES;
    legendTextView.hidden = NO;
    legendTextView.text = (currentSheet.legend  != nil && ![currentSheet.legend isEqualToString:@""]) ? currentSheet.legend : nil;
    [legendDialog setupButtonFor:1 andText:NSLocalizedString(@"add", nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
    [legendDialog setTarget:self forButton:1 action:@selector(actionUpdateLegend)];
}


-(void)textViewDidBeginEditing:(UITextView *)textView {
    legendDialog.center = CGPointMake(legendDialog.center.x, legendDialog.center.y - 100);
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    legendDialog.center = CGPointMake(legendDialog.center.x, legendDialog.center.y + 100);
}

- (void) actionUpdateLegend {
    NSLog(@"actionUpdateLegend");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    currentSheet.legend = (legendTextView.text == nil || [legendTextView.text isEqualToString:@""]) ? nil : legendTextView.text;
    [legendDialog setHidden:YES];
}

- (void) actionReturn {
    NSLog(@"actionReturn");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [legendDialog setHidden:YES];
}

- (void) quitButtonClicked {
    NSLog(@"quitButtonClicked");
    
    [self createCustomDialogWithTitle:@"quit?" andMiddleText:@"ask_quit" andButton1Text:@"yes" andButton2Text:@"no"];
    [self.dialogView setTarget:self forButton:1 action:@selector(actionDialogQuitButtonYes)];
    [self.dialogView setTarget:self forButton:2 action:@selector(actionDialogQuitButtonNo)];
    [self.view bringSubviewToFront:self.dialogView];
}

/* click "yes" on the "play again dialog" */
- (void)actionDialogQuitButtonYes
{
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    if (youtubeViewController) {
        [youtubeViewController stopVideo];
    }
    if (audioPlayer) {
        [audioPlayer stop];
        audioPlayer = nil;
    }
    [self saveSessionSheetToBDD];
    [self saveSessionToBDD];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/* click "no" on the "play again dialog" */
- (void)actionDialogQuitButtonNo
{
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [self.dialogView setHidden:YES];
}


- (void)createCustomDialogWithTitle:(NSString*) title andMiddleText:(NSString*) text andButton1Text:(NSString*)button1Text andButton2Text:(NSString*)button2Text
{
    [self createDialog:STMDialogTypeNormal];
    
    int button1Id = 1;
    int button2Id = 2;
    [self.dialogView setTitleUpText:NSLocalizedString(title, nil) andSize:70];
    [self.dialogView setTitleMiddleText:NSLocalizedString(text, nil)];
    
    [self.dialogView setupButtonFor:button1Id andText:NSLocalizedString(button1Text, nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
    [self.dialogView setupButtonFor:button2Id andText:NSLocalizedString(button2Text, nil) andColor:@"#c3e6ec" andHighlight:@"#b4d5da"];
}

- (void)createDialog:(int)type
{
    self.dialogView = [[STMDialogView alloc]initWithType:type andStyle:2 andBackground:@"dialog.png"];
    [self.view addSubview:self.dialogView];
    [self.dialogView.buttonLeft.titleLabel setFont:[UIFont boldSystemFontOfSize:30]];
    [self.dialogView.buttonRight.titleLabel setFont:[UIFont boldSystemFontOfSize:30]];
}

-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    NSLog(@"image clicked");
    if (currentSheet.getResourceTypeSimple == IMAGE) {
        if (!isFullScreen) {
            [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
                [UIView transitionWithView:imageFullScreen
                                  duration:0.4
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    imageFullScreen.hidden = NO;
                                }
                                completion:NULL];
                
                
                NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString * fileImage = [NSString stringWithFormat:@"%@/%@", path, currentSheet.resourceName];
                [imageFullScreen setImage:[UIImage imageNamed:fileImage]];
                imageFullScreen.backgroundColor = [UIColor blackColor];
                imageFullScreen.contentMode = UIViewContentModeScaleAspectFit;
                scroll.userInteractionEnabled = NO;
                imageFullScreen.userInteractionEnabled = YES;
                [imageFullScreen addGestureRecognizer:singleTap];
            }completion:^(BOOL finished){
                isFullScreen = true;
            }];
            return;
        } else {
            [UIView animateWithDuration:0.5 delay:0 options:0 animations:^{
                scroll.userInteractionEnabled = YES;
                [scroll addGestureRecognizer:singleTap];
                imageFullScreen.userInteractionEnabled = NO;
                [imageFullScreen setHidden:YES];
            }completion:^(BOOL finished){
                isFullScreen = false;
            }];
            return;
        }
    }
    return;
}

- (void) showInfoCurrentSheet {
    NSLog(@"showInfoCurrentSheet");
    //[self initPopUpView];
    NSLog(@"current DescriptionSheet : %@", currentSheet.description);
    if (currentSheet.description != nil && ![currentSheet.description isEqualToString:@""] && ![currentSheet.description isEqualToString:@"(null)"]) {
        [STMUtilities showAlertDialogWithMessage:currentSheet.description andDuration:3];
    }
}

- (void) initPopUpView {
    popup.alpha = 0;
    popup.frame = CGRectMake (160, 240, 0, 0);
    [self.view addSubview:popup];
}

- (void) animatePopUpShow {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(initPopUpView)];
    
    popup.alpha = 1;
    popup.backgroundColor = [UIColor blackColor];
    popup.frame = CGRectMake (20, 40, 300, 400);
    
    [UIView commitAnimations];
}

@end
