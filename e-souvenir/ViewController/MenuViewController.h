//
//  ViewController.h
//  e-souvenir
//
//  Created by Dynseo on 18/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StimartLib/StimartLib.h>
#import "SynchronisationView.h"
#import "SettingViewController.h"
#import "AboutUsViewController.h"
#import "DialogIdentification.h"

@interface MenuViewController : UIViewController <STMDialogIdentificationDelegate, STMDialogViewDelegate>

@property (nonatomic) DialogIdentification * dialogIdentification;
@property (nonatomic) SynchronisationView * synchronisation;
@property (nonatomic) SettingViewController * dialogSetting;
@property (nonatomic) AboutUsViewController * dialogAboutUs;

@property (nonatomic) MBProgressHUD *hud;

/* create profile view */
@property (nonatomic) IBOutlet UIView * viewCreateProfile;
@property (strong, nonatomic) IBOutlet UIView *dialogViewCreateProfil;

@property (strong, nonatomic) IBOutlet UIButton *buttonTuto;
/* button "save profile" on the create profile view */
@property (nonatomic) IBOutlet STMButton * buttonCreateProfileSave;
/* button "cancel profile" on the profile view */
@property (nonatomic) IBOutlet STMButton * buttonCreateProfileCancel;
/* main label on the create profile view */
@property (nonatomic) IBOutlet UILabel * labelCreateProfile;
/* switch to choose sex on the create profile view */
@property (nonatomic) IBOutlet UISegmentedControl *sexSegmentedControl;
/* text field for entering first name on the create profile view */
@property (nonatomic) IBOutlet UITextField * firstnameTextField;
/* text field for entering name on the create profile view */
@property (nonatomic) IBOutlet UITextField * nameTextField;
/* text field for choosing date of birth on the create profile view */
@property (nonatomic) IBOutlet UITextField * dateOfBirthTextField;
@property (strong, nonatomic) IBOutlet UIButton *buttonPurchase;
@property (strong, nonatomic) IBOutlet UIButton *buttonChooseProfil;
@property (strong, nonatomic) IBOutlet UIButton *buttonCreateProfil;


@property (nonatomic) ActionSheetStringPicker * pickerProfile;

- (IBAction)createProfilToDababase:(id)sender;
- (IBAction)cancelCreateProfil:(id)sender;
- (IBAction)showDatePicker:(id)sender;
- (IBAction)chooseProfilAction:(id)sender;
- (IBAction)createProfilAction:(id)sender;
- (IBAction)startTutoViewController:(id)sender;
- (IBAction)buyAction:(id)sender;
- (IBAction)settingAction:(id)sender;
- (IBAction)aboutUsAction:(id)sender;

@property (nonatomic) STMDialogView * dialogChooseProfile;
@property (nonatomic) STMDialogView * dialogAskForAccountCreation;
@property (nonatomic) STMDialogView * dialogCreateOrConnectInstitution;
@property (nonatomic) STMDialogCreateInstitution * dialogCreateInstitution;
@property (nonatomic) STMDialogCreateProfessional * dialogCreateProfessional;
@property (nonatomic) STMDialogView * dialogGameBlocked;
- (IBAction)synchroAndUpdateAction:(id)sender;
@end

