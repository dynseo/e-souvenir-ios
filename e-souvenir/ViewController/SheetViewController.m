//
//  SheetViewController.m
//  e-souvenir
//
//  Created by Dynseo on 18/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//
#import "Constants.h"
#import "SheetViewController.h"
#import "SheetViewCell.h"
#import "Sheet.h"
#import "ScreenSlideSheetViewController.h"
#import "DDCoachMarksView.h"
#import "DatabaseManager.h"

//@import Floaty;

@interface SheetViewController ()

@end

@implementation SheetViewController {
    NSArray * allTypes;
    int numberRows;
    NSMutableArray * listSheetsForShow;
    NSMutableArray * listSheetsSelected ;
    int nbFiles;
    DatabaseManager * databaseManager;
    bool isSelected;
}

@synthesize listSheets, pickerFileType, textFieldPicker, filesCollectionView, buttonHome, buttonTuto, buttonStart, nbFilesPicked, explanationLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    buttonHome.tag = 1;
    buttonTuto.tag = 2;
    buttonStart.tag = 3;
    [buttonStart setEnabled:NO];
    nbFiles = 0;
    nbFilesPicked.text = [NSString stringWithFormat:@"%d", nbFiles];
    [self.filesCollectionView registerNib:[UINib nibWithNibName:@"SheetViewCell" bundle:nil] forCellWithReuseIdentifier:@"sheetViewCell"];
    numberRows = 4;
    allTypes = [NSLocalizedString(@"typeFile", nil) componentsSeparatedByString:@"|"];
    NSLog(@"nombre sheets : %lu", (unsigned long)listSheetsForShow.count);
    textFieldPicker.textAlignment = NSTextAlignmentCenter;
    textFieldPicker.text = [allTypes objectAtIndex:0];
    
    listSheetsForShow = listSheets;
    listSheetsSelected = [[NSMutableArray alloc] init];
    
    [textFieldPicker addTarget:self action:@selector(showTypePickerAction) forControlEvents:UIControlEventTouchDown];
    
    explanationLabel.hidden = YES;
    
    if (listSheets.count == 0) {
        explanationLabel.hidden = NO;
        filesCollectionView.hidden = YES;
        explanationLabel.text = NSLocalizedString(@"frieze_explanation", nil);
        
    } else {
        filesCollectionView.hidden = NO;
        filesCollectionView.delegate = self;
        filesCollectionView.dataSource = self;
    }
    
    [buttonStart.layer setCornerRadius:buttonStart.frame.size.height*0.5];
    [buttonStart setEnabled:YES];
    isSelected = NO;
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:kTypeSession] isEqualToString:NSLocalizedString(@"byFrieze", nil)]) {
    }
//    // INIT FLOATING BUTTON
//    Floaty *floaty = [[Floaty alloc] init];
//    // ADD FIRST ITEM WITH HIS ACTION HANDLER
//    [floaty addItem:@"Depuis votre iPad" icon:[UIImage imageNamed:@"tablet_mac"] handler:^(FloatyItem * _Nonnull action) {
//        if([Subscription isSubscriptionValid]){
//            CreateSheetViewController *createSheetVC = [self.storyboard instantiateViewControllerWithIdentifier:@"createSheetViewController"];
//            createSheetVC.delegate = self;
//            [self.navigationController pushViewController:createSheetVC animated:YES];
//        }else{
//            [STMUtilities showAlertDialogWithMessageKey:NSLocalizedString(@"unavailable_in_visit_mode", nil) andDuration:3];
//        }
//
//    }];
//    // ADD SECOND ITEM WITH HIS ACTION HANDLER
//    [floaty addItem:@"Depuis le web" icon:[UIImage imageNamed:@"web"] handler:^(FloatyItem * _Nonnull action) {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.stimart.com/souvenirs/"]];
//    }];
//    // SETUP BUTTON COLOR
//    [floaty setButtonColor:[UIColor colorWithRed:90.0f/255.0f green:154.0f/255.0f blue:203.0f/255.0f alpha:1.0f]];
//    // SETUP PLUS COLOR
//    [floaty setPlusColor:[UIColor blackColor]];
//    // ADD FLOATING BUTTON INTO THE MAIN VIEW
//    [self.view addSubview:floaty];
//    }
    
    
}

- (void)createSheetViewController:(CreateSheetViewController *)createSheetVC didFinishEnteringSheet:(Sheet *)aSheet {
    NSLog(@"This was returned from CreateSheetViewController %@",aSheet);
    
    [listSheets addObject:aSheet];
    
    //[listSheets addObject:aSheet];
    NSLog(@"listSheets is : %@", listSheets);
    NSLog(@"listSheets count is: %lu",(unsigned long)listSheets.count);
    
    [filesCollectionView reloadData];
    
}

- (void) showTypePickerAction {
    [textFieldPicker setEnabled:NO];
    
    pickerFileType = [ActionSheetStringPicker showPickerWithTitle:nil rows:allTypes initialSelection:0 target:self successAction:@selector(typeSelectedAction:) cancelAction:@selector(cancelTypeSelectionAction) origin:textFieldPicker];
}

- (void) typeSelectedAction:(NSNumber *)selectedIndex {
    [textFieldPicker setEnabled:YES];
    textFieldPicker.text = [allTypes objectAtIndex:[selectedIndex intValue]];
    listSheetsForShow = [[NSMutableArray alloc] init];
    if ([selectedIndex intValue] == 1) {
        for (Sheet * aSheet in listSheets) {
            if (aSheet.getResourceTypeSimple == IMAGE) {
                [listSheetsForShow addObject:aSheet];
            }
        }
    }
    else if ([selectedIndex intValue] == 2) {
        for (Sheet * aSheet in listSheets) {
            if (aSheet.getResourceTypeSimple == AUDIO) {
                [listSheetsForShow addObject:aSheet];
            }
        }
    }
    else if ([selectedIndex intValue] == 3) {
        for (Sheet * aSheet in listSheets) {
            if (aSheet.getResourceTypeSimple == VIDEO) {
                [listSheetsForShow addObject:aSheet];
            }
        }
    }
    else {
        listSheetsForShow = listSheets;
    }
    
    [filesCollectionView reloadData];
}

- (void) cancelTypeSelectionAction {
    [textFieldPicker setEnabled:YES];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSLog(@"nbRecipeArray : %lu", (unsigned long)listSheetsForShow.count);
    NSLog(@"section : %lu", (long)section);
    int reste=[listSheetsForShow count]%numberRows;
    
    if (reste==0)
        return numberRows;
    else if (section==[listSheetsForShow count]/4)
        return reste;
    else
        return numberRows;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    if ([listSheetsForShow count]%numberRows == 0)
        return [listSheetsForShow count]/4;
    
    return [listSheetsForShow count]/4 + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    int position = (int) ((indexPath.section * 4) + indexPath.row);
    
    NSLog(@"collectionView -- cellForItemAtIndexPath %d", position);
    
    static NSString * cellIdentifier = @"sheetViewCell";
    
    SheetViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:kTypeSession] isEqualToString:NSLocalizedString(@"byTaste", nil)]) {
        cell.likeSheetImage.image = [UIImage imageNamed:@"icone_like_on.png"];
    }
    
    Sheet *aSheet = [listSheetsForShow objectAtIndex:position];
    NSLog(@"aSheet Ressource name is: %@", aSheet.resourceName);
    NSLog(@"aSheet Ressource name th is: %@", aSheet.resourceNameTh);
    
    
    if (aSheet.selected == YES) {
        isSelected = YES;
        //[buttonStart setEnabled:YES];
        
        cell.choisiSheetImage.hidden = NO;
        cell.choisiSheetImage.image = [UIImage imageNamed:@"icone_check"];
    } else {
        cell.choisiSheetImage.hidden = YES;
    }
    
    NSString * nameSheetTh = [aSheet.resourceNameTh stringByAppendingString:@".png"];
    NSString  *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * fileImage = [NSString stringWithFormat:@"%@/%@", path, nameSheetTh];
    
    cell.sheetImage.image = [UIImage imageNamed:fileImage];
    
    if (aSheet.getResourceTypeSimple == VIDEO) {
        NSLog(@"ressources type : video");
        cell.typeSheetImage.image = [UIImage imageNamed:@"icone_category_film.png"];
    } else if (aSheet.getResourceTypeSimple == AUDIO) {
        NSLog(@"ressources type : audio");
        cell.typeSheetImage.image = [UIImage imageNamed:@"icone_category_music.png"];
    } else {
        NSLog(@"ressources type : autre");
        cell.typeSheetImage.image = [UIImage imageNamed:@"icone_category_image.png"];
    }
    
    cell.titleSheetLabel.text = aSheet.name;
    
    if (aSheet.date != nil && aSheet.date.length > 4)
        cell.dateSheet.text = [aSheet.date substringToIndex:4];
    else
        cell.dateSheet.hidden = YES;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    NSLog(@"selected : %ld", (long)(indexPath.section * 4) + indexPath.row);
    NSArray * listModes = [NSLocalizedString(@"listModes", nil) componentsSeparatedByString:@"|"];
    NSString * modeSelected = [[NSUserDefaults standardUserDefaults]objectForKey:kTypeModeSelection];
    
    int position = (int) ((indexPath.section * 4) + indexPath.row);
    if ([modeSelected isEqualToString:[listModes objectAtIndex:0]]) {
        
        for (int i = 0; i < 5; i++) {
            if (position + i < listSheetsForShow.count) {
                Sheet * aSheet = [listSheetsForShow objectAtIndex:position + i];
                [listSheetsSelected addObject:aSheet];
            }
        }
        
        ScreenSlideSheetViewController *screenSlideSheetViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"screenSlideSheetViewController"];
        screenSlideSheetViewController.listSheetsSelected = listSheetsSelected;
        [self.navigationController pushViewController:screenSlideSheetViewController animated:YES];
    }
    
    else {
        Sheet * aSheet = [listSheetsForShow objectAtIndex:position];
        if (aSheet.selected == NO) {
            aSheet.selected = YES;
            [listSheetsSelected addObject:aSheet];
            nbFiles ++;
        }
        else {
            aSheet.selected = NO;
            [listSheetsSelected removeObject:aSheet];
            nbFiles --;
        }
        nbFilesPicked.text = [NSString stringWithFormat:@"%d", nbFiles];
        [filesCollectionView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onClick:(UIButton *)sender {
    switch (sender.tag) {
        case 1:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case 2:
            [self startTutoViewController];
            break;
        case 3:
            [self buttonStartClicked];
            break;
        default:
            break;
    }
}

- (void) buttonStartClicked {
    NSMutableArray * tempSheets = [[NSMutableArray alloc] init];
    NSMutableArray * randomIndexesArray;
    int numberSheetUsed;
    int nbSheetVisited = [[[NSUserDefaults standardUserDefaults] objectForKey:@"sheetVisited"] intValue];
    int nbSelectedSheet = [listSheetsSelected count];
    //int nbSelectedSheet = [listSheetsForShow count];
    int nbMaxSheetsGenerated = 20;
    int nbMaxSheetsAllowed = 80;
    
    if ([Subscription isSubscriptionValid] ) {
        if (nbSelectedSheet == 0) {
            if ([listSheets count] > nbMaxSheetsGenerated) {
                randomIndexesArray = [STMUtilities getRandomUniqueIntegersFrom:0 To:listSheets.count-1 AndNumber:nbMaxSheetsGenerated];

                for(int i = 0; i < randomIndexesArray.count;i++) {
                    [tempSheets addObject:listSheets[i]];
                }

            } else {
                [tempSheets addObjectsFromArray:listSheets];
            }

            nbSelectedSheet = [tempSheets count];

        } else {
            tempSheets = listSheetsSelected;
        }
        
        numberSheetUsed = (int)listSheetsForShow.count + nbSheetVisited;
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:numberSheetUsed] forKey:@"sheetVisited"];
        ScreenSlideSheetViewController *screenSlideSheetViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"screenSlideSheetViewController"];
        screenSlideSheetViewController.listSheetsSelected = tempSheets;
        [self.navigationController pushViewController:screenSlideSheetViewController animated:YES];
        
    }
    
    else if ([Account isVisitMode]) {
        if (nbSheetVisited >= nbMaxSheetsAllowed) {
            [STMUtilities showAlertDialogWithMessage:[NSString stringWithFormat:NSLocalizedString(@"number_max_sheet_visit_limit", nil), nbSheetVisited] andDuration:4];
            return;
        } else if ( (nbSheetVisited + listSheetsSelected.count) > nbMaxSheetsAllowed) {
            [STMUtilities showAlertDialogWithMessage:[NSString stringWithFormat:NSLocalizedString(@"number_max_sheet_visit_limit", nil), nbSheetVisited] andDuration:4];
            return;
        } else {
            if (nbSelectedSheet == 0) {
                
                if ( [listSheets count] <= (nbMaxSheetsAllowed - nbSheetVisited) ) {
                    if ( [listSheets count] >= nbMaxSheetsGenerated) {
                        
                        randomIndexesArray = [STMUtilities getRandomUniqueIntegersFrom:0 To:listSheets.count-1 AndNumber:nbMaxSheetsGenerated];
                        
                        for(int i = 0; i < randomIndexesArray.count;i++) {
                               [tempSheets addObject:listSheets[i]];
                           }
                        
                    } else {
                        [tempSheets addObjectsFromArray:listSheets];
                    }
                } else if ( [listSheets count] >= (nbMaxSheetsAllowed - nbSheetVisited) ) {
                    if ( (nbMaxSheetsAllowed - nbSheetVisited) >= nbMaxSheetsGenerated ) {
                        randomIndexesArray = [STMUtilities getRandomUniqueIntegersFrom:0 To:listSheets.count-1 AndNumber:nbMaxSheetsGenerated];
                    } else {
                        randomIndexesArray = [STMUtilities getRandomUniqueIntegersFrom:0 To:listSheets.count-1 AndNumber:(nbMaxSheetsAllowed - nbSheetVisited)];
                    }
                    for(int i = 0; i < randomIndexesArray.count;i++) {
                        [tempSheets addObject:listSheets[i]];
                    }
                    
                } else {
                    [tempSheets addObjectsFromArray:listSheets];
                }
                
                nbSelectedSheet = [tempSheets count];
                
            } else {
                tempSheets = listSheetsSelected;
            }
            
                numberSheetUsed = (int)listSheetsForShow.count + nbSheetVisited;
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:numberSheetUsed] forKey:@"sheetVisited"];
                ScreenSlideSheetViewController *screenSlideSheetViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"screenSlideSheetViewController"];
                screenSlideSheetViewController.listSheetsSelected = tempSheets;
                [self.navigationController pushViewController:screenSlideSheetViewController animated:YES];
                
            }
                
//                if ([listSheets count] > nbMaxSheetsGenerated) {
//                    randomIndexesArray = [STMUtilities getRandomUniqueIntegersFrom:0 To:listSheets.count-1 AndNumber:nbMaxSheetsGenerated];
//
//                    for(int i = 0; i < randomIndexesArray.count;i++) {
//                        [tempSheets addObject:listSheets[i]];
//                    }
//
//                } else {
//                    [tempSheets addObjectsFromArray:listSheets];
//                }
//
//                nbSelectedSheet = [tempSheets count];
//
//            } else {
//                tempSheets = listSheetsSelected;
//            }
       
        

     }
    

    
//    if (isSelected) {
//    for (Sheet * aSheet in listSheetsForShow) {
//        if (aSheet.selected == YES) {
//            [listSheetsSelected addObject:aSheet];
//        }
//    }
//    }else{
//        int randomNumberToGenerate;
//        if(listSheetsForShow.count > nbMaxSheetsGenerated){
//            randomNumberToGenerate = nbMaxSheetsGenerated;
//        }else{
//            randomNumberToGenerate = listSheetsForShow.count;
//        }
//
//        NSMutableArray * randomNumberArray = [STMUtilities getRandomUniqueIntegersFrom:0 To:listSheetsForShow.count AndNumber:randomNumberToGenerate];
//        for(int i = 0; i < randomNumberArray.count;i++){
//            [listSheetsSelected addObject:listSheetsForShow[i]];
//
//        }
//
//    }
//
//    if (([Account isVisitMode] || ![Subscription isSubscriptionValid]) && (nbSheetVisited + listSheetsSelected.count) > nbMaxSheetsGenerated) {
//        [STMUtilities showAlertDialogWithMessage:[NSString stringWithFormat:NSLocalizedString(@"number_max_sheet_visit_limit", nil), nbSheetVisited] andDuration:4];
//        return;
//    }
//    else if (listSheetsSelected.count < 1) {
//        if (listSheetsForShow.count < nbMaxSheetsGenerated)
//            listSheetsSelected = listSheetsForShow;
//        else {
//            randomNumbersArray = [STMUtilities getRandomIntegersFrom:0 To:listSheetsForShow.count-1 AndNumber:nbMaxSheetsGenerated];
//            for (NSNumber * i in randomNumbersArray) {
//                [listSheetsSelected addObject:[listSheetsForShow objectAtIndex:[i intValue]]];
//            }
//        }
//    }

}

- (void) startTutoViewController {
    NSArray *coachMarks = @[
        @{
            @"rect": [NSValue valueWithCGRect:CGRectMake(408, 0, 150, 150)],
            @"caption": NSLocalizedString(@"sheetTutoText1", nil),
            @"shape": @"circle",
            @"font": [UIFont boldSystemFontOfSize:14.0]
        },
        @{
            @"rect": [NSValue valueWithCGRect:CGRectMake(10, 0, 250, 250)],
            @"caption": NSLocalizedString(@"sheetTutoText2", nil),
            @"shape": @"circle",
            @"font": [UIFont boldSystemFontOfSize:14.0]
        },
        @{
            @"rect": [NSValue valueWithCGRect:CGRectMake(780, 0, 212, 212)],
            @"caption": NSLocalizedString(@"sheetTutoText3", nil),
            @"shape": @"circle",
            @"font": [UIFont boldSystemFontOfSize:14.0]
        },
        @{
            @"rect": [NSValue valueWithCGRect:CGRectMake(16, 16, 78, 78)],
            @"caption": NSLocalizedString(@"sheetTutoText4", nil),
            @"shape": @"circle",
            @"font": [UIFont boldSystemFontOfSize:14.0]
        },
        @{
            @"rect": [NSValue valueWithCGRect:CGRectMake(96, 16, 78, 78)],
            @"caption": NSLocalizedString(@"sheetTutoText5", nil),
            @"shape": @"circle",
            @"font": [UIFont boldSystemFontOfSize:14.0]
        }
    ];
    
    DDCoachMarksView *coachMarksView = [[DDCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    [self.view addSubview:coachMarksView];
    [coachMarksView start];
}
@end
