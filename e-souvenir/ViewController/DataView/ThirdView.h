//
//  ThirdView.h
//  e-souvenir
//
//  Created by Dynseo on 01/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThirdView : UIView<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *viewTitle;
@property (strong, nonatomic) IBOutlet UILabel *sheetNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *sheetTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *sheetLikeLabel;
@property (strong, nonatomic) IBOutlet UILabel *sheetLegendLabel;

@property (nonatomic) UITableView * dateTableView;
@property (nonatomic) UITableView * sessionTableView;
@property (nonatomic) UITableView * sessionSheetTableView;
- (void) initThirdView;
@end
