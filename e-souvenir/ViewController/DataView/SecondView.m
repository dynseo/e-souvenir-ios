//
//  SecondView.m
//  e-souvenir
//
//  Created by Dynseo on 01/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "SecondView.h"
#import "DatabaseManager.h"

@implementation SecondView {
    DatabaseManager * databaseManager;
    NSMutableArray * idSheetMostViewed;
    NSMutableArray * nbSheetMostViewed;
    
    NSMutableArray * idSheetMostLiked;
    NSMutableArray * nbSheetMostLiked;
    
    Sheet * aSheet;
    NSString * path;
    NSString * nameSheetTh;
    NSString * fileImage;
}

@synthesize image1, image2, image3, image4, image5, image6, title1, title2, title3, title4, title5, title6, nbView1, nbView2, nbView3, nbView4, nbView5, nbView6;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void) initSecondView {
    databaseManager = [DatabaseManager getSharedInstance];
    
    path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

    
    idSheetMostViewed = [[NSMutableArray alloc] init];
    nbSheetMostViewed = [[NSMutableArray alloc] init];
    idSheetMostLiked = [[NSMutableArray alloc] init];
    nbSheetMostLiked = [[NSMutableArray alloc] init];
    
    
    NSMutableDictionary * listMostViewed = [databaseManager getTopViewedSheets];
    NSMutableDictionary * listMostLiked = [databaseManager getTopLikedSheets];
    
    for (NSString * key in listMostViewed) {
        NSNumber * anObject = (NSNumber *) [listMostViewed objectForKey:key];
        [idSheetMostViewed addObject:key];
        [nbSheetMostViewed addObject:anObject];
    }
    
    for (NSString * key in listMostLiked) {
        NSNumber * anObject = (NSNumber *) [listMostLiked objectForKey:key];
        [idSheetMostLiked addObject:key];
        [nbSheetMostLiked addObject:anObject];
    }
    
    if (listMostViewed.count >= 1) {
        aSheet = [databaseManager getNameSheetBySheetId:[[idSheetMostViewed objectAtIndex:0] intValue]];
        title1.text = aSheet.name;
        nbView1.text = [[nbSheetMostViewed objectAtIndex:0] stringValue];
    
        nameSheetTh = [aSheet.resourceNameTh stringByAppendingString:@".png"];
        fileImage = [NSString stringWithFormat:@"%@/%@", path, nameSheetTh];
        image1.image = [UIImage imageNamed:fileImage];
    }
    if (listMostViewed.count >= 2) {
        aSheet = [databaseManager getNameSheetBySheetId:[[idSheetMostViewed objectAtIndex:1] intValue]];
        title2.text = aSheet.name;
        nbView2.text = [[nbSheetMostViewed objectAtIndex:1] stringValue];
        
        nameSheetTh = [aSheet.resourceNameTh stringByAppendingString:@".png"];
        fileImage = [NSString stringWithFormat:@"%@/%@", path, nameSheetTh];
        image2.image = [UIImage imageNamed:fileImage];
    }
    if (listMostViewed.count >= 3) {
        aSheet = [databaseManager getNameSheetBySheetId:[[idSheetMostViewed objectAtIndex:2] intValue]];
        title3.text = aSheet.name;
        nbView3.text = [[nbSheetMostViewed objectAtIndex:2] stringValue];
        
        nameSheetTh = [aSheet.resourceNameTh stringByAppendingString:@".png"];
        fileImage = [NSString stringWithFormat:@"%@/%@", path, nameSheetTh];
        image3.image = [UIImage imageNamed:fileImage];
    }
    
    if (listMostLiked.count >= 1) {
        aSheet = [databaseManager getNameSheetBySheetId:[[idSheetMostLiked objectAtIndex:0] intValue]];
        title4.text = aSheet.name;
        nbView4.text = [[nbSheetMostLiked objectAtIndex:0] stringValue];
        
        nameSheetTh = [aSheet.resourceNameTh stringByAppendingString:@".png"];
        fileImage = [NSString stringWithFormat:@"%@/%@", path, nameSheetTh];
        image4.image = [UIImage imageNamed:fileImage];
    }
    if (listMostLiked.count >= 2) {
        aSheet = [databaseManager getNameSheetBySheetId:[[idSheetMostLiked objectAtIndex:1] intValue]];
        title5.text = aSheet.name;
        nbView5.text = [[nbSheetMostLiked objectAtIndex:1] stringValue];
        
        nameSheetTh = [aSheet.resourceNameTh stringByAppendingString:@".png"];
        fileImage = [NSString stringWithFormat:@"%@/%@", path, nameSheetTh];
        image5.image = [UIImage imageNamed:fileImage];
    }
    if (listMostLiked.count >= 3) {
        aSheet = [databaseManager getNameSheetBySheetId:[[idSheetMostLiked objectAtIndex:2] intValue]];
        title6.text = aSheet.name;
        nbView6.text = [[nbSheetMostLiked objectAtIndex:2] stringValue];
        
        nameSheetTh = [aSheet.resourceNameTh stringByAppendingString:@".png"];
        fileImage = [NSString stringWithFormat:@"%@/%@", path, nameSheetTh];
        image6.image = [UIImage imageNamed:fileImage];
    }
    
    NSLog(@"initSecondeView");
}

@end
