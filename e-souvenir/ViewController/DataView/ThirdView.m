//
//  ThirdView.m
//  e-souvenir
//
//  Created by Dynseo on 01/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//
#import "AppDelegate.h"
#import "ThirdView.h"
#import "DatabaseManager.h"
#import "Session.h"
#import "SheetTableViewCell.h"
#import "DialogSheetView.h"
#import "Utilities.h"

@implementation ThirdView {
    DatabaseManager * databaseManager;
    NSMutableArray * listSessions;
    NSMutableArray * listSessionsOnSameDay;
    NSMutableArray * listFichesOnSameSession;
    NSMutableArray * listDates;
    NSMutableArray * listSheetNames;
    
    UILabel * titleLabel;
    UIButton * buttonBack;
    Person * currentPerson;
    int currentSessionDatePosition;
    int currentSessionPosition;
    int currentPage;
    int currentRow;
}

@synthesize dateTableView, sessionTableView, sessionSheetTableView, viewTitle, sheetLikeLabel, sheetNameLabel, sheetTimeLabel, sheetLegendLabel;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void) initThirdView {
    currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    viewTitle.hidden = YES;
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1024, 50)];
    titleLabel.center = CGPointMake(512, 20);
    titleLabel.text = [NSString stringWithFormat:@"%@ %@ %@ !",NSLocalizedString(@"sessions_of", nil), currentPerson.firstName , currentPerson.name];
//    titleLabel.font = [UIFont systemFontOfSize:40];
    titleLabel.font = [UIFont boldSystemFontOfSize:30];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    buttonBack = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    buttonBack.center = CGPointMake(970, 20);
    UIImage *backImage = [UIImage imageNamed:@"icon_previous.png"];
    [buttonBack setImage:backImage forState:UIControlStateNormal];
    buttonBack.hidden = YES;
    [buttonBack addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:buttonBack];
    [self addSubview:titleLabel];
    databaseManager = [DatabaseManager getSharedInstance];
    currentPage = 0;
    currentRow = 0;
    [self initPage1];
    dateTableView.delegate = self;
    dateTableView.dataSource = self;
    [self addSubview:dateTableView];
    
    NSLog(@"initThirdView");
}

- (void) initPage1 {
    listSessions = [databaseManager getSessionForCurrentPerson];
    listDates = [[NSMutableArray alloc] init];
    for (Session * aSession in listSessions) {
        NSString * date = [aSession.sessionDate componentsSeparatedByString:@" "][0];
        if (![listDates containsObject:date]) {
            [listDates addObject:date];
        }
    }
    dateTableView = [[UITableView alloc] initWithFrame:CGRectMake(20,80,984,450)];
    dateTableView.backgroundColor = [UIColor clearColor];
    NSLog(@"initPage1");
}

- (void) initPage2 {
    listSessionsOnSameDay = [[NSMutableArray alloc] init];
    NSString * date = [listDates objectAtIndex:currentSessionDatePosition];
    for (Session * aSession in listSessions) {
        if ([aSession.sessionDate containsString:date]) {
            [listSessionsOnSameDay addObject:aSession];
        }
    }
    sessionTableView = [[UITableView alloc] initWithFrame:CGRectMake(20,80,984,450)];
    sessionTableView.backgroundColor = [UIColor clearColor];
    NSLog(@"initPage2");
}

- (void) initPage3 {
    listFichesOnSameSession = [[NSMutableArray alloc] init];
    Session * aSession = [listSessionsOnSameDay objectAtIndex:currentSessionPosition];
    listFichesOnSameSession = [databaseManager getSessionSheetsBySessionId:aSession.sessionId];
    
    listSheetNames = [[NSMutableArray alloc] init];
    listSheetNames = [databaseManager getListNameSheetsByListSessionSheets:listFichesOnSameSession];
    
    sessionSheetTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,160,1024,400)];
    sessionSheetTableView.backgroundColor = [UIColor clearColor];
    NSLog(@"initPage3");
}

// ---------------------------- Table management
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:dateTableView]) {
        return listDates.count;
    }
    else if ([tableView isEqual:sessionTableView]) {
        return listSessionsOnSameDay.count;
    }
    else if ([tableView isEqual:sessionSheetTableView]) {
        return listFichesOnSameSession.count;
    }
    else
        return 0;
}

// creates cell of tableView
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:dateTableView]) {
        static NSString *simpleTableIdentifier = @"SimpleTableItem";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        NSString * text = [NSString stringWithFormat:@"%@ %d : ",NSLocalizedString(@"session", nil) ,indexPath.row + 1];
        NSString * dateString = [Utilities modifyFormatDate:@"yyyy-MM-dd" :@"dd-MM-yyyy" :[listDates objectAtIndex:indexPath.row]];        
        text = [text stringByAppendingString:dateString];
        cell.textLabel.text = text;
        cell.textLabel.font = [UIFont systemFontOfSize:30];
        cell.textLabel.textColor = [UIColor grayColor];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
    else if ([tableView isEqual:sessionTableView]) {
        static NSString *simpleTableIdentifier = @"SimpleTableItem";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        Session * aSession = [listSessionsOnSameDay objectAtIndex:indexPath.row];
        NSString * text = [NSString stringWithFormat:@"%d. ", indexPath.row + 1];
        text = [text stringByAppendingString:aSession.type];
        text = [text stringByAppendingString:@" à "];
        text = [text stringByAppendingString:[aSession.sessionDate componentsSeparatedByString:@" "][1]];
        cell.textLabel.text = text;
        cell.textLabel.font = [UIFont systemFontOfSize:25];
        cell.textLabel.textColor = [UIColor grayColor];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
    else if ([tableView isEqual:sessionSheetTableView]) {
        static NSString *CellWithIdentifier = @"SheetTableViewCell";
        SheetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellWithIdentifier];
        if (cell == nil) {
            NSArray *nib;
            nib = [[NSBundle mainBundle] loadNibNamed:@"SheetTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        NSUInteger row = [indexPath row];
        SessionSheet * aSessionSheet = [listFichesOnSameSession objectAtIndex:row];
        NSString * sheetName = [listSheetNames objectAtIndex:row];
        cell.sheetNameLabel.text = sheetName;
        cell.sheetTime.text = [Utilities convertSecondeToString:aSessionSheet.sessionSheetTimer];
        cell.sheetLike.text = (aSessionSheet.like != nil && ![aSessionSheet.like isEqualToString:@"(null)"] && ![aSessionSheet.like isEqualToString:@""]) ? aSessionSheet.like : @"/";
        cell.sheetLegend.text = (aSessionSheet.legend != nil && ![aSessionSheet.legend isEqualToString:@"(null)"] && ![aSessionSheet.legend isEqualToString:@""]) ? aSessionSheet.legend : @"/";
        cell.sheetNameLabel.font = [UIFont systemFontOfSize:15];
        cell.sheetTime.font = [UIFont systemFontOfSize:15];
        cell.sheetLike.font = [UIFont systemFontOfSize:15];
        cell.sheetLegend.font = [UIFont systemFontOfSize:15];
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:dateTableView]) {
        dateTableView.hidden = YES;
        buttonBack.hidden = NO;
        currentPage ++;
        currentSessionDatePosition = (int) indexPath.row;
        titleLabel.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"series_of", nil), [Utilities modifyFormatDate:@"yyyy-MM-dd" :@"dd-MM-yyyy" :[listDates objectAtIndex:indexPath.row]]];
        [self initPage2];
        currentRow = (int) indexPath.row;
        sessionTableView.delegate = self;
        sessionTableView.dataSource = self;
        [self addSubview:sessionTableView];
    }
    
    else if ([tableView isEqual:sessionTableView]) {
        sessionTableView.hidden = YES;
        titleLabel.text = [NSString stringWithFormat:@"%@ %d",NSLocalizedString(@"detail_serie", nil), indexPath.row+1];
        UIImage *backImage = [UIImage imageNamed:@"icon_previous_double.png"];
        [buttonBack setImage:backImage forState:UIControlStateNormal];
        currentPage ++;
        viewTitle.hidden = NO;
        currentSessionPosition = (int) indexPath.row;
        [self initPage3];
        sessionSheetTableView.delegate = self;
        sessionSheetTableView.dataSource = self;
        [self addSubview:sessionSheetTableView];
    }
    
    else if ([tableView isEqual:sessionSheetTableView]) {
        [self showSessionSheetDialog : (int) indexPath.row];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:dateTableView] || [tableView isEqual:sessionTableView]) {
        return 50;
    }
    return 80;
}

- (void) onClick :(id) sender {
    if (currentPage == 1) {
        currentPage --;
        viewTitle.hidden = YES;
        sessionTableView.hidden = YES;
        sessionSheetTableView.hidden = YES;
        dateTableView.hidden = NO;
        buttonBack.hidden = YES;
        titleLabel.text = [NSString stringWithFormat:@"%@ %@ %@ !",NSLocalizedString(@"sessions_of", nil), currentPerson.firstName , currentPerson.name];
    }
    else if (currentPage == 2) {
        currentPage --;
        UIImage *backImage = [UIImage imageNamed:@"icon_previous.png"];
        [buttonBack setImage:backImage forState:UIControlStateNormal];
        viewTitle.hidden = YES;
        sessionTableView.hidden = NO;
        sessionSheetTableView.hidden = YES;
        dateTableView.hidden = YES;
        buttonBack.hidden = NO;
        titleLabel.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"series_of", nil), [Utilities modifyFormatDate:@"yyyy-MM-dd" :@"dd-MM-yyyy" :[listDates objectAtIndex:currentRow]]];
    }
}

- (void) showSessionSheetDialog : (int) position {
    NSLog(@"showSessionSheetDialog %d", position);
    
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"DialogSheetView" owner:self options:nil];
    SessionSheet * aSessionSheet = [listFichesOnSameSession objectAtIndex:position];
    DialogSheetView * dialogSheetView = [subviewArray objectAtIndex:0];
    [dialogSheetView initializeDialogWithSessionSheet:aSessionSheet];
    
    [self addSubview:dialogSheetView];
    dialogSheetView.hidden = NO;
    [self bringSubviewToFront:dialogSheetView];
}
@end
