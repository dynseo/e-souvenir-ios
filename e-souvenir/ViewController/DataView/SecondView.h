//
//  SecondView.h
//  e-souvenir
//
//  Created by Dynseo on 01/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondView : UIView
@property (strong, nonatomic) IBOutlet UIImageView *image1;
@property (strong, nonatomic) IBOutlet UIImageView *image2;
@property (strong, nonatomic) IBOutlet UIImageView *image3;
@property (strong, nonatomic) IBOutlet UIImageView *image4;
@property (strong, nonatomic) IBOutlet UIImageView *image5;
@property (strong, nonatomic) IBOutlet UIImageView *image6;
@property (strong, nonatomic) IBOutlet UILabel *nbView1;
@property (strong, nonatomic) IBOutlet UILabel *nbView2;
@property (strong, nonatomic) IBOutlet UILabel *nbView3;
@property (strong, nonatomic) IBOutlet UILabel *nbView4;
@property (strong, nonatomic) IBOutlet UILabel *nbView5;
@property (strong, nonatomic) IBOutlet UILabel *nbView6;


@property (strong, nonatomic) IBOutlet UILabel *title1;
@property (strong, nonatomic) IBOutlet UILabel *title2;
@property (strong, nonatomic) IBOutlet UILabel *title3;
@property (strong, nonatomic) IBOutlet UILabel *title4;
@property (strong, nonatomic) IBOutlet UILabel *title5;
@property (strong, nonatomic) IBOutlet UILabel *title6;

- (void) initSecondView;
@end
