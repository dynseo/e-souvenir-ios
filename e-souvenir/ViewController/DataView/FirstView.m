//
//  FirstView.m
//  e-souvenir
//
//  Created by Dynseo on 01/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "FirstView.h"
#import "DatabaseManager.h"

@implementation FirstView {
    DatabaseManager * databaseManager;
    NSMutableArray * itmesBarChart;
    NSMutableArray * times;
    NSMutableArray * itemsPieChart;
    NSMutableDictionary * listMostViewedTheme;
    NSMutableDictionary * listMostLikedTheme;
    
    NSMutableDictionary * listMostViewedDecade;
    NSMutableDictionary * listMostLikedDecade;
    NSArray * arrayColor;
}

@synthesize pieChart, barChart, themeSwitch, decadeSwitch;


- (IBAction)themeSwitchAction:(id)sender {
    if (themeSwitch.selectedSegmentIndex == 0) {
        [self setItemsMostLikedTheme];
    }
    else {
        [self setItemsMostViewedTheme];
    }
    if (itemsPieChart.count == 0) {

        NSLog(@"no database !");
    }
    else {
        if (self.pieChart) {
            [self.pieChart updateChartData:itemsPieChart];
            [self.pieChart strokeChart];
        }
        else
            [self drawPieChart];
    }
}

- (IBAction)decadeSwitchAction:(id)sender {
    if (decadeSwitch.selectedSegmentIndex == 0) {
        [self setItemsMostLikedDecade];
    }
    else {
        [self setItemsMostViewedDecade];
    }
    if (itmesBarChart.count == 0 || times.count == 0) {
        NSLog(@"no database !");
        UILabel* noDataLabel = [[UILabel alloc] initWithFrame:CGRectMake((CGFloat) 625, 135, 300.0, 300.0)];
        noDataLabel.text = @"il n’y a pas encore assez de donnée";
        [self addSubview:noDataLabel];

    }
    else {
        [self drawBarChart];
    }
}

- (void) initFirstView {
    itemsPieChart = [[NSMutableArray alloc] init];
    arrayColor = [NSArray arrayWithObjects:PNLightGreen, PNMauve, PNBrown, PNRed, PNTwitterColor, PNBrown, PNBlue, nil];
    databaseManager = [DatabaseManager getSharedInstance];
    themeSwitch.selectedSegmentIndex = 0;
    listMostViewedTheme = [databaseManager getCategoryViewsPercentages];
    listMostLikedTheme = [databaseManager getCategoryLikePercentages];
    [self setItemsMostLikedTheme];
    
    if (itemsPieChart.count == 0) {
        NSLog(@"no database !");
    }
    else
        [self drawPieChart];
    
    itmesBarChart = [[NSMutableArray alloc] init];
    times = [[NSMutableArray alloc] init];
    decadeSwitch.selectedSegmentIndex = 0;
    listMostViewedDecade = [databaseManager getDecadeViewPercentages];
    listMostLikedDecade = [databaseManager getDecadeLikePercentages];
    

    [self setItemsMostLikedDecade];
    
    if (itmesBarChart.count == 0 || times.count == 0) {
    }
    else
        [self drawBarChart];
}

- (void) setItemsMostLikedTheme {
    NSLog(@"setItemsMostLikedTheme");
    [itemsPieChart removeAllObjects];
    int i = 0;
    for (NSString * key in listMostLikedTheme) {
        NSNumber * anObject = (NSNumber *) [listMostLikedTheme objectForKey:key];
        [itemsPieChart addObject:[PNPieChartDataItem dataItemWithValue:[anObject floatValue] color:[arrayColor objectAtIndex:i] description:NSLocalizedString(key, nil)]];
        i++;
    }
}

- (void) setItemsMostViewedTheme {
    NSLog(@"setItemsMostViewedTheme");
    [itemsPieChart removeAllObjects];
    int i = 0;
    for (NSString * key in listMostViewedTheme) {
        NSNumber * anObject = (NSNumber *) [listMostViewedTheme objectForKey:key];
        [itemsPieChart addObject:[PNPieChartDataItem dataItemWithValue:[anObject floatValue] color:[arrayColor objectAtIndex:i] description:NSLocalizedString(key, nil)]];
        i++;
    }
}

- (void) setItemsMostLikedDecade {
    [itmesBarChart removeAllObjects];
    [times removeAllObjects];
    
    NSArray *sortedKeys = [[listMostLikedDecade allKeys] sortedArrayUsingSelector: @selector(compare:)];
    for (NSString *key in sortedKeys)
        [times addObject: [listMostLikedDecade objectForKey: key]];
    
    itmesBarChart = [[NSMutableArray alloc] initWithArray:sortedKeys];
}

- (void) setItemsMostViewedDecade {
    [itmesBarChart removeAllObjects];
    [times removeAllObjects];
    
    NSArray *sortedKeys = [[listMostViewedDecade allKeys] sortedArrayUsingSelector: @selector(compare:)];
    for (NSString *key in sortedKeys)
        [times addObject: [listMostViewedDecade objectForKey: key]];
    
    itmesBarChart = [[NSMutableArray alloc] initWithArray:sortedKeys];
}

- (void) drawPieChart {
    self.pieChart = [[PNPieChart alloc] initWithFrame:CGRectMake((CGFloat) 625, 135, 300.0, 300.0) items:itemsPieChart];
    self.pieChart.descriptionTextColor = [UIColor whiteColor];
    self.pieChart.descriptionTextFont = [UIFont fontWithName:@"Avenir-Medium" size:11.0];
    self.pieChart.descriptionTextShadowColor = [UIColor clearColor];
    self.pieChart.showAbsoluteValues = NO;
    self.pieChart.showOnlyValues = NO;
    [self.pieChart strokeChart];
    [self addSubview:self.pieChart];
}

- (void) drawBarChart {
    if (barChart) {
        barChart.hidden = YES;
        barChart = nil;
    }
    
    static NSNumberFormatter *barChartFormatter;
    if (!barChartFormatter) {
        barChartFormatter = [[NSNumberFormatter alloc] init];
        barChartFormatter.numberStyle = kCFNumberFormatterNoStyle;
        barChartFormatter.allowsFloats = NO;
        barChartFormatter.maximumFractionDigits = 0;
    }
    self.barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(0, 100.0, 500, 350.0)];
    self.barChart.backgroundColor = [UIColor clearColor];
    [self.barChart setLabelTextColor:[UIColor whiteColor]];
    self.barChart.yLabelFormatter = ^(CGFloat yValue) {
        return [barChartFormatter stringFromNumber:@(yValue)];
    };
    self.barChart.chartMarginLeft = 30.0;
    self.barChart.showChartBorder = YES;
    [self.barChart setXLabels:itmesBarChart];
    [self.barChart setYValues:times];
    self.barChart.isGradientShow = NO;
    self.barChart.isShowNumbers = NO;
    self.barChart.showLabel = NO;
    [self.barChart strokeChart];
    self.barChart.delegate = self;
    [self addSubview:self.barChart];
}

@end
