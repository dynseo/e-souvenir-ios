//
//  FirstView.h
//  e-souvenir
//
//  Created by Dynseo on 01/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PNChartDelegate.h"
#import "PNChart.h"

@interface FirstView : UIView<PNChartDelegate>

@property (nonatomic) PNPieChart * pieChart;
@property (nonatomic) PNBarChart * barChart;
@property (strong, nonatomic) IBOutlet UISegmentedControl *themeSwitch;
@property (strong, nonatomic) IBOutlet UISegmentedControl *decadeSwitch;
- (IBAction)themeSwitchAction:(id)sender;
- (IBAction)decadeSwitchAction:(id)sender;

- (void) initFirstView;
@end
