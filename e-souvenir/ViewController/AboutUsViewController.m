//
//  AboutUsViewController.m
//  e-souvenirs
//
//  Created by Dynseo on 22/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "AboutUsViewController.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import <StimartLib/Reachability.h>


@implementation AboutUsViewController {
    DimensionAdapter * dimensionAdapter;
    int marginLeft, marginTop;
}

@synthesize backgroundView, titleUp, buttonBack, textAboutUs;
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id) initDialogAboutUs {
    NSLog(@"initWithAccountInfo");
    dimensionAdapter = [[DimensionAdapter alloc] initDimensionAdapter];
    self = [super initWithFrame:CGRectMake(0, 0, dimensionAdapter.sizex1024, dimensionAdapter.sizey768)];
    [self setBackgroundColor:[UIColor colorWithRed:0.502 green:0.502 blue:0.502 alpha:0.6]];
    [self createBackgroundWithColorString:@"#ffffff"];
    marginLeft = dimensionAdapter.sizex222;
    marginTop = dimensionAdapter.sizey187;
    titleUp = [[UILabel alloc]initWithFrame:CGRectMake(dimensionAdapter.sizex262, dimensionAdapter.sizey120, dimensionAdapter.sizex500, dimensionAdapter.sizey120)];
    titleUp.font = [UIFont boldSystemFontOfSize:dimensionAdapter.sizey40];
    titleUp.lineBreakMode = NSLineBreakByWordWrapping;
    titleUp.numberOfLines = 0;
    titleUp.textAlignment = NSTextAlignmentCenter;
    [titleUp setText:NSLocalizedString(@"about", nil)];
    [self addSubview:titleUp];
    
    [self loadDialogAboutUsTexts];
    
    buttonBack = [[STMButton alloc]initWithFrame:CGRectMake(dimensionAdapter.sizex412, dimensionAdapter.sizey585, dimensionAdapter.sizex200, dimensionAdapter.sizey60) andType:STMButtonTypeRounded andTitle:NSLocalizedString(@"return",nil) andTitleSize:dimensionAdapter.sizey40 andBackgroundColor:NSLocalizedString(@"menu_light_button_code", nil) andHighlightColor:NSLocalizedString(@"menu_dark_button_code", nil)];
    [buttonBack.titleLabel setFont:[UIFont boldSystemFontOfSize:dimensionAdapter.sizey30]];
    [buttonBack addTarget:self action:@selector(hideAction)forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:buttonBack];
    
    
    return self;
}

- (void)loadDialogAboutUsTexts
{
    NSLog(@"loadDialogAboutTexts");
    textAboutUs = [[UITextView alloc] initWithFrame:CGRectMake(dimensionAdapter.sizex237, dimensionAdapter.sizey237, dimensionAdapter.sizex550, dimensionAdapter.sizey291)];
    [textAboutUs setFont:[UIFont systemFontOfSize:dimensionAdapter.sizey20]];
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    
    NSString *titleAppName = NSLocalizedString(@"application_name", nil);
    NSString *titleLang = NSLocalizedString(@"language", nil);
    NSString *titleVersion = NSLocalizedString(@"application_version", nil);
    NSString *titleIOSVersion = NSLocalizedString(@"ios_version", nil);
    NSString *titleSSID = NSLocalizedString(@"ssid_name", nil);
    NSString *titleReachability = NSLocalizedString(@"reachability", nil);
    NSString *titleSerialNumber = NSLocalizedString(@"serial_number", nil);
    
    
    
    NSString *valueAppName = [STMUtilities getAppName]?[STMUtilities getAppName]:@"";
    NSString *valueLang = [STMUtilities getAppLanguageText]?[STMUtilities getAppLanguageText]:@"";
    NSString *valueVersion = [STMUtilities getAppVersion]?[STMUtilities getAppVersion]:@"";
    NSString *valueIOSVersion = [STMUtilities getSystemVersion]?[STMUtilities getSystemVersion]:@"";
    NSString *valueSSID = [self currentWifiSSID]?[self currentWifiSSID]:@"";
    NSString *valueReachability = [self currentWifiSSID]?[self currentWifiSSID]:@"";
    NSString *valueSerailNumber = [STMUtilities getUUID]?[STMUtilities getUUID]:@"";
    
    if (networkStatus == NotReachable) {
          valueReachability = @"No internet";
    } else {
          valueReachability = @"There is internet";
    }
    
    
    NSString *aboutUs = NSLocalizedString(@"about_text", nil)?NSLocalizedString(@"about_text", nil):@"";
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithString: [NSString stringWithFormat:@"%@ %@\n\n%@ %@\n\n%@ %@\n\n%@ %@\n\n%@ %@\n\n%@ %@\n\n%@  %@\n\n\n%@", titleAppName, valueAppName, titleLang, valueLang, titleVersion, valueVersion, titleIOSVersion, valueIOSVersion, titleSSID, valueSSID, titleReachability, valueReachability, titleSerialNumber, valueSerailNumber, aboutUs]];
    
    NSString *alldString = [[NSString alloc]initWithString: [NSString stringWithFormat:@"%@ %@\n\n%@ %@\n\n%@ %@\n\n%@ %@\n\n%@ %@\n\n%@ %@\n\n%@  %@\n\n\n%@", titleAppName, valueAppName, titleLang, valueLang, titleVersion, valueVersion, titleIOSVersion, valueIOSVersion, titleSSID, valueSSID, titleReachability, valueReachability, titleSerialNumber, valueSerailNumber, aboutUs]];
    
    int rangePosition = 0;
    
    NSArray *titles = [[NSArray alloc]initWithObjects:titleAppName, titleLang, titleVersion, titleIOSVersion, titleSSID, titleReachability, titleSerialNumber, nil];
    NSArray *values = [[NSArray alloc]initWithObjects:valueAppName, valueLang, valueVersion, valueIOSVersion, valueSSID, valueReachability, valueSerailNumber, nil];
    
    int count = titles.count;
    NSLog(@"count = %d", count);
    for (int i = 0; i < count; i++) {
        int lengthTitle = ((NSString *)[titles objectAtIndex:i]).length;
        int lengthValue = ((NSString *)[values objectAtIndex:i]).length;
        int rangeTitleEndPosition = rangePosition + lengthTitle + 1;
        [attributedString addAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:dimensionAdapter.sizey20]} range:NSMakeRange(rangePosition, lengthTitle)];
        [attributedString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:dimensionAdapter.sizey20]} range:NSMakeRange(rangeTitleEndPosition, lengthValue)];
        rangePosition = rangeTitleEndPosition + lengthValue + 2;
    }
    [attributedString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:dimensionAdapter.sizey20]} range:NSMakeRange(rangePosition + 1, aboutUs.length)];
    
    /*
     add super link to about_us text
     */
    
    //定义一个字符串存储文本内容
    
    //3.初始化一个富文本，并将string内容赋值给它
    //value:超链，range:要添加超链的文本位置
    [attributedString addAttribute:NSLinkAttributeName value:@"https://www.dynseo.com" range:[alldString rangeOfString:NSLocalizedString(@"DYNSEO", nil)]];
    //[attributedString addAttribute:NSLinkAttributeName value:@"contact@dynseo.com" range:[alldString rangeOfString:NSLocalizedString(@"contact@dynseo.com", nil)]];


    //4.给文本www设置字体和大小
//    NSString * str = NSLocalizedString(@"www", nil);
//    //获取要设置字体的文本的位置
//    NSRange fontRange = NSMakeRange([[conditionsAttributeStr string] rangeOfString:str].location,[[conditionsAttributeStr string] rangeOfString:str].length);
    //value:设置字体和大小，range:要设置字体的文本的位置
//    [conditionsAttributeStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:DINOFFCPRO_FONT_FAMILY size:25] range:fontRange];

    
    
    [textAboutUs setAttributedText:attributedString];
    [textAboutUs setSelectable:YES];
    textAboutUs.editable = NO;
    textAboutUs.dataDetectorTypes = UIDataDetectorTypePhoneNumber;
    
    [self addSubview:textAboutUs];
}


- (void)createBackgroundWithColorString:(NSString *)aColorString
{
    if (!backgroundView) {
        backgroundView = [[UIView alloc]initWithFrame:CGRectMake(dimensionAdapter.sizex128, dimensionAdapter.sizey96, dimensionAdapter.sizex768, dimensionAdapter.sizey576)];
        [backgroundView.layer setCornerRadius:dimensionAdapter.sizey5];
        if ([aColorString hasPrefix:@"#"]) {
            [backgroundView setBackgroundColor:[ColorTools colorFromHexString:aColorString]];
        }
        else {
            [backgroundView setBackgroundColor:[UIColor whiteColor]];
        }
        [self addSubview:backgroundView];
    }
}

- (void) hideAction {
    [self setHidden:YES];
}

- (NSString *)currentWifiSSID {
    // Does not work on the simulator.
    NSString *ssid = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"SSID"]) {
            ssid = info[@"SSID"];
        }
    }
    return ssid;
}



@end
