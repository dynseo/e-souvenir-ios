//
//  ThemeViewController.h
//  e-souvenir
//
//  Created by Dynseo on 18/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryViewController.h"

@interface ThemeViewController : CategoryViewController
- (void) setTypeSession : (int) position;
@end
