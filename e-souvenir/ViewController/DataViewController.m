//
//  DataViewController.m
//  e-souvenir
//
//  Created by Dynseo on 01/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "DataViewController.h"
#import "DDCoachMarksView.h"

@interface DataViewController ()

@end

@implementation DataViewController {
    FirstView * firstView;
    SecondView * secondeView;
    ThirdView * thirdView;
}

@synthesize tabBar, homeButton, tutoButton, titleLabel, contentView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    firstView = (FirstView *) [self viewFromNib:@"FirstView"];
    secondeView = (SecondView *) [self viewFromNib:@"SecondView"];
    thirdView = (ThirdView *) [self viewFromNib:@"ThirdView"];
    [firstView initFirstView];
    [secondeView initSecondView];
    [thirdView initThirdView];
    
    homeButton.tag = 1;
    tutoButton.tag = 2;
    titleLabel.text = NSLocalizedString(@"check_title", nil);
    [self tabBar:self.tabBar didSelectItem:[self.tabBar.items firstObject]];
    self.tabBar.selectedItem = [self.tabBar.items firstObject];
    tabBar.delegate = self;
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    switch (item.tag)
    {
        case 0:
            [contentView addSubview:firstView];
            [firstView setHidden:NO];
            [secondeView setHidden:YES];
            [thirdView setHidden:YES];
            break;
        case 1:
            [contentView addSubview:secondeView];
            [firstView setHidden:YES];
            [secondeView setHidden:NO];
            [thirdView setHidden:YES];
            break;
        case 2:
            [contentView addSubview:thirdView];
            [firstView setHidden:YES];
            [secondeView setHidden:YES];
            [thirdView setHidden:NO];
            break;
    }
    [contentView reloadInputViews];
    [self.view bringSubviewToFront:contentView];
}

- (UIView *)viewFromNib : (NSString *) nameView
{
    NSArray *nibViews = [[NSBundle mainBundle] loadNibNamed:nameView owner:self options:nil];
    UIView *view = [nibViews objectAtIndex:0];
    return view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClicked:(UIButton *)sender {
    switch (sender.tag) {
        case 1:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        case 2:
            [self startTutoViewController];
            break;
            
        default:
            break;
    }
}

- (void) startTutoViewController {
    NSArray *coachMarks;
    if (tabBar.selectedItem.tag == 0 ) {
        NSLog(@"tuto firstview");
        coachMarks = @[
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(80, 10, 340, 340)],
                                    @"caption": NSLocalizedString(@"firstViewTutoText1", nil),
                                    @"shape": @"circle",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(610, 10, 340, 340)],
                                    @"caption": NSLocalizedString(@"firstViewTutoText2", nil),
                                    @"shape": @"circle",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(675, 574, 200, 200)],
                                    @"caption": NSLocalizedString(@"firstViewTutoText3", nil),
                                    @"shape": @"circle",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(16, 30, 78, 78)],
                                    @"caption": NSLocalizedString(@"dataTutoText1", nil),
                                    @"shape": @"circle",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(120, 30, 78, 78)],
                                    @"caption": NSLocalizedString(@"dataTutoText2", nil),
                                    @"shape": @"circle",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    }
                                ];
    }
    else if (tabBar.selectedItem.tag == 1) {
        NSLog(@"tuto secondview");
        coachMarks = @[
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(130, 100, 250, 250)],
                                    @"caption": NSLocalizedString(@"secondViewTutoText1", nil),
                                    @"shape": @"circle",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(658, 100, 250, 250)],
                                    @"caption": NSLocalizedString(@"secondViewTutoText2", nil),
                                    @"shape": @"circle",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(320, 145, 40, 40)],
                                    @"caption": NSLocalizedString(@"secondViewTutoText3", nil),
                                    @"shape": @"circle",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(16, 30, 78, 78)],
                                    @"caption": NSLocalizedString(@"dataTutoText1", nil),
                                    @"shape": @"circle",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(120, 30, 78, 78)],
                                    @"caption": NSLocalizedString(@"dataTutoText2", nil),
                                    @"shape": @"circle",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    }
                                ];
    }
    else if (tabBar.selectedItem.tag == 2) {
        NSLog(@"tuto thirdview");
        coachMarks = @[
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(212, 100, 600, 100)],
                                    @"caption": NSLocalizedString(@"thirdViewTutoText1", nil),
                                    @"shape": @"square",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(16, 30, 78, 78)],
                                    @"caption": NSLocalizedString(@"dataTutoText1", nil),
                                    @"shape": @"circle",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:CGRectMake(120, 30, 78, 78)],
                                    @"caption": NSLocalizedString(@"dataTutoText2", nil),
                                    @"shape": @"circle",
                                    @"font": [UIFont boldSystemFontOfSize:14.0]
                                    }
                                ];
    }
    
    
    DDCoachMarksView *coachMarksView = [[DDCoachMarksView alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    [self.view addSubview:coachMarksView];
    [coachMarksView start];
}
@end
