//
//  ThemeViewController.m
//  e-souvenir
//
//  Created by Dynseo on 18/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "ThemeViewController.h"
#import "Category.h"
#import "Constants.h"

@interface ThemeViewController ()

@end

@implementation ThemeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
    [self initializeView];
    // Do any additional setup after loading the view.
}

- (void) loadData {
    self.nameCategory = @"Theme";
    self.listCategory = [[NSMutableArray alloc] init];
    self.nameCategoryToShow = [[NSMutableArray alloc] init];
    NSArray * listThemes = [NSLocalizedString(@"themes", nil) componentsSeparatedByString:@"|"];
    NSArray * listNameThemes = [NSLocalizedString(@"themesForShow", nil) componentsSeparatedByString:@"|"];
    for (int i = 0; i < listThemes.count; i++) {
        Category * aCategory = [[Category alloc] initCategoryWithName:[listThemes objectAtIndex:i]];
        [self.listCategory addObject:aCategory];
        [self.nameCategoryToShow addObject:[listNameThemes objectAtIndex:i]];
    }
}

- (void) setTypeSession : (int) position {
    NSLog(@"setTypeSession themeView");
    [super setTypeSession:position];
    NSString * typeSession = [NSString stringWithFormat:@" (%@)", [self.nameCategoryToShow objectAtIndex:position]];
    typeSession = [NSLocalizedString(@"byTheme", nil) stringByAppendingString:typeSession];
    [[NSUserDefaults standardUserDefaults] setObject:typeSession forKey:kTypeSession];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
