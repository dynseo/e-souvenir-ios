//
//  DialogIdentification.h
//  e-souvenirs
//
//  Created by Dynseo on 11/07/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StimartLib/StimartLib.h>

@interface DialogIdentification : STMDialogIdentification
- (id)initWithStyle:(int)aDialogStyle;
- (void)changeToPageAccountTypeDescriptionAction:(UIButton *)sender;
@end
