//
//  DialogIdentification.m
//  e-souvenirs
//
//  Created by Dynseo on 11/07/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "DialogIdentification.h"
#import "SouvenirInAppPurchaseHelper.h"

@implementation DialogIdentification
- (id)initWithStyle:(int)aDialogStyle {
    return [super initWithStyle:aDialogStyle];
}
- (void)changeToPageAccountTypeDescriptionAction:(UIButton *)sender {
    [super changeToPageAccountTypeDescriptionAction:sender];
    NSString * descriptionAccountType;
    switch (sender.tag) {
        case SubscriptionTypeIndividual:
            descriptionAccountType = [NSString stringWithFormat:@"%@\n\n%@", NSLocalizedString(@"description_account_individual",nil), [[SouvenirInAppPurchaseHelper sharedHelper]getProductPriceByIndex:SubscriptionTypeIndividual-1]];
            break;
        case SubscriptionTypeInstitution:
            descriptionAccountType = [NSString stringWithFormat:@"%@\n\n%@", NSLocalizedString(@"description_account_institution",nil), [[SouvenirInAppPurchaseHelper sharedHelper]getProductPriceByIndex:SubscriptionTypeInstitution-1]];
            break;
        case SubscriptionTypeProfessional:
            descriptionAccountType = [NSString stringWithFormat:@"%@\n\n%@", NSLocalizedString(@"description_account_professional", nil), [[SouvenirInAppPurchaseHelper sharedHelper]getProductPriceByIndex:SubscriptionTypeProfessional-1]];
        default:
            break;
    }
    [self setTextViewText:descriptionAccountType];
}

@end
