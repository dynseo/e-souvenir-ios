//
//  CategoryViewController.m
//  e-souvenir
//
//  Created by Dynseo on 18/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "CategoryViewController.h"
#import "SheetViewController.h"
#import "Category.h"
#import "DatabaseManager.h"
#import "Constants.h"

@interface CategoryViewController ()

@end

@implementation CategoryViewController {
    DatabaseManager * databaseManager;
}

@synthesize listCategory, nameCategory, nameCategoryToShow;

- (void)viewDidLoad {
    [super viewDidLoad];
    databaseManager = [DatabaseManager getSharedInstance];
    // Do any additional setup after loading the view.
}

- (void) initializeView {
    NSString * text;
    if ([self.nameCategory isEqualToString:@"Decade"]) {
        text = @"UNE DÉCENNIE";
    }
    else if ([self.nameCategory isEqualToString:@"Theme"]) {
        text = @"UN THÈME";
    }
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 500, 200)];
    titleLabel.center = CGPointMake(self.view.frame.size.width/2, 100);
    titleLabel.text = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"choose", nil) ,text];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:45];
    [self.view addSubview:titleLabel];
    for (int i = 0; i < self.listCategory.count; i++) {
        UIButton * imageButton = [[UIButton alloc] initWithFrame:CGRectMake(110 + i*200, 250, 200, 200)];
        if ([self.nameCategory isEqualToString:@"Theme"]) {
            imageButton = [[UIButton alloc] initWithFrame:CGRectMake((i%5)*200 + 10, 250, 200, 200)];
            if (i>4) {
                imageButton = [[UIButton alloc] initWithFrame:CGRectMake(110 + (i%5)*200 , 500, 200, 200)];
            }
        }
        else {
            if (i>3) {
                imageButton = [[UIButton alloc] initWithFrame:CGRectMake(110 + (i%4)*200 , 500, 200, 200)];
            }
        }
        NSString * imageName = [NSString stringWithFormat:@"%@%d.png", self.nameCategory, i+1];
        [imageButton setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        imageButton.hidden = NO;
        
        UILabel * aLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 140, 200, 60)];
        Category * aCategory = [listCategory objectAtIndex:i];
        NSString * text = (aCategory.start != nil && ![aCategory.start isEqualToString:@""]) ? aCategory.start : [nameCategoryToShow objectAtIndex:i];
        aLabel.text = text;
        aLabel.lineBreakMode = NSLineBreakByWordWrapping;
        aLabel.numberOfLines = 2;
        aLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:20];
        aLabel.textAlignment = NSTextAlignmentCenter;
        aLabel.hidden = NO;
        [imageButton addSubview:aLabel];
        [self.view addSubview:imageButton];
        imageButton.tag = i + 1;
        [imageButton addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    UIButton * buttonBack = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 70, 70)];
    [buttonBack setBackgroundImage:[UIImage imageNamed:@"button_home.png"] forState:UIControlStateNormal];
    [buttonBack addTarget:self action:@selector(quitCategoryViewController) forControlEvents:UIControlEventTouchUpInside];
    buttonBack.hidden = NO;
    [self.view addSubview:buttonBack];
}

- (void) onClick : (UIButton *) aButton {
    NSLog(@"button tag clicked : %ld", (long)aButton.tag);
    Category * aCategory = [self.listCategory objectAtIndex:aButton.tag - 1];
    NSLog(@"category choisi : %@ --- %@", aCategory.start, aCategory.name);
    [self setTypeSession:(int)aButton.tag - 1];
    SheetViewController *sheetViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"sheetViewController"];
    sheetViewController.listSheets = [databaseManager getSheetByCategory:aCategory];
    [self.navigationController pushViewController:sheetViewController animated:YES];
}

- (void) quitCategoryViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) setTypeSession : (int) position {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
