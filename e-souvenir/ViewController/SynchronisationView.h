//
//  SynchronisationView.h
//  e-souvenir
//
//  Created by Dynseo on 13/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StimartLib/StimartLib.h>
#import "Synchronisation.h"

@interface SynchronisationView : STMDialogView <SynchronisationDelegate>

@property (nonatomic) STMDialogView * dialogSynchro;
@property (nonatomic) UIProgressView * progressView;
@property (nonatomic) UILabel * progressValueLabel;
- (id) initSynchronisationView;
-(void) showDialogSynchroFinish;
- (void) showDialogSynchroFailure;
- (void) showDialogSynchroInProgress;
- (void) hideDialogSynchro;
- (id) initSynchronisationViewOnAuto : (BOOL) TypeData;
@end
