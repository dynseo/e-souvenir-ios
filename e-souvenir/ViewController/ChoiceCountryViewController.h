//
//  ChoiceCountryViewController.h
//  esouvenirs
//
//  Created by Dynseo on 29/10/2018.
//  Copyright © 2018 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StimartLib/StimartLib.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChoiceCountryViewController : UIViewController
@property (weak, nonatomic) IBOutlet STMButton *franceButton;
@property (weak, nonatomic) IBOutlet STMButton *belgiqueButton;
@property (weak, nonatomic) IBOutlet STMButton *suisseButton;
@property (weak, nonatomic) IBOutlet STMButton *quebecButton;
@property (weak, nonatomic) IBOutlet UIView *choiceCountryDialog;
@property (weak, nonatomic) IBOutlet UILabel *titleDialog;
@property (weak, nonatomic) IBOutlet UILabel *contentDialog;
@property (weak, nonatomic) IBOutlet STMButton *buttonYes;
@property (weak, nonatomic) IBOutlet STMButton *buttonNo;
- (IBAction)countryButtonAction:(STMButton *)sender;


- (IBAction)buttonYesAction:(id)sender;
- (IBAction)buttonNoAction:(id)sender;

@end

NS_ASSUME_NONNULL_END
