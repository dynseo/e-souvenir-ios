//
//  ChoiceCountryViewController.m
//  esouvenirs
//
//  Created by Dynseo on 29/10/2018.
//  Copyright © 2018 dynseo. All rights reserved.
//

#import "ChoiceCountryViewController.h"

@interface ChoiceCountryViewController ()

@end

@implementation ChoiceCountryViewController {
    NSString * langAlter;
    NSArray * countries;
    NSArray * arrayLangAlters;
    NSString *stringMenuButtonColorLight, *stringMenuButtonColorDark;
}

@synthesize titleDialog, contentDialog, buttonNo, buttonYes, franceButton, belgiqueButton, suisseButton, quebecButton, choiceCountryDialog;

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    arrayLangAlters = [NSLocalizedString(@"lang_alter", nil) componentsSeparatedByString:@"|"];
    langAlter = [[NSUserDefaults standardUserDefaults]objectForKey:@"lang_alter"];
    if (arrayLangAlters.count == 1 || (langAlter != nil && ![langAlter isEqualToString:@""])) {
        [self startMenuViewController];
    } else {
        if ([STMUtilities getFirstLaunchDate] != nil) {
            [[NSUserDefaults standardUserDefaults]setObject:[arrayLangAlters objectAtIndex:0] forKey:@"lang_alter"];
            [self startMenuViewController];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    titleDialog.text = NSLocalizedString(@"dialog_confirm_content", nil);
    stringMenuButtonColorLight = NSLocalizedString(@"menu_light_button_code", nil);
    stringMenuButtonColorDark = NSLocalizedString(@"menu_dark_button_code", nil);
    
    countries = [NSLocalizedString(@"countries", nil) componentsSeparatedByString:@"|"];
    NSString *choose = NSLocalizedString(@"choose", nil);
    [franceButton setFrame:franceButton.frame andType:STMButtonTypeRounded andTitle:choose andTitleSize:30 andBackgroundColor:stringMenuButtonColorDark andHighlightColor:stringMenuButtonColorLight];
    [belgiqueButton setFrame:belgiqueButton.frame andType:STMButtonTypeRounded andTitle:choose andTitleSize:30 andBackgroundColor:stringMenuButtonColorDark andHighlightColor:stringMenuButtonColorLight];
    [suisseButton setFrame:belgiqueButton.frame andType:STMButtonTypeRounded andTitle:choose andTitleSize:30 andBackgroundColor:stringMenuButtonColorDark andHighlightColor:stringMenuButtonColorLight];
    [quebecButton setFrame:quebecButton.frame andType:STMButtonTypeRounded andTitle:choose andTitleSize:30 andBackgroundColor:stringMenuButtonColorDark andHighlightColor:stringMenuButtonColorLight];
    
    [buttonYes setFrame:buttonYes.frame andType:STMButtonTypeRounded andTitle:NSLocalizedString(@"yes", nil) andTitleSize:30 andBackgroundColor:stringMenuButtonColorDark andHighlightColor:stringMenuButtonColorLight];
    [buttonNo setFrame:buttonNo.frame andType:STMButtonTypeRounded andTitle:NSLocalizedString(@"no", nil) andTitleSize:30 andBackgroundColor:stringMenuButtonColorDark andHighlightColor:stringMenuButtonColorLight];
    
    [choiceCountryDialog setHidden:YES];
    
    franceButton.tag = 1;
    belgiqueButton.tag = 2;
    suisseButton.tag = 3;
    quebecButton.tag = 4;
}


- (IBAction)countryButtonAction:(STMButton *)sender {
    contentDialog.text = [countries objectAtIndex:sender.tag-1];
    langAlter = [arrayLangAlters objectAtIndex:sender.tag-1];
    [choiceCountryDialog setHidden:NO];
}

- (IBAction)buttonYesAction:(id)sender {
    [[NSUserDefaults standardUserDefaults]setObject:langAlter forKey:@"lang_alter"];
    [choiceCountryDialog setHidden:YES];
    [self startMenuViewController];
}

- (IBAction)buttonNoAction:(id)sender {
    [choiceCountryDialog setHidden:YES];
}

- (void) startMenuViewController {
    UIViewController *menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"menuViewController"];
    [self.navigationController pushViewController:menuViewController animated:NO];
}

@end
