//
//  ScreenSlideSheetViewController.h
//  e-souvenir
//
//  Created by Dynseo on 23/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//
#import <StimartLib/StimartLib.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

@interface ScreenSlideSheetViewController : UIViewController<UIScrollViewDelegate,AVAudioPlayerDelegate,UITextViewDelegate>
@property STMDialogView * dialogView;
@property NSMutableArray * listSheetsSelected;
@property (strong, nonatomic) UIScrollView *scroll;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (strong, nonatomic) IBOutlet UIButton *tutoButton;
@property (strong, nonatomic) IBOutlet UIButton *questionButton;
@property (strong, nonatomic) IBOutlet UIButton *descriptionButton;
@property (strong, nonatomic) IBOutlet UIButton *legendButton;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *unlikeButton;
@property (strong, nonatomic) IBOutlet UIView *dialogQuestion;
@property (strong, nonatomic) IBOutlet UILabel *question1Label;
@property (strong, nonatomic) IBOutlet UILabel *question2Label;
@property (strong, nonatomic) IBOutlet UILabel *question3Label;
@property (strong, nonatomic) IBOutlet UIButton *quitDialogQuestionButton;
@property (strong, nonatomic) IBOutlet UIPageControl *sheetPageControl;
@property (strong, nonatomic) IBOutlet UIView *sheetView;
@property (strong, nonatomic) IBOutlet UILabel *sheetLabel;
- (IBAction)onClick:(UIButton *)sender;

@end
