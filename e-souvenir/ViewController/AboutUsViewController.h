//
//  AboutUsViewController.h
//  e-souvenirs
//
//  Created by Dynseo on 22/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StimartLib/StimartLib.h>

@interface AboutUsViewController : UIView
@property (nonatomic) UIView *backgroundView;
@property (nonatomic) UILabel * titleUp;
@property (nonatomic) UITextView * textAboutUs;
@property (nonatomic) STMButton * buttonBack;
- (id) initDialogAboutUs;
@end
