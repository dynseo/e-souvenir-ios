//
//  SynchronisationView.m
//  e-souvenir
//
//  Created by Dynseo on 13/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "SynchronisationView.h"
#import "SynchronisationData.h"
#import "SynchronisationResources.h"

@implementation SynchronisationView {
    DimensionAdapter * dimensionAdapter;
    Synchronisation * synchro;
    BOOL isTypeData;
    NSString * stringMenuButtonColorLight;
    NSString *stringMenuButtonColorDark;
    UIColor * colorRoundButtonTitle;
    MBProgressHUD *hud;
}

@synthesize dialogSynchro, progressView, progressValueLabel;

- (id) initSynchronisationView {
    NSLog(@"SynchronisationView");
    dimensionAdapter = [[DimensionAdapter alloc] initDimensionAdapter];
    stringMenuButtonColorLight = NSLocalizedString(@"#c3e6ec", nil);
    stringMenuButtonColorDark = NSLocalizedString(@"#b4d5da", nil);
    
    self = [super initWithType:STMDialogTypeTwoButtons andStyle:2 andBackground:@"dialog.png"];
    if (self) {
        int button1Id = 1;
        int button2Id = 2;
        int button3Id = 3;
        [self setupButtonFor:button1Id andText:NSLocalizedString(@"synchro", nil) andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
        [self setupButtonFor:button2Id andText:NSLocalizedString(@"update", nil) andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
        [self setupButtonFor:button3Id andText:NSLocalizedString(@"return", nil) andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
        self.buttonUp.tag = 1;
        self.buttonDown.tag = 2;
        [self setTarget:self forButton:button1Id action:@selector(showDialogSynchro:)];
        [self setTarget:self forButton:button2Id action:@selector(showDialogSynchro:)];
        [self setTarget:self forButton:button3Id action:@selector(returnButtonAction)];

    }
    
    return self;
}

- (id) initSynchronisationViewOnAuto : (BOOL) TypeData {
    self = [self initSynchronisationView];
    isTypeData = TypeData;
    [self showDialogSynchro:isTypeData ? self.buttonUp : self.buttonDown];
    [self synchronizationAction];
    return self;
}

- (void) synchronizationAction {
    NSLog(@"synchronizationAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [self showDialogSynchroInProgress];
    
    if (isTypeData) {
        [self showHudWithString:nil];
        NSLog(@"Data");
        synchro = [[SynchronisationData alloc] initSynchronisationData];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [(SynchronisationData *)synchro startSynchronisationData :^(BOOL success){
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (success) {
                        [self showDialogSynchroFinish];
                    }
                    else {
                        [self showDialogSynchroFailure];
                        NSLog(@"failure");
                    }
                    [MBProgressHUD hideHUDForView:self animated:YES];
                });
            }];
        });
    }
    else {
        NSLog(@"Resources");
        [progressView setHidden:NO];
        [progressValueLabel setHidden:NO];
        synchro = [[SynchronisationResources alloc] initSynchronisationResources];
        synchro.aDelegate = self;
        [(SynchronisationResources *)synchro startSynchronisationResources];
    }
    
}

- (void) returnButtonAction {
    NSLog(@"returnButtonAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [self setHidden:YES];
}

- (void) showDialogSynchro: (STMButton *) sender
{
    NSLog(@"showDialogResources");
    if ([STMUtilities isInternetConnected]) {
        BOOL isSubscriptionValid = [Subscription isSubscriptionValid];
//
//        if(sender.tag != 1){
//            
//        }
//
//
        if (isSubscriptionValid) {
            if (sender.tag == 1) {
                isTypeData = YES;
            }
            else {
                isTypeData = NO;
            }
            if (dialogSynchro == nil) {
                [self createDialogSynchro];
            }
            else {
                [dialogSynchro setHidden:NO];
            }
        }
        else {
            [STMUtilities showAlertDialogWithMessageKey:NSLocalizedString(@"unavailable_in_visit_mode", nil) andDuration:3];
        }
    }
    else {
        [STMUtilities showAlertDialogWithMessageKey:NSLocalizedString(@"internet_failed", nil) andDuration:1];
        [self setHidden:YES];
    }
}

-(void) createDialogSynchro {
    NSLog(@"createDialogSynchro");
    if (dialogSynchro == nil) {
        dialogSynchro = [[STMDialogView alloc] initWithType:STMDialogTypeTextWithOneButton andStyle:2 andBackground:@"dialog.png"];
        NSString *titleUp;
        if (isTypeData) {
            titleUp = NSLocalizedString(@"title_synchro_data", nil);
            if ([STMUtilities getLastSynchronizationDate] == nil) {
                [dialogSynchro setTextViewText:NSLocalizedString(@"passynchro_data", nil)];
            }
            else {
                NSString * lastDateSynchro = [STMUtilities getLastSynchronizationDateString];
                [dialogSynchro setTextViewText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"dernieresynchro_data", nil), lastDateSynchro]];
            }
        }
        else {
            progressValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.dimensionAdapter1.sizex100, self.dimensionAdapter1.sizey30)];
            progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 0, self.dimensionAdapter1.sizex300, self.dimensionAdapter1.sizey20)];
            [progressView.layer setCornerRadius:self.dimensionAdapter1.sizey10];
            progressView.layer.masksToBounds = YES;
            progressView.clipsToBounds = YES;
            [dialogSynchro addSubview:progressValueLabel];
            [dialogSynchro addSubview:progressView];
            progressValueLabel.textAlignment = NSTextAlignmentCenter;
            progressValueLabel.center = CGPointMake(dialogSynchro.frame.size.width/2, dialogSynchro.frame.size.height/2+self.dimensionAdapter1.sizey30);
            [progressValueLabel setFont:[UIFont boldSystemFontOfSize:dimensionAdapter.sizey15]];
            progressView.center = CGPointMake(dialogSynchro.frame.size.width/2, dialogSynchro.frame.size.height/2+self.dimensionAdapter1.sizey50);
            [progressView setTransform:CGAffineTransformMakeScale(1.0, 3.0)];
            [progressView setHidden:YES];
            [progressValueLabel setHidden:YES];
            titleUp = NSLocalizedString(@"title_synchro_res", nil);
            if ([STMUtilities getLastSynchronizationResDate] == nil) {
                [dialogSynchro setTextViewText:NSLocalizedString(@"passynchro_res", nil)];
            }
            else {
                NSString * lastDateSynchro = [STMUtilities getLastSynchronizationResDateString];
                [dialogSynchro setTextViewText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"dernieresynchro_res", nil), lastDateSynchro]];
            }
        }
        
        [dialogSynchro setTitleUpText:titleUp andSize:dimensionAdapter.sizey45];
        [dialogSynchro setupButtonFor:0 andText:NSLocalizedString(@"launch", nil) andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
        [dialogSynchro setTarget:self forButton:0 action:@selector(synchronizationAction)];
        [self addSubview:dialogSynchro];
    }
}

- (void) showDialogSynchroInProgress {
    if (dialogSynchro == nil) {
        [self createDialogSynchro];
    }
    else {
        [dialogSynchro setHidden:NO];
    }
    [dialogSynchro setTextViewText:NSLocalizedString(@"synchroencours_res", nil)];
    [[dialogSynchro buttonMiddle] setEnabled:NO];
}

-(void) showDialogSynchroFinish {
    NSLog(@"showDialogSynchroResourcesFinish");
    if (dialogSynchro == nil) {
        [self createDialogSynchro];
    }
    else {
        [dialogSynchro setHidden:NO];
    }
    if (isTypeData)
        [dialogSynchro setTextViewText:NSLocalizedString(@"synchrotermine_data", nil)];
    else
        [dialogSynchro setTextViewText:NSLocalizedString(@"synchrotermine_res", nil)];
    [[dialogSynchro buttonMiddle] setEnabled:YES];
    [dialogSynchro setupButtonFor:0 andText:NSLocalizedString(@"return", nil) andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
    [dialogSynchro setTarget:self forButton:0 action:@selector(hideDialogSynchro)];
}

- (void) showDialogSynchroFailure {
    NSLog(@"showDialogSynchroResourcesFinish");
    if (dialogSynchro == nil) {
        [self createDialogSynchro];
    }
    else {
        [dialogSynchro setHidden:NO];
    }
    if (isTypeData)
        [dialogSynchro setTextViewText:NSLocalizedString(@"synchrofailure_data", nil)];
    else
        [dialogSynchro setTextViewText:NSLocalizedString(@"synchrofailure_res", nil)];
    [[dialogSynchro buttonMiddle] setEnabled:YES];
    [dialogSynchro setupButtonFor:0 andText:NSLocalizedString(@"return", nil) andColor:stringMenuButtonColorLight andHighlight:stringMenuButtonColorDark];
    [dialogSynchro setTarget:self forButton:0 action:@selector(hideDialogSynchro)];
}

- (void) hideDialogSynchro {
    NSLog(@"synchronizationAction");
    [STMUtilities playSoundEffect:SOUND_EFFECT_DEFAULT];
    [dialogSynchro setHidden:YES];
    dialogSynchro = nil;
    [self setHidden:YES];
}

- (void)showHudWithString:(NSString *)message {
    hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.labelText = message;
}

- (void) setStringByStep : (int) step {
    NSString * stringKey = [NSString stringWithFormat:@"resource_download_step_%d", step];
    [self.dialogSynchro setTextViewText:NSLocalizedString(stringKey, nil)];
}

- (void)setProgressValue :(float) progressValue {
    NSLog(@"setProgressValue %f", progressValue);
    [dialogSynchro bringSubviewToFront:progressView];
    [dialogSynchro bringSubviewToFront:progressValueLabel];
    progressView.progress = progressValue;
    progressValueLabel.text = [NSString stringWithFormat:@"%d%%",(int) (progressValue * 100)];
}

- (void)hideProgress {
    NSLog(@"hideProgress");
    [progressView setHidden:YES];
    [progressValueLabel setHidden:YES];
    [MBProgressHUD showHUDAddedTo:self animated:YES];
}

- (void)isSynchroSuccess : (BOOL) isSuccess {
    if (isSuccess) {
        [self showDialogSynchroFinish];
        NSString * version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSLog(@"show version aller : %@", version);
        [[NSUserDefaults standardUserDefaults] setObject:version forKey:@"versionNumber"];
    }
    else {
        [self showDialogSynchroFailure];
        NSLog(@"failure");
    }
    [MBProgressHUD hideHUDForView:self animated:YES];
}
- (void)onError : (int) codeError {

}
@end
