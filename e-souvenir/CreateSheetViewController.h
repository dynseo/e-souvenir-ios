//
//  CreateSheetViewController.h
//  e-souvenirs
//
//  Created by Christophe DURAND on 24/04/2020.
//  Copyright © 2020 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StimartLib/StimartLib.h>
#import "Sheet.h"

@class CreateSheetViewController;

@protocol CreateSheetViewControllerDelegate <NSObject>
- (void)createSheetViewController:(CreateSheetViewController *)createSheetVC didFinishEnteringSheet:(Sheet *)aSheet;
@end

@interface CreateSheetViewController : UIViewController
//MARK: - Properties
@property (nonatomic, weak) id <CreateSheetViewControllerDelegate> delegate;

//MARK: - Outlets
@property (strong, nonatomic) IBOutlet UIButton *buttonTakePicture;
@property (strong, nonatomic) IBOutlet UIButton *buttonPickPicture;
@property (strong, nonatomic) IBOutlet UIButton *buttonBack;
@property (strong, nonatomic) IBOutlet UIButton *buttonSave;
@property (strong, nonatomic) IBOutlet UILabel *labelAddSheetTitle;
@property (strong, nonatomic) IBOutlet UIImageView * imageViewSheet;
@property (strong, nonatomic) IBOutlet UITextField *textFieldSheetTitle;
@property (strong, nonatomic) IBOutlet UITextField *textFieldSheetDate;
@property (strong, nonatomic) IBOutlet UITextField *textFieldFirstQuestion;
@property (strong, nonatomic) IBOutlet UITextField *textFieldSecondQuestion;
@property (strong, nonatomic) IBOutlet UITextField *textFieldThirdQuestion;
@property (strong, nonatomic) IBOutlet UIView *viewPictureChosen;

//MARK: - Actions
- (IBAction)takePictureButtonPressed:(UIButton *)sender;
- (IBAction)pickPictureButtonPressed:(UIButton *)sender;
- (IBAction)backButtonPressed:(UIButton *)sender;
- (IBAction)saveButtonPressed:(UIButton *)sender;

@end
