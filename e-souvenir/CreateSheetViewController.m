//
//  CreateSheetViewController.m
//  esouvenirs
//
//  Created by Christophe DURAND on 24/04/2020.
//  Copyright © 2020 dynseo. All rights reserved.
//

#import "CreateSheetViewController.h"
#import "SheetViewController.h"
#import "Constants.h"
#import "Sheet.h"
#import "DatabaseManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "AppDelegate.h"

@implementation CreateSheetViewController {
    DatabaseManager * databaseManager;
    NSString * imageURL;
    NSString *chosenImageName;
}

//MARK: - SYNTHESIZE
@synthesize delegate, buttonTakePicture, buttonPickPicture, buttonBack, buttonSave, labelAddSheetTitle, imageViewSheet, textFieldSheetTitle, textFieldSheetDate, textFieldFirstQuestion, textFieldSecondQuestion, textFieldThirdQuestion, viewPictureChosen;

//MARK: - VIEW LIFE CYCLE
- (void)viewDidLoad {
    [super viewDidLoad];
    // SET CLEAR COLOR TO STATUS BAR
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundHome"]];
    //allow only number input for date textfield
    [textFieldSheetDate setKeyboardType:UIKeyboardTypeNumberPad];
    // SET ROUNDED CORNER RADIUS TO VIEW PICTURE CHOSEN
    viewPictureChosen.layer.cornerRadius = 5;
    viewPictureChosen.layer.masksToBounds = true;
    // SET BORDER COLOR TO VIEW PICTURE CHOSEN
    UIColor *color = [UIColor colorWithRed:199/255.0 green:225/255.0 blue:238/255.0 alpha:1];
    viewPictureChosen.layer.borderColor = color.CGColor;
    // SET BORDER WIDTH TO VIEW PICTURE CHOSEN
    viewPictureChosen.layer.borderWidth = 5.0f;
}

//MARK: - ACTIONS
- (IBAction)takePictureButtonPressed:(UIButton *)sender {
    // INIT INSTANCE OF UIIMAGEPICKERCONTROLLER
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    // DEFINE SOURCE TYPE
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    // PRESENT IMAGE PICKER CONTROLLER
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)pickPictureButtonPressed:(UIButton *)sender {
    // INIT INSTANCE OF UIIMAGEPICKERCONTROLLER
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    // DEFINE SOURCE TYPE
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    // PRESENT IMAGE PICKER CONTROLLER
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)backButtonPressed:(UIButton *)sender {
    // DISMISS CURRENT VIEW CONTROLLER AND GO BACK TO ROOT VIEW CONTROLLER
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)saveButtonPressed:(UIButton *)sender {
    Person * currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    
    // PASS DATA TO SHEET VIEW CONTROLLER
    sqlite3_stmt * statement = nil;

    NSMutableArray * questions = [[NSMutableArray alloc] init];
    
    NSString * question1 = textFieldFirstQuestion.text;
    NSString * question2 = textFieldSecondQuestion.text;
    NSString * question3 = textFieldThirdQuestion.text;
    
    [questions addObject:question1];
    [questions addObject:question2];
    [questions addObject:question3];

    NSMutableArray * hints = [[NSMutableArray alloc] init];
    
    NSString * hint1 = @"";
    NSString * hint2 = @"";
    NSString * hint3 = @"";
    
    [hints addObject:hint1];
    [hints addObject:hint2];
    [hints addObject:hint3];
    
    NSString * nameSheet = textFieldSheetTitle.text;
    NSString * category = @"";
    NSString * date = textFieldSheetDate.text;
    NSString * resourceName = chosenImageName;
    NSString * resourceNameTH = chosenImageName;
    NSString * resourceType = @"img_s";
    NSString * type = @"P";
    NSString * like = @"";
    NSString * description = @"";
    
    int sheetId = sqlite3_column_int(statement, 0);
    int sheetServerId = sqlite3_column_int(statement, 1);
    int personId = currentPerson.serverId;
    
    Sheet *aSheet = [[Sheet alloc] initSheetWithAId:sheetId andServerId:sheetServerId andName:nameSheet andCategory:category andDate:date andQuestions:questions andHints:hints andResourceName:resourceName andResouceNameTh:resourceNameTH andResourceType:resourceType andType:type andDescription:description andLike:like];
    aSheet.idPerson = personId;
    NSLog(@"aSheet is: %@", aSheet);
    NSLog(@"aSheet name is: %@", nameSheet);
    NSLog(@"aSheet date is: %@", date);
    DatabaseManager * databaseManager = [DatabaseManager getSharedInstance];
    [databaseManager insertSheet:aSheet];
    
    [self.delegate createSheetViewController:self didFinishEnteringSheet:aSheet];
    
    SheetViewController *sheetViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"sheetViewController"];
    sheetViewController.listSheets = [databaseManager getSheetsByFrieze];
    [self.navigationController pushViewController:sheetViewController animated:YES];
    
}

//MARK: - METHODS
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    // IMAGE CHOSEN BY USER
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    NSURL *chosenImagePath = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
    //chosenImageName = [chosenImagePath lastPathComponent];
    

    
        //obtaining saving path
  
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];

    [library assetForURL:chosenImagePath resultBlock:^(ALAsset *asset) {

        ALAssetRepresentation *imageRep = [asset defaultRepresentation];

        NSLog(@"[imageRep filename] : %@", [imageRep url]);

        NSLog(@"[imageRep filename] : %@", [imageRep filename]);

        chosenImageName = [imageRep filename];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
          

        NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:chosenImageName];
        //extracting image from the picker and saving it
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        if ([mediaType isEqualToString:@"public.image"]){
            UIImage *editedImage = [info objectForKey:UIImagePickerControllerEditedImage];
            NSData *webData = UIImagePNGRepresentation(editedImage);
            [webData writeToFile:imagePath atomically:YES];
        }
        // DISPLAY IMAGE TAKEN OR PICKED
        self.imageViewSheet.image = chosenImage;
        // DISMISS CONTROLLER
        [picker dismissViewControllerAnimated:YES completion:NULL];
        // DISABLE PICK PICTURE BUTTON
        [buttonPickPicture setEnabled:NO];
        // DISABLE TAKE PICTURE BUTTON
        [buttonTakePicture setEnabled:NO];
    } failureBlock:^(NSError *error) {

        NSLog(@"[library failure] :");

    }];
    NSLog(@"[imageRep chosenImageName] : %@", chosenImageName);


}

// DISMISS KEYBOARD BY TAPPING ON SCREEN
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

@end
