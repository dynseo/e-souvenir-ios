//
//  DialogSheetView.h
//  e-souvenir
//
//  Created by Dynseo on 06/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionSheet.h"

@interface DialogSheetView : UIView
@property (strong, nonatomic) IBOutlet UILabel *titleDialog;
@property (strong, nonatomic) IBOutlet UIImageView *imageSheet;
@property (strong, nonatomic) IBOutlet UILabel *nameSheet;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *unlikeButton;
@property (strong, nonatomic) IBOutlet UILabel *legendTitle;
@property (strong, nonatomic) IBOutlet UILabel *legendSheet;
@property (strong, nonatomic) IBOutlet UIButton *timeSheet;

- (void) initializeDialogWithSessionSheet : (SessionSheet *) aSessionSheet;
- (IBAction)onClick:(id)sender;

@end
