//
//  DialogSheetView.m
//  e-souvenir
//
//  Created by Dynseo on 06/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "DialogSheetView.h"
#import "DatabaseManager.h"
#import "Sheet.h"
#import "Utilities.h"


@implementation DialogSheetView

@synthesize titleDialog, imageSheet, nameSheet, likeButton, unlikeButton, legendSheet, legendTitle, timeSheet;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) initializeDialogWithSessionSheet : (SessionSheet *) aSessionSheet {
    DatabaseManager * databaseManager = [DatabaseManager getSharedInstance];
    
    Sheet * aSheet = [databaseManager getNameSheetBySheetId:aSessionSheet.sheetId];
    nameSheet.text = aSheet.name;
    
    legendSheet.text = (aSessionSheet.legend != nil && ![aSessionSheet.legend isEqualToString:@"(null)"] && ![aSessionSheet.legend isEqualToString:@""]) ? aSessionSheet.legend : NSLocalizedString(@"no_legend", nil);
    legendSheet.font = [UIFont italicSystemFontOfSize:16.0f];
    
    if (aSessionSheet.like != nil && ![aSessionSheet.like isEqualToString:@"(null)"] && ![aSessionSheet.like isEqualToString:@""]) {
        if ([aSessionSheet.like isEqualToString:@"true"]) {
            [likeButton setBackgroundImage:[UIImage imageNamed:@"icone_like_on.png"] forState:UIControlStateNormal];
        }
        else {
            [unlikeButton setBackgroundImage:[UIImage imageNamed:@"icone_unlike_on.png"] forState:UIControlStateNormal];
        }
    }
    [timeSheet setTitle:[Utilities convertSecondeToString:aSessionSheet.sessionSheetTimer] forState:UIControlStateNormal];
    
    timeSheet.titleLabel.font = [UIFont systemFontOfSize:25];
    timeSheet.titleLabel.textColor = [UIColor whiteColor];
    NSString * nameSheetTh = [aSheet.resourceNameTh stringByAppendingString:@".png"];
    NSString  *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * fileImage = [NSString stringWithFormat:@"%@/%@", path,nameSheetTh];
    imageSheet.image = [UIImage imageNamed:fileImage];
}

- (IBAction)onClick:(id)sender {
    [self setHidden:YES];
}

@end
