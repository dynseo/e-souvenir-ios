//
//  SheetTableViewCell.m
//  e-souvenir
//
//  Created by Dynseo on 06/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "SheetTableViewCell.h"

@implementation SheetTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
