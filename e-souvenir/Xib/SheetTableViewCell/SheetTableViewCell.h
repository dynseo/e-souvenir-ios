//
//  SheetTableViewCell.h
//  e-souvenir
//
//  Created by Dynseo on 06/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SheetTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *sheetNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *sheetTime;
@property (strong, nonatomic) IBOutlet UILabel *sheetLike;
@property (strong, nonatomic) IBOutlet UILabel *sheetLegend;

@end
