//
//  SheetViewCell.h
//  e-souvenir
//
//  Created by Dynseo on 22/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SheetViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *typeSheetImage;
@property (weak, nonatomic) IBOutlet UIImageView *sheetImage;
@property (weak, nonatomic) IBOutlet UILabel *titleSheetLabel;
@property (weak, nonatomic) IBOutlet UIImageView *choisiSheetImage;
@property (weak, nonatomic) IBOutlet UIImageView *likeSheetImage;
@property (weak, nonatomic) IBOutlet UILabel *dateSheet;

@end
