//
//  AppDelegate.h
//  e-souvenir
//
//  Created by Dynseo on 18/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StimartLib/StimartLib.h>


#define ON_RETURN_NOT_AUTHORIZED 1
#define ON_RETURN_AUTHORIZED 0

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic) Person * currentPerson;
@property (strong, nonatomic) UIWindow *window;

@property (nonatomic) int onReturn;

@end

