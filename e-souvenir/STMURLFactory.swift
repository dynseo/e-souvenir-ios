//
//  STMURLFactory.swift
//  esouvenirs
//
//  Created by Pierre-Côme BETSCH on 12/09/2024.
//  Copyright © 2024 dynseo. All rights reserved.
//

import Foundation

@objc(STMURLFactorySwift)
class STMURLFactorySwift : NSObject {
    
    //The StibartLib souce code is lost, so i use this url decause the old one is deprecated
    @objc class func getURLResourcesForApp() -> String {
        "https://stimart.com/app?newPilotSession=true&service=getResourcesForApp&brand=Apple&model=iPad11,3&osVersion=18.0&" +
        "serialNumber=\(STMUtilities.getUUID()!)&" +
        "app=SOUVENIRS&lang=fr&langType=fr&appType=SOUVENIRS&deviceType=I&formatOutput=json&mode=visit";
    }
    
}
