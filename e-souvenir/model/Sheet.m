//
//  Sheet.m
//  e-souvenir
//
//  Created by Dynseo on 25/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "Sheet.h"


@implementation Sheet
@synthesize sheetId, idPerson, serverId, name, category, date, questions, hints, resourceName, resourceType, resourceNameTh, action, type, like, legend, selected, description;

- (id) initSheetWithAId : (int) aId andServerId : (int) aServerId andName : (NSString *) aName andCategory : (NSString*) aCategory andDate :(NSString *) aDate andQuestions : (NSMutableArray *) aQuestions andHints : (NSMutableArray *) aHints andResourceName : (NSString *) aResourceName andResouceNameTh : (NSString *) aResourceNameTh andResourceType : (NSString *) aResourceType andType : (NSString *) aType andDescription : (NSString *) aDescription {
    self = [super init];
    if (self) {
        sheetId = aId;
        serverId = aServerId;
        name = aName;
        category = aCategory;
        date = aDate;
        questions = aQuestions;
        hints = aHints;
        resourceName = aResourceName;
        resourceNameTh = aResourceNameTh;
        resourceType = aResourceType;
        type = aType;
        description = aDescription;
    }
    return self;
}

- (id) initSheetWithAId : (int) aId andServerId : (int) aServerId andName : (NSString *) aName andCategory : (NSString*) aCategory andDate :(NSString *) aDate andQuestions : (NSMutableArray *) aQuestions andHints : (NSMutableArray *) aHints andResourceName : (NSString *) aResourceName andResouceNameTh : (NSString *) aResourceNameTh andResourceType : (NSString *) aResourceType andType : (NSString *) aType andDescription : (NSString *) aDescription andLike : (NSString *) aLike {
    self = [self initSheetWithAId:aId andServerId : aServerId andName:aName andCategory:aCategory andDate:aDate andQuestions:aQuestions andHints:aHints andResourceName:aResourceName andResouceNameTh:aResourceNameTh andResourceType:aResourceType andType:aType andDescription:aDescription];
    like = aLike;
    
    return self;
}

- (id) initSheetWithJSONObject : (NSDictionary *) jsonSheet {
    self = [super init];
    if (self) {
        questions = [[NSMutableArray alloc] init];
        hints = [[NSMutableArray alloc] init];
        
        serverId = [[jsonSheet objectForKey:@"serverId"] intValue];
        name = [jsonSheet objectForKey:@"name"];
        category = [jsonSheet objectForKey:@"category"];
        
        NSString * test;
        
        test = [jsonSheet objectForKey:@"date"];
        if (test.length == 4) {
            date = [test stringByAppendingString:@"-00-00"];
        }
        else if (test.length == 7) {
            date = [test stringByAppendingString:@"-00"];
        }
        else
            date = test;
        
        test = [jsonSheet objectForKey:@"question1"];
        if (test != nil && ![test isEqualToString:@""]) {
            [questions addObject:test];
        }
        else {
            [questions addObject:@""];
        }
        
        test = [jsonSheet objectForKey:@"question2"];
        if (test != nil && ![test isEqualToString:@""]) {
            [questions addObject:test];
        }
        else {
            [questions addObject:@""];
        }
        
        test = [jsonSheet objectForKey:@"question3"];
        if (test != nil && ![test isEqualToString:@""]) {
            [questions addObject:test];
        }
        else {
            [questions addObject:@""];
        }
        
        test = [jsonSheet objectForKey:@"hint1"];
        if (test != nil && ![test isEqualToString:@""]) {
            [hints addObject:test];
        }
        else {
            [hints addObject:@""];
        }
        
        test = [jsonSheet objectForKey:@"hint2"];
        if (test != nil && ![test isEqualToString:@""]) {
            [hints addObject:test];
        }
        else {
            [hints addObject:@""];
        }
        
        test = [jsonSheet objectForKey:@"hint3"];
        if (test != nil && ![test isEqualToString:@""]) {
            [hints addObject:test];
        }
        else {
            [hints addObject:@""];
        }
        
        resourceName = [jsonSheet objectForKey:@"resourceName"];
        resourceNameTh = [jsonSheet objectForKey:@"resourceNameTh"];
        resourceType = [jsonSheet objectForKey:@"resourceType"];
        
        test = [jsonSheet objectForKey:@"action"];
        if (test != nil && ![test isEqualToString:@""]) {
            action = test;
        }
        else
            action = @"";
        
        type = [jsonSheet objectForKey:@"type"];
        
        test = [jsonSheet objectForKey:@"description"];
        if (test != nil && ![test isEqualToString:@""]) {
            description = test;
        }
        
        idPerson = [[jsonSheet objectForKey:@"idPerson"] intValue];
    }
    return self;
}

- (Type) getResourceType {
    if (resourceType != nil && ![resourceType isEqualToString:@""]) {
        if ([resourceType isEqualToString:@"video"]) {
            return VIDEO;
        }
        else if ([resourceType isEqualToString:@"audio"]) {
            return AUDIO;
        }
        else if ([resourceType isEqualToString:@"img"]) {
            return IMAGE;
        }
        else if ([resourceType isEqualToString:@"img_h"]) {
            return IMAGE_H;
        }
        else if ([resourceType isEqualToString:@"img_h_b"]) {
            return IMAGE_H_B;
        }
        else if ([resourceType isEqualToString:@"img_v"]) {
            return IMAGE_V;
        }
        else if ([resourceType isEqualToString:@"img_v_b"]) {
            return IMAGE_V_B;
        }
        else if ([resourceType isEqualToString:@"img_s"]) {
            return IMAGE_S;
        }
    }
    return -1;
}

- (Type) getResourceTypeSimple {
    if (resourceType != nil && ![resourceType isEqualToString:@""]) {
        if ([resourceType isEqualToString:@"video"]) {
            return VIDEO;
        }
        else if ([resourceType isEqualToString:@"audio"]) {
            return AUDIO;
        }
        else
            return IMAGE;
    }
    return 0;
}

- (NSString *) sheetToString {
    return [NSString stringWithFormat:@"%d,%d,%@,%@,%@", sheetId,serverId,name,category,date];
}

- (NSString *) sheetToString1 {
    return [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@", [questions objectAtIndex:0],[questions objectAtIndex:1],[questions objectAtIndex:2],[hints objectAtIndex:0],[hints objectAtIndex:1],[hints objectAtIndex:2]];
}

@end
