//
//  Category.m
//  e-souvenir
//
//  Created by Dynseo on 26/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "Category.h"

@implementation Category
@synthesize start, end, name;

- (id) initCategoryWithName : (NSString *) aName {
    self = [super init];
    if (self) {
        name = aName;
    }
    return self;
}
- (id) initCategoryWithStart : (NSString *) aStart andEnd : (NSString *) aEnd {
    self = [super init];
    if (self) {
        start = aStart;
        end = aEnd;
    }
    return self;
}

@end
