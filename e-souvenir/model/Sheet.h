//
//  Sheet.h
//  e-souvenir
//
//  Created by Dynseo on 25/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    VIDEO,
    AUDIO,
    IMAGE,
    IMAGE_V,
    IMAGE_V_B,
    IMAGE_H,
    IMAGE_H_B,
    IMAGE_S
} Type;

@interface Sheet : NSObject

@property int sheetId;
@property int serverId;
@property NSString * name;
@property NSString * category;
@property NSString * date;
@property NSMutableArray * questions;
@property NSMutableArray * hints;
@property NSString * resourceName;
@property NSString * resourceNameTh;
@property NSString * resourceType;
@property NSString * action;
@property NSString * type;
@property NSString * description;
@property NSString * like;
@property NSString * legend;
@property int idPerson;
@property BOOL selected;

- (id) initSheetWithAId : (int) aId andServerId : (int) aServerId andName : (NSString *) aName andCategory : (NSString*) aCategory andDate :(NSString *) aDate andQuestions : (NSMutableArray *) aQuestions andHints : (NSMutableArray *) aHints andResourceName : (NSString *) aResourceName andResouceNameTh : (NSString *) aResourceNameTh andResourceType : (NSString *) aResourceType andType : (NSString *) aType andDescription : (NSString *) aDescription;

- (id) initSheetWithAId : (int) aId andServerId : (int) aServerId andName : (NSString *) aName andCategory : (NSString*) aCategory andDate :(NSString *) aDate andQuestions : (NSMutableArray *) aQuestions andHints : (NSMutableArray *) aHints andResourceName : (NSString *) aResourceName andResouceNameTh : (NSString *) aResourceNameTh andResourceType : (NSString *) aResourceType andType : (NSString *) aType andDescription : (NSString *) aDescription andLike : (NSString *) aLike;

- (id) initSheetWithJSONObject : (NSDictionary *) jsonSheet;

- (Type) getResourceType;

- (Type) getResourceTypeSimple;

- (NSString *) sheetToString;
@end
