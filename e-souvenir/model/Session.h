//
//  Session.h
//  e-souvenir
//
//  Created by Dynseo on 26/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Session : NSObject

@property int sessionId;
@property int sessionPersonId;
@property NSString * sessionDate;
@property NSString * sessionTimer;
@property NSString * satisfaction;
@property NSString * type;

- (id) initSessionWithAId : (int) aSessionId andDate :(NSString *) aDate andType : (NSString *) aType;

- (id) initSessionWithPersonId : (int) aPersonId andTimer : (NSString *) aTimer andSatisfaction : (NSString *) aSatisfaction andType : (NSString *) aType;

- (id) initSessionWithAId : (int) aSessionId andPersonId : (int) aPersonId andDate :(NSString *) aDate andTimer : (NSString *) aTimer andSatisfaction : (NSString *) aSatisfaction andType : (NSString *) aType;


@end
