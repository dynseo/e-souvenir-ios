//
//  SessionSheet.h
//  e-souvenir
//
//  Created by Dynseo on 26/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SessionSheet : NSObject

@property int sessionSheetId;
@property int sessionId;
@property int sheetId;
@property NSString * sessionSheetTimer;
@property NSString * like;
@property NSString * legend;
@property NSString * sessionDate;

- (id) initSessionSheetWithSessionId : (int) aSessionId andSheetId : (int) aSheetId andSessionSheetTimer : (NSString *) aSessionSheetTimer andLike : (NSString *) aLike andLegend : (NSString *) aLegend;

- (id) initSessionSheetWithSessionSheetId : (int) aSessionSheetId andSessionId : (int) aSessionId andSheetId : (int) aSheetId andSessionSheetTimer : (NSString *) aSessionSheetTimer andLike : (NSString *) aLike andLegend : (NSString *) aLegend;

- (id) initSessionSheetWithSessionId : (int) aSessionId andSessionDate : (NSString *) aSessionDate;

@end
