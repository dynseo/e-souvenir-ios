//
//  SessionSheet.m
//  e-souvenir
//
//  Created by Dynseo on 26/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "SessionSheet.h"

@implementation SessionSheet
@synthesize sessionSheetId, sessionId, sheetId, sessionSheetTimer, legend, like, sessionDate;

- (id) initSessionSheetWithSessionId : (int) aSessionId andSheetId : (int) aSheetId andSessionSheetTimer : (NSString *) aSessionSheetTimer andLike : (NSString *) aLike andLegend : (NSString *) aLegend {
    self = [super init];
    if (self) {
        sessionId = aSessionId;
        sheetId   = aSheetId;
        sessionSheetTimer = aSessionSheetTimer;
        like = aLike;
        legend = aLegend;
    }
    return self;
}

- (id) initSessionSheetWithSessionSheetId : (int) aSessionSheetId andSessionId : (int) aSessionId andSheetId : (int) aSheetId andSessionSheetTimer : (NSString *) aSessionSheetTimer andLike : (NSString *) aLike andLegend : (NSString *) aLegend {
    self = [self initSessionSheetWithSessionId:aSessionId andSheetId:aSheetId andSessionSheetTimer:aSessionSheetTimer andLike:aLike andLegend:aLegend];
    sessionSheetId = aSessionSheetId;
    return self;
}

- (id) initSessionSheetWithSessionId : (int) aSessionId andSessionDate : (NSString *) aSessionDate {
    self = [super init];
    if (self) {
        sessionDate = aSessionDate;
        sessionId = aSessionId;
    }
    return self;
}


@end
