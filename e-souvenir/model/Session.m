//
//  Session.m
//  e-souvenir
//
//  Created by Dynseo on 26/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "Session.h"

@implementation Session
@synthesize sessionId, sessionPersonId, sessionDate, sessionTimer, satisfaction, type;

- (id) initSessionWithAId : (int) aSessionId andDate :(NSString *) aDate andType : (NSString *) aType {
    self = [super init];
    if (self) {
        sessionId = aSessionId;
        sessionDate = aDate;
        type = aType;
    }
    return self;
}

- (id) initSessionWithPersonId : (int) aPersonId andTimer : (NSString *) aTimer andSatisfaction : (NSString *) aSatisfaction andType : (NSString *) aType {
    if (self) {
        sessionPersonId = aPersonId;
        sessionTimer = aTimer;
        satisfaction = aSatisfaction;
        type = aType;
    }
    return self;
}

- (id) initSessionWithAId : (int) aSessionId andPersonId : (int) aPersonId andDate :(NSString *) aDate andTimer : (NSString *) aTimer andSatisfaction : (NSString *) aSatisfaction andType : (NSString *) aType {
    self = [self initSessionWithAId:aSessionId andDate:aDate andType:aType];
    sessionPersonId = aPersonId;
    sessionTimer = aTimer;
    satisfaction = aSatisfaction;
    return self;
}

@end
