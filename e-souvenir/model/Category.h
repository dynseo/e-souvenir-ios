//
//  Category.h
//  e-souvenir
//
//  Created by Dynseo on 26/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category : NSObject

@property NSString * start;
@property NSString * end;
@property NSString * name;

- (id) initCategoryWithName : (NSString *) aName;
- (id) initCategoryWithStart : (NSString *) aStart andEnd : (NSString *) aEnd;

@end
