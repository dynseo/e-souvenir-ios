//
//  Utilities.m
//  e-souvenir
//
//  Created by Dynseo on 07/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities

+ (NSString *) convertSecondeToString : (NSString *) time {
    NSString * timeString;
    int sec = [time intValue];
    if(sec >= 60)
    {
        int min = sec/60;
        sec -= min*60;
        timeString = [NSString stringWithFormat:@"%dm %ds", min, sec];
    }
    else
        timeString = [NSString stringWithFormat:@"%ds", sec];
    return timeString;
}

+ (NSString *) convertMillisecondeToString : (NSString *) time {
    int sec = [time intValue]/1000;
    return [self convertSecondeToString:[NSString stringWithFormat:@"%d", sec]];
}

+ (NSString *) modifyFormatDate : (NSString *) inputFormat : (NSString *) outputFormat : (NSString *) aStringDate {
    NSDateFormatter * dateFormatInput = [[NSDateFormatter alloc]init];
    [dateFormatInput setDateFormat:inputFormat];
    NSDateFormatter * dateFormatOutput = [[NSDateFormatter alloc]init];
    [dateFormatOutput setDateFormat:outputFormat];
    NSDate * date = [dateFormatInput dateFromString:aStringDate];
    return [dateFormatOutput stringFromDate:date];
}

@end
