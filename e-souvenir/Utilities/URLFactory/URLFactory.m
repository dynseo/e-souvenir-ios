//
//  URLFactory.m
//  e-souvenir
//
//  Created by Dynseo on 15/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "URLFactory.h"
#import "SouvenirInAppPurchaseHelper.h"

NSString * const VAL_SYNCHRO_SESSION = @"addSession";
NSString * const PARAM_SATISFACTION = @"&satisfaction=";
NSString * const PARAM_TYPE= @"&type=";

NSString * const VAL_SYNCHRO_SESSION_SHEET = @"addSessionSheet";
NSString * const PARAM_SERVER_ID_SESSION= @"&sessionId=";
NSString * const PARAM_SERVER_ID_SHEET= @"&sheetId=";
NSString * const PARAM_LIKE= @"&liked=";
NSString * const PARAM_LEGEND= @"&legend=";

@implementation URLFactory

+ (NSString *) urlSendSessionWithASession : (Session *) aSession andAServerPersonId : (int) aServerPersonId {
    
    NSString * url = [[NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%d%@%@%@%@%@%@%@%@", BASE_URL, PARAM_SERVICE, VAL_SYNCHRO_SESSION, PARAM_SERIAL_NUMBER, [STMUtilities getUUID], PARAM_APP, [STMUtilities getAppSubName], PARAM_APP_LANG, [STMUtilities getAppLanguage], PARAM_PERSON_ID, aServerPersonId, PARAM_DATE, aSession.sessionDate, PARAM_DURATION, aSession.sessionTimer, PARAM_SATISFACTION, aSession.satisfaction, PARAM_TYPE, aSession.type]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"urlSendSessionWithASession : %@", url);
    return url;
}

+ (NSString *) urlSendSessionSheetWithASessionSheet : (SessionSheet *) aSessionSheet andAServerSessionId : (int) aServerSessionId andAServerSheetId : (int) aServerSheetId {
    NSString * url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@%d%@%d%@%@", BASE_URL, PARAM_SERVICE, VAL_SYNCHRO_SESSION_SHEET, PARAM_SERIAL_NUMBER, [STMUtilities getUUID], PARAM_APP, [STMUtilities getAppSubName], PARAM_APP_LANG, [STMUtilities getAppLanguage], PARAM_SERVER_ID_SESSION, aServerSessionId, PARAM_SERVER_ID_SHEET, aServerSheetId, PARAM_DURATION, aSessionSheet.sessionSheetTimer];
    
    if (aSessionSheet.like != nil && ![aSessionSheet.like isEqualToString:@""]) {
        url = [NSString stringWithFormat:@"%@%@%@", url, PARAM_LIKE, aSessionSheet.like];
    }
    
    if (aSessionSheet.legend != nil && ![aSessionSheet.legend isEqualToString:@""]) {
        url = [NSString stringWithFormat:@"%@%@%@", url, PARAM_LEGEND, aSessionSheet.legend];
    }
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"urlSendSessionSheetWithASessionSheet : %@", url);
    return url;
}

+ (NSDictionary *)getURLNotifyServerPurchaseParametersWithProductId:(NSString *)productId
{
    NSData *receipt = [SouvenirInAppPurchaseHelper getInAppPurchaseReceipt];
    NSDictionary *parameters = @{@"app":[STMUtilities getAppSubName],@"lang":[STMUtilities getAppLanguage],@"serialNumber":[STMUtilities getUUID],@"appVersion":[STMUtilities getAppVersion],@"productId":productId, @"receipt":[receipt base64EncodedStringWithOptions:0]};
    NSLog(@"productId = %@",productId);
    return parameters;
}

@end
