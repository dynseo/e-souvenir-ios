//
//  URLFactory.h
//  e-souvenir
//
//  Created by Dynseo on 15/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StimartLib/StimartLib.h>
#import "Session.h"
#import "SessionSheet.h"

@interface URLFactory : STMURLFactory

+ (NSString *) urlSendSessionWithASession : (Session *) aSession andAServerPersonId : (int) aServerPersonId;

+ (NSString *) urlSendSessionSheetWithASessionSheet : (SessionSheet *) aSessionSheet andAServerSessionId : (int) aServerSessionId andAServerSheetId : (int) aServerSheetId;

+ (NSDictionary *)getURLNotifyServerPurchaseParametersWithProductId:(NSString *)productId;
@end
