//
//  SynchronisationData.m
//  e-souvenir
//
//  Created by Dynseo on 13/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "SynchronisationData.h"
#import "DatabaseManager.h"
#import "URLFactory.h"

#define kUserDefaultsKeySubscriptionPurchaseNotSentToServerProductId @"subscription_not_sent_to_server_product_id"

@implementation SynchronisationData {
    DatabaseManager * databaseManager;
    
    int nbPersonsToSend;
    int nbSessionsToSend;
    int nbSessionsSheetToSend;
}

- (id) initSynchronisationData {
    self = [super initSynchronisation:YES];
    nbPersonsToSend = 0;
    nbSessionsToSend = 0;
    nbSessionsSheetToSend = 0;
    databaseManager = [DatabaseManager getSharedInstance];
    return self;
}

- (void) startSynchronisationData :(void (^)(BOOL success))completionBlock {
    BOOL isValidUserWithoutAccount = [Account isValidUserWithoutAccount];
    [self resendPurchaseIfNeeded];
    if (![Account isVisitMode] && !isValidUserWithoutAccount) {
        NSLog(@"Synchro");
        [self launchSynchro : completionBlock];
    }
    else {
        [self notifyEndOfSynchronizationToServer : completionBlock];
    }
}

- (void)resendPurchaseIfNeeded
{
    NSLog(@"resendPurchaseIfNeeded");
    NSString *stringIdProductPurchasedNotSentToServer = [[NSUserDefaults standardUserDefaults]objectForKey:kUserDefaultsKeySubscriptionPurchaseNotSentToServerProductId];
    if (stringIdProductPurchasedNotSentToServer != nil) {
        NSLog(@"resendPurchaseIfNeeded - product that not sent to server found, send again!");
        [self notifyServerPurchaseWithProductId:stringIdProductPurchasedNotSentToServer];
    }
}

/* send the purchase receipt to server for validation */
- (void)notifyServerPurchaseWithProductId:(NSString *)productId
{
    NSLog(@"notifyServerPurchaseWithProductId");
    NSString * requestURL = [STMURLFactory getURLNotifyServerPurchase];
    [self.requestManager POST:requestURL parameters:[URLFactory getURLNotifyServerPurchaseParametersWithProductId:productId] success:^(AFHTTPRequestOperation * operation, id responseObject) {
        NSLog(@"notifyServerPurchase - server response: %@", responseObject);
        NSString *stringDateEnd = [responseObject objectForKey:kJsonParamSubscriptionDateEnd];
        if (stringDateEnd != nil) {
            [Subscription updateSubscriptionEndDateWithString:stringDateEnd];
            [Subscription updateSubscriptionState:@"OK"];
            [[NSUserDefaults standardUserDefaults]setObject:nil forKey:kUserDefaultsKeySubscriptionPurchaseNotSentToServerProductId];
        }
        else {
            NSString *stringError = [responseObject objectForKey:kJsonParamError];
            if (stringError != nil) {
                NSLog(@"purchase not valided by server, reject subscription, error = %@", stringError);
                [Subscription rejectLastSubscriptionUpdate];
            }
        }
        [[NSUserDefaults standardUserDefaults]synchronize];
    } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
        NSLog(@"ERROR - notifyServerPurchase: error = %@, for URL = %@", error, requestURL);
        [[NSUserDefaults standardUserDefaults]setObject:productId forKey:kUserDefaultsKeySubscriptionPurchaseNotSentToServerProductId];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }];
}

- (void) launchSynchro : (void (^)(BOOL success))completionBlock {
    NSLog(@"sendAllNewPersonsToServer");
    NSArray * personsToSend = [databaseManager getAllPersonsWithoutServerId];
    nbPersonsToSend = (int) [personsToSend count] ;
    if (nbPersonsToSend != 0) {
        for (Person * personTemp in personsToSend) {
            [self sendNewPersonToServer:personTemp existLocal:YES:completionBlock];
        }
    }
    
    else {
        NSLog(@"getPersonsFromServer");
        [self getPersonsFromServer:completionBlock];
    }
}

- (void) sendAllSessionsToServer : (void (^)(BOOL success))completionBlock {
    NSLog(@"sendAllNewSessionsToServer");
    NSMutableArray * mSessions = [databaseManager getSessionsWithoutServerId];
    nbSessionsToSend = (int) [mSessions count];
    if (nbSessionsToSend != 0) {
        for (Session * sessionTemp in mSessions) {
            [self sendNewSessionToServer:sessionTemp:completionBlock];
        }
    }
    
    else {
        [self sendAllSessionsSheetToServer:completionBlock];
    }
}

- (void) sendAllSessionsSheetToServer : (void (^)(BOOL success))completionBlock {
    NSLog(@"sendAllNewSessionsSheetToServer");
    NSMutableArray * mSessionsSheet = [databaseManager getSessionsSheetWithoutServerId];
    nbSessionsSheetToSend = (int) [mSessionsSheet count];
    if (nbSessionsSheetToSend != 0) {
        for (SessionSheet * sessionSheetTemp in mSessionsSheet) {
            [self sendNewSessionSheetToServer:sessionSheetTemp: completionBlock];
        }
    }
    else {
        [self notifyEndOfSynchronizationToServer:completionBlock];
    }
}



- (void)sendNewPersonToServer:(Person *)person existLocal:(BOOL)existLocal : (void (^)(BOOL success))completionBlock
{
    NSLog(@"sendPersonToServer");
    NSString * requestURL = [STMURLFactory getURLAddPerson:person existLocal:existLocal];
    [self.requestManager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
        NSLog(@"sendNewPersonToServer - server response: %@", responseObject);
        nbPersonsToSend --;
        /* a JSON element should be a NSDictionary but not a NSString */
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            /* server response case 1 */
            if ([responseObject objectForKey:kJsonParamServerId] != nil) {
                [databaseManager modifyServerId:[[responseObject objectForKey:kJsonParamServerId]intValue] forPerson:person];
            }
            /* server response case 2 */
            else if ([responseObject objectForKey:kJsonParamConflictId] != nil) {
                [databaseManager modifyConflictId:[[responseObject objectForKey:kJsonParamConflictId]intValue] forPerson:person];
            }
            /* server response case 3 */
            else if ([responseObject objectForKey:kJsonParamPerson] != nil) {
                [databaseManager updatePerson:person withJson:responseObject];
            }
            else {
                NSLog(@"ERROR - sendPersonToServer unknown server response for URL: %@", requestURL);
            }
        }
        
        if (nbPersonsToSend == 0) {
            [self getPersonsFromServer: completionBlock];
        }
    } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
        NSLog(@"ERROR - sendPersonToServer: %@", error);
        [self getPersonsFromServer: completionBlock];
    }];
}

/* get all persons created on the server but don't exist locally (when synchronization) */
- (void)getPersonsFromServer : (void (^)(BOOL success))completionBlock
{
    NSLog(@"getPersonsFromServer");
    NSString * requestURL = [STMURLFactory getURLSynchroPersons];
    [self.requestManager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
        NSLog(@"getPersonsFromServer - server response: %@", responseObject);
        NSArray * responseObjects = [responseObject allObjects];
        for (id object in responseObjects) {
            /* a JSON element should be a NSDictionary but not a NSString */
            if ([object isKindOfClass:[NSDictionary class]]) {
                int serverId = [[object objectForKey:kJsonParamServerId]intValue];
                Person * person = [databaseManager getPersonWithServerId:serverId];
                BOOL isActivePerson = [[object objectForKey:kJsonParamPersonIsActive]isEqualToString:kJsonParamActiveAccount];
                /* person exists in local database */
                if (person != nil) {
                    /* person is active, update person */
                    if (isActivePerson) {
                        [databaseManager updatePersonWithId:person.personId andServerId:serverId andConflictId:-1 andFirstName:[object objectForKey:kJsonParamPersonFirstname] andName:[object objectForKey:kJsonParamPersonName] andBirthdate:[object objectForKey:kJsonParamPersonBirthdate] andSex:[object objectForKey:kJsonParamPersonSex] andRole:[object objectForKey:kJsonParamPersonRole]];
                    }
                    /* person is inactive, delete the person */
                    else {
                        [databaseManager deletePersonWithId:person.personId];
                    }
                }
                /* person don't exist in local database */
                else {
                    /* create person if person is active */
                    if (isActivePerson) {
                        [databaseManager insertPersonWithServerId:serverId andConflictId:-1 andFirstName:[object objectForKey:kJsonParamPersonFirstname] andName:[object objectForKey:kJsonParamPersonName] andBirthdate:[object objectForKey:kJsonParamPersonBirthdate] andSex:[object objectForKey:kJsonParamPersonSex] andRole:[object objectForKey:kJsonParamPersonRole]];
                    }
                }
            }
        }
        
        [self sendAllSessionsToServer : completionBlock];
    } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
        NSLog(@"ERROR - getPersonsFromServer error: %@, for URL: %@", error, requestURL);
    }];
}


- (void) sendNewSessionToServer : (Session *) aSession : (void (^)(BOOL success))completionBlock {
    NSLog(@"sendNewSessionToServer");
    int serverPersonId = [databaseManager getPersonServerIdByPersonId:aSession.sessionPersonId];
    NSString * requestURL = [URLFactory urlSendSessionWithASession:aSession andAServerPersonId:serverPersonId];
    [self.requestManager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
        NSLog(@"sendNewSessionToServer - server response: %@", responseObject);
        nbSessionsToSend --;
        /* a JSON element should be a NSDictionary but not a NSString */
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            /* server response case 1 */
            if ([responseObject objectForKey:kJsonParamResultId] != nil) {
                int answer = [[responseObject objectForKey:kJsonParamResultId] intValue];
                [databaseManager setServerIdToSession:aSession.sessionId :answer];
            }
            else {
                NSLog(@"ERROR - sendNewSessionToServer unknown server response for URL: %@", requestURL);
            }
        }
        if (nbSessionsToSend == 0) {
            [self sendAllSessionsSheetToServer:completionBlock];
        }
    } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
        NSLog(@"ERROR - sendPersonToServer: %@", error);
        [self sendAllSessionsSheetToServer:completionBlock];
    }];

}

- (void) sendNewSessionSheetToServer : (SessionSheet *) aSessionSheet : (void (^)(BOOL success))completionBlock {
    NSLog(@"sendNewSessionSheetToServer");
    int serverIdSession = [databaseManager getSessionServerIdBySessionId:aSessionSheet.sessionId];
    int serverIdSheet = [databaseManager getSheetServerIdBySheetId:aSessionSheet.sheetId];
    
    NSString * requestURL = [URLFactory urlSendSessionSheetWithASessionSheet:aSessionSheet andAServerSessionId:serverIdSession andAServerSheetId:serverIdSheet];
    [self.requestManager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
        NSLog(@"sendNewSessionToServer - server response: %@", responseObject);
        nbSessionsSheetToSend --;
        /* a JSON element should be a NSDictionary but not a NSString */
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            /* server response case 1 */
            if ([responseObject objectForKey:kJsonParamResultId] != nil) {
                int answer = [[responseObject objectForKey:kJsonParamResultId] intValue];
                [databaseManager setServerIdToSessionSheet:aSessionSheet.sessionSheetId :answer];
            }
            else {
                NSLog(@"ERROR - sendNewSessionToServer unknown server response for URL: %@", requestURL);
            }
        }
        if (nbSessionsSheetToSend == 0) {
            [self notifyEndOfSynchronizationToServer:completionBlock];
        }
    } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
        NSLog(@"ERROR - sendPersonToServer: %@", error);
        [self notifyEndOfSynchronizationToServer:completionBlock];
    }];
}
@end
