//
//  SynchronisationResources.h
//  e-souvenir
//
//  Created by Dynseo on 13/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "Synchronisation.h"

@interface SynchronisationResources : Synchronisation <NSURLSessionDelegate, NSURLSessionDownloadDelegate>
- (id) initSynchronisationResources;
- (void) startSynchronisationResources;
+ (BOOL) hasResources;

@end
