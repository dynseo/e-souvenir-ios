//
//  Synchronisation.m
//  e-souvenir
//
//  Created by Dynseo on 13/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "Synchronisation.h"
#import <StimartLib/StimartLib.h>
#import "DatabaseManager.h"

@implementation Synchronisation {

}

@synthesize dataType, requestManager;

- (id) initSynchronisation : (BOOL) aDataType {
    self = [super init];
    self.dataType = aDataType;
    self.requestManager = [AFHTTPRequestOperationManager manager];
    self.requestManager.responseSerializer.acceptableContentTypes = [self.requestManager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    return self;
}

- (void)notifyEndOfSynchronizationToServer : (void (^)(BOOL success))completionBlock
{
    NSLog(@"notifyEndOfSynchronizationToServer");
    NSString * requestURL = [STMURLFactory getURLSynchroEnd : self.dataType];
    [self.requestManager GET:requestURL parameters:nil success:^(AFHTTPRequestOperation * operation, id responseObject) {
        NSLog(@"notifyEndOfSynchronizationToServer - server response: %@", responseObject);
        BOOL isServerResponseUnknown = YES;
        /* a JSON element should be a NSDictionary but not a NSString */
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            /* server response with date of synchronization */
            if ([responseObject objectForKey:kJsonParamSynchroEnd] != nil || [responseObject objectForKey:kJsonParamSynchroResDate] != nil) {
                NSLog(@"notifyEndOfSynchronizationToServer - OK");
                isServerResponseUnknown = NO;
                [self updateSubscriptionAndAccountInfoWithJsonString:responseObject];
                
                if ([[STMDateManager getSharedInstance] stringToDate:[responseObject objectForKey:kJsonParamSynchroEnd]]) {
                    [[NSUserDefaults standardUserDefaults] setObject:[[STMDateManager getSharedInstance] stringToDate:[responseObject objectForKey:kJsonParamSynchroEnd]] forKey:USER_DEFAULTS_KEY_LAST_SYNCHRONIZATION_DATE];
                    NSLog(@"save dataDate : OK");
                    
                }
                if ([[STMDateManager getSharedInstance] stringToDate:[responseObject objectForKey:kJsonParamSynchroResDate]]) {
                    [[NSUserDefaults standardUserDefaults] setObject:[[STMDateManager getSharedInstance] stringToDate:[responseObject objectForKey:kJsonParamSynchroResDate]] forKey:USER_DEFAULTS_KEY_LAST_SYNCHRONIZATION_RESOURCES];
                    NSLog(@"save resDate : KO");
                }
            }
        }
        /* server response unknown */
        if (isServerResponseUnknown) {
            NSLog(@"ERROR - notifyEndOfSynchronizationToServer unknown server response for URL %@",requestURL);
        }
        if(completionBlock)
            completionBlock(YES);
    } failure:^(AFHTTPRequestOperation * operation, NSError * error) {
        NSLog(@"ERROR - notifyEndOfSynchronizationToServer: error = %@, for URL = %@", error, requestURL);
    }];
}

/* for all server responses from identification/subscription/synchronization, update subscription and account information */
- (void)updateSubscriptionAndAccountInfoWithJsonString:(NSString *)jsonString
{
    NSLog(@"updateSubscriptionAndAccountInfoWithJsonString");
    /* update subscription information */
    Subscription * subscription = [[Subscription alloc]initFromJsonStringIdentification:jsonString];
    [subscription saveToUserDefaults];
    
    Account *account = [[Account alloc]initFromJsonString:jsonString];
    /* save account information to user defaults */
    [account saveToUserDefaults];
    
    [Subscription print];
    [Account print];
}

@end
