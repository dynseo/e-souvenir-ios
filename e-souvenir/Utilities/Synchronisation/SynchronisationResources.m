//
//  SynchronisationResources.m
//  e-souvenir
//
//  Created by Dynseo on 13/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "SynchronisationResources.h"
#import "DatabaseManager.h"
#import "esouvenirs-Swift.h"

@implementation SynchronisationResources
static DatabaseManager * databaseManager;

- (id) initSynchronisationResources {
    self = [super initSynchronisation:NO];
    return self;
}

- (void) startSynchronisationResources {
    [self.aDelegate setStringByStep :1];
    NSString * documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * imageDirectory = [documentsDirectory stringByAppendingString:@"/images"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSLog(@"filePathImage %@", imageDirectory);
    NSString * stringURL;
    
    //The StibartLib souce code is lost, so i use this url decause the old one is deprecated

    
//    if (![fileManager fileExistsAtPath:imageDirectory]) {
//        NSLog(@"image does not exist, we need to download resources !!!");
//        stringURL = [STMURLFactory getURLResourcesForApp : [Account isVisitMode] : YES];
//    }
//    else {
//        NSLog(@"image existed, we don't need to download resources !!!");
//        stringURL = [STMURLFactory getURLResourcesForApp : [Account isVisitMode] : NO];
//    }
    
    stringURL = [STMURLFactorySwift getURLResourcesForApp];
    
    NSURL  *url = [NSURL URLWithString:stringURL];
    
    //NSURLSessionConfiguration * sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSessionConfiguration  * sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"downloadResourcesTaskBackground"];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:nil];
    NSURLSessionDownloadTask *downloadTask = [session downloadTaskWithURL:url];
    [downloadTask resume];
    return;
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location {
    NSLog(@"didFinishDownloadingToURL");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.aDelegate hideProgress];
        [self.aDelegate setStringByStep :2];
    });
    NSError * err;
    BOOL res;
    NSString * documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSURL * filePathDownload = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"resources.zip"]];
    NSString * filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"resources.zip"];
    if ([fileManager fileExistsAtPath:filePath]) {
        [fileManager removeItemAtPath:filePath error:&err];
    }
    [fileManager moveItemAtURL:location toURL:filePathDownload error:&err];
    [Unzipper upZipFileWithFilePath:filePath];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.aDelegate setStringByStep :3];
    });
    filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"sheets.json"];
    if ([fileManager fileExistsAtPath:filePath]){
        [SynchronisationResources saveResourcesToDB:filePath];
        res = [fileManager removeItemAtPath:filePath error:&err];
    }
    
    filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"sheetsToDelete.json"];
    if ([fileManager fileExistsAtPath:filePath]) {
        [SynchronisationResources deleteSheets:filePath];
        res = [fileManager removeItemAtPath:filePath error:&err];
    }
    
    filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory, @"mediasToDelete.json"];
    if ([fileManager fileExistsAtPath:filePath]) {
        [SynchronisationResources deleteMediaFiles:filePath];
        res = [fileManager removeItemAtPath:filePath error:&err];
    }
    
    NSLog(@"finish insert");
    [session finishTasksAndInvalidate];
    [self notifyEndOfSynchronizationToServer:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.aDelegate isSynchroSuccess:YES];
    });
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didResumeAtOffset:(int64_t)fileOffset expectedTotalBytes:(int64_t)expectedTotalBytes {
    
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
    NSLog(@"%s: %@ %@", __FUNCTION__, error, task.originalRequest.URL.lastPathComponent);
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    NSLog(@"didWriteData");
    float progress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.aDelegate setProgressValue:progress];
    });
}

+(BOOL) hasResources {
    NSString  *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString * filePathForCheckResourcesImages = [NSString stringWithFormat:@"%@/%@", path, @"images"];
    NSString * filePathForCheckResourcesAudios = [NSString stringWithFormat:@"%@/%@", path, @"audios"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePathForCheckResourcesImages] &&  [[NSFileManager defaultManager] fileExistsAtPath:filePathForCheckResourcesAudios])
        return YES;
    return NO;
}

+ (void) deleteSheets : (NSString *) filePath {
    NSLog(@"deleteSheets");
    NSString * content = [self loadContentFromFile:filePath];
    if (content != nil) {
        databaseManager = [DatabaseManager getSharedInstance];
        [databaseManager openDatabase];
        [self deleteSheetsFromString:content];
        [databaseManager closeDatabase];
    }
}

+ (void) deleteMediaFiles : (NSString *) filePath {
    NSLog(@"deleteMediaFiles");
    NSString * content = [self loadContentFromFile:filePath];
    if (content != nil) {
        databaseManager = [DatabaseManager getSharedInstance];
        [databaseManager openDatabase];
        [self deleteMediaFilesFromString:content];
        [databaseManager closeDatabase];
    }
}

+ (void) saveResourcesToDB : (NSString *) filePath {
    NSLog(@"saveResourcesToDB");
    NSString* content = [self loadContentFromFile:filePath];
    if (content != nil) {
        databaseManager = [DatabaseManager getSharedInstance];
        [databaseManager openDatabase];
        [self manageSheet:@"sheetsStandard" andResult:content];
        [self manageSheet:@"sheetsPerso" andResult:content];
        [databaseManager closeDatabase];
    }
}

+ (NSString *) loadContentFromFile : (NSString *) filePath {
    NSString* content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    return content;
}

+ (void) manageSheet : (NSString *) sheets andResult : (NSString *) aResult {
    NSError *error;
    NSData* data = [aResult dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSArray * jsonArraySheets = [json objectForKey:sheets];
    NSLog(@"count elements : %lu", (unsigned long)jsonArraySheets.count);
    for (id jsonElement in jsonArraySheets) {
        Sheet * aSheet = [[Sheet alloc] initSheetWithJSONObject:jsonElement];
        NSString * path = [databaseManager pathNameSheet:aSheet.serverId];
        
        if ([aSheet.action isEqualToString:@"U"] && path != nil && ![path isEqualToString:@""]) {
            NSLog(@"Update Sheet");
            if (![path isEqualToString:aSheet.resourceName]) {
                NSLog(@"delete filePath and filePathTh");
            }
            [databaseManager updateSheet:aSheet];
        }
        else if ([aSheet.action isEqualToString:@"D"]) {
            NSLog(@"Delete Sheet");
            int nbSameSheet = [databaseManager deleteSheet:aSheet andCheckFileNeed: YES];
            if (nbSameSheet <=1) {
                NSLog(@"delete filePath and filePathTh");
            }
        }
        else {
            NSLog(@"Add Sheet");
            BOOL success = [databaseManager insertSheet:aSheet];
            if (!success) {
                NSLog(@"error insert sheet");
            }
        }
    }
}

+ (void) deleteSheetsFromString : (NSString *) jsonAllSheets {
    NSError *error;
    NSData* data = [jsonAllSheets dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSArray * jsonArraySheets = [json objectForKey:@"sheets"];
    for (id jsonElement in jsonArraySheets) {
        NSString * idList = [jsonElement objectForKey:@"sheetList"];
        BOOL succes = [databaseManager deleteSheetByListId:idList];
        if (!succes) {
            NSLog(@"deleteSheetsFromString failure");
        }
    }
}

+ (void) deleteMediaFilesFromString : (NSString *) mediaToDelete {
    NSString  *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *error;
    NSData* data = [mediaToDelete dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSArray * jsonArrayMediaFiles = [json objectForKey:@"files"];
    
    for (id jsonElement in jsonArrayMediaFiles) {
        NSString * type = [jsonElement objectForKey:@"type"];
        if ([type isEqualToString:@"image"]) {
            [STMUtilities deleteFileWithPath:[NSString stringWithFormat:@"%@/%@", path, [jsonElement objectForKey:@"file"]]];
            [STMUtilities deleteFileWithPath:[NSString stringWithFormat:@"%@/%@", path, [jsonElement objectForKey:@"thumbnail"]]];
        }
        else if ([type isEqualToString:@"audio"]) {
            [STMUtilities deleteFileWithPath:[NSString stringWithFormat:@"%@/%@", path, [jsonElement objectForKey:@"file"]]];
            [STMUtilities deleteFileWithPath:[NSString stringWithFormat:@"%@/%@", path, [jsonElement objectForKey:@"thumbnail"]]];
        }
        else if ([type isEqualToString:@"video"]) {
            [STMUtilities deleteFileWithPath:[NSString stringWithFormat:@"%@/%@", path, [jsonElement objectForKey:@"thumbnail"]]];
        }
    }
}
@end
