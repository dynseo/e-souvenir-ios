//
//  Synchronisation.h
//  e-souvenir
//
//  Created by Dynseo on 13/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StimartLib/StimartLib.h>
@protocol SynchronisationDelegate;

@interface Synchronisation : NSObject

@property (nonatomic) id<SynchronisationDelegate> aDelegate;

@property(nonatomic) AFHTTPRequestOperationManager * requestManager;
// 1 : data  &  2 : resouces
@property(nonatomic) BOOL dataType;
- (id) initSynchronisation : (BOOL) aDataType;
- (void)notifyEndOfSynchronizationToServer : (void (^)(BOOL success))completionBlock;
- (void)updateSubscriptionAndAccountInfoWithJsonString:(NSString *)jsonString;
@end

@protocol SynchronisationDelegate <NSObject>
@optional
- (void)onError : (int) codeError;
- (void)setStringByStep : (int) step;
- (void)setProgressValue :(float) progressValue;
- (void)hideProgress;
- (void)isSynchroSuccess : (BOOL) isSuccess;
@end
