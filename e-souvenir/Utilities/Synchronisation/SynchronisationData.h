//
//  SynchronisationData.h
//  e-souvenir
//
//  Created by Dynseo on 13/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "Synchronisation.h"
#import "Session.h"

@interface SynchronisationData : Synchronisation

- (id) initSynchronisationData;

- (void) startSynchronisationData :(void (^)(BOOL success))completionBlock;
- (void) launchSynchro : (void (^)(BOOL success))completionBlock;
- (void)sendNewPersonToServer:(Person *)person existLocal:(BOOL)existLocal : (void (^)(BOOL success))completionBlock;
- (void) sendAllSessionsToServer : (void (^)(BOOL success))completionBlock;
- (void) sendAllSessionsSheetToServer : (void (^)(BOOL success))completionBlock;
- (void) sendNewSessionToServer : (Session *) aSession : (void (^)(BOOL success))completionBlock;

@end
