//
//  Utilities.h
//  e-souvenir
//
//  Created by Dynseo on 07/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StimartLib/STMUtilities.h>

@interface Utilities : NSObject
+ (NSString *) convertSecondeToString : (NSString *) time;
+ (NSString *) convertMillisecondeToString : (NSString *) time;
+ (NSString *) modifyFormatDate : (NSString *) inputFormat : (NSString *) outputFormat : (NSString *) aStringDate;
@end
