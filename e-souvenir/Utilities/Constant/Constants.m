//
//  Constant.m
//  e-souvenir
//
//  Created by Dynseo on 29/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "Constants.h"

@implementation Constant

NSString * const kTypeSession = @"typeSession";
NSString * const kTypeModeSelection = @"modeSelection";
@end
