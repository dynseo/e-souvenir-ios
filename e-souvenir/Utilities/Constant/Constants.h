//
//  Constant.h
//  e-souvenir
//
//  Created by Dynseo on 29/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject

extern NSString * const kTypeSession;
extern NSString * const kTypeModeSelection;
@end
