//
//  SouvenirInAppPurchaseHelper.h
//  e-souvenir
//
//  Created by Dynseo on 16/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StimartLib/StimartLib.h>

#define nbProductsInApp 1

@interface SouvenirInAppPurchaseHelper : InAppPurchaseHelper {
    NSArray *productsArray;
    NSMutableArray *pricesArray;
    BOOL isPurchasedArray[nbProductsInApp];
    MBProgressHUD *hud;
    UIView *_view;
}

@property (retain, nonatomic) NSArray *productsArray;
@property (retain, nonatomic) NSMutableArray *pricesArray;
@property (retain, nonatomic) UIView *_view;

+ (SouvenirInAppPurchaseHelper *) sharedHelper;
- (BOOL)isAllPurchased;
- (void)setRealProductPrices;

- (NSString *)getProductIdentifierByIndex:(NSInteger)index;
- (NSString *)getProductPriceByIndex:(NSInteger)index;
- (BOOL)getProductStatusByIndex:(NSInteger)index;
//- (void) setProductPurchased:(NSInteger)index;
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue;

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error;
- (void)restoreTransactions:(UIView *) view;
+ (NSData *)getInAppPurchaseReceipt;

@end
