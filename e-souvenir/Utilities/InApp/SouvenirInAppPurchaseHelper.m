//
//  SouvenirInAppPurchaseHelper.m
//  e-souvenir
//
//  Created by Dynseo on 16/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "SouvenirInAppPurchaseHelper.h"
#import <StimartLib/StimartLib.h>

@implementation SouvenirInAppPurchaseHelper {
    NSString * identifierIndividual;
    NSString * identifierInstitution;
    NSString * identifierProfessional;
}

@synthesize productsArray,pricesArray;
@synthesize hud = _hud;
@synthesize _view;

static SouvenirInAppPurchaseHelper * _sharedHelper;

+ (SouvenirInAppPurchaseHelper *) sharedHelper {
    
    if (_sharedHelper != nil) {
        return _sharedHelper;
    }
    _sharedHelper = [[SouvenirInAppPurchaseHelper alloc] init];
    return _sharedHelper;
}

- (id)init {
    
    if ((self = [super init])) {
        identifierIndividual = @"esouvenirs_home";
        identifierInstitution = @"esouvenirs_institution";
        identifierProfessional = @"esouvenirs_professional";
        
        self.productsArray = [[NSArray alloc ]initWithObjects:identifierIndividual, identifierInstitution, identifierProfessional, nil];
        self.pricesArray   = [[NSMutableArray alloc] initWithObjects:@"49,99 €", @"149,99 €", @"149,99 €", nil];
        
        for(int index=0; index<nbProductsInApp; index++){
            isPurchasedArray[index]=NO;
        }
        NSSet *productIdentifiers = [NSSet setWithArray:productsArray];
        [super initProductIdentifiers:productIdentifiers];
    }
    
    return self;
}

- (void)setRealProductPrices {
    
    int i=0;
    for( NSString *product in productsArray) {
        
        for (SKProduct * skProduct in self.products) {
            if( [skProduct.productIdentifier isEqualToString:product] ) {
                
                [self.pricesArray replaceObjectAtIndex:i withObject:skProduct.priceAsString];
                break;
            }
        }
        i++;
    }
    return;
}


- (BOOL)isAllPurchased {
    
    BOOL everythingBought = YES;
    int index = 0;
    for( NSString *product in productsArray){
        BOOL purchase = [_sharedHelper isPurchased:product];
        if( !purchase )
            everythingBought = NO;
        
        //find the index
        isPurchasedArray[index]=purchase;
        index++;
    }
    return everythingBought;
}


- (NSString *) getProductIdentifierByIndex:(NSInteger) index{
    return [productsArray objectAtIndex:index];
}

- (NSString *) getProductPriceByIndex:(NSInteger) index{
    return [pricesArray objectAtIndex:index];
}

-(BOOL) getProductStatusByIndex:(NSInteger) index{

    if( isPurchasedArray[index] )
        return YES;
    //Product marked as not bought, check if its status has changed
    NSString *identifier = [[SouvenirInAppPurchaseHelper sharedHelper] getProductIdentifierByIndex: index];
    if( [[SouvenirInAppPurchaseHelper sharedHelper] isPurchased:identifier] ) {
        isPurchasedArray[index] = YES;
        return YES;
    }
    return NO;
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    [super paymentQueueRestoreCompletedTransactionsFinished:queue];
    [MBProgressHUD hideHUDForView:_view animated:YES];
    NSString * alertString = NSLocalizedString(@"restore_purchase_success", nil);
    if (queue.transactions.count == 0) {
        alertString = NSLocalizedString(@"restore_purchase_failure", nil);
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:alertString
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK", nil];
    [alert show];
}

-(void) restoreTransactions:(UIView *) view{
    NSLog(@"SouvenirInApp-restoreTransactions");
    _view=view;
    
    [[SKPaymentQueue defaultQueue] addTransactionObserver:[SouvenirInAppPurchaseHelper sharedHelper]];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    
    self.hud = [MBProgressHUD showHUDAddedTo:_view animated:YES];
    _hud.labelText = NSLocalizedString(@"Accès à l'Apple Store en cours", @"");
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error{
    [MBProgressHUD hideHUDForView:_view animated:YES];
}

+ (NSData *)getInAppPurchaseReceipt {
    NSLog(@"getInAppPurchaseReceipt");
    return [NSData dataWithContentsOfURL:[[NSBundle mainBundle]appStoreReceiptURL]];
}

- (void)provideContent:(NSString *)productIdentifier {
    [Subscription setSubscriptionType:[self getSubscriptionTypeWithProductIdentifier:productIdentifier]];
    [super provideContent:productIdentifier];
}

- (int)getSubscriptionTypeWithProductIdentifier:(NSString *)identifierProduct {
    /* an individual subscription */
    if ([identifierProduct isEqualToString:identifierIndividual]) {
        NSLog(@"product - home");
        return SubscriptionTypeIndividual;
    }
    /* an institution subscription */
    else if ([identifierProduct isEqualToString:identifierInstitution]) {
        NSLog(@"product - institution");
        return SubscriptionTypeInstitution;
    }
    else if ([identifierProduct isEqualToString:identifierProfessional]) {
        NSLog(@"product - professional");
        return SubscriptionTypeProfessional;
    }
    else {
        NSLog(@"product - unknown");
        return -1;
    }
}

@end

