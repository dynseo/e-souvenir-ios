//
//  DatabaseManager.m
//  e-souvenir
//
//  Created by Dynseo on 25/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import "AppDelegate.h"
#import "DatabaseManager.h"

#define kTAG @"DatabaseManager"

// tables and columns in Stimart
/* tables */
#define kTablePerson @"person"
#define kTableResult @"result"
#define kTableInvitation @"invitation"
#define kTableMessage @"message"
#define kTableUser @"user"
#define kTableLink @"link"
#define kTableGamePersonInfo @"gamepersoninfo"

/* columns */
#define kColumnId @"id"
#define kColumnServerId @"serverId"

/* columns table person */
#define kColumnRole @"role"
#define kColumnName @"name"
#define kColumnFirstName @"firstname"
#define kColumnBirthdate @"birthdate"
#define kColumnSex @"sex"
#define kColumnConflictId @"conflictId"
#define kColumnModificationDate @"modificationDate"
#define kColumnIsActive @"active"

/* columns table result */
#define kColumnPersonId @"personId"
//char * const kColumnServerId_PERSON = "serverIdPerson";
#define kColumnDuration @"duration"
#define kColumnLevel @"level"
#define kColumnScore1 @"score1"
#define kColumnScore2 @"score2"
#define kColumnScore3 @"score3"
#define kColumnScore4 @"score4"
#define kColumnScore5 @"score5"
#define kColumnScore6 @"score6"
#define kColumnInterrupted @"interrupted"
#define kColumnGame @"game"

/* columns table invitation */
#define kColumnState @"state"
#define kColumnType @"type"
#define kColumnNeedSend @"needSend"

/* columns table message */
#define kColumnMessageUserId @"userId"
#define kColumnMessageObject @"objetMessage"
#define kColumnMessage @"message"
#define kColumnMessagePersonId @"personId"
#define kColumnMessageFromUserToPerson @"fromUser2Person"
#define kColumnMessageChecked @"checked"
#define kColumnMessageImageServer @"urlImageServer"
#define kColumnDate @"date"

#define kColumnMessageLinkPersonId @"personId"
#define kColumnMessageAliasPerson @"aliasPerson"

/* colum table GamePersonInfo*/
#define kColumnInfo1 @"info1"
#define kColumnInfo2 @"info2"

// tables and columns in e-Souvenir

#define kTableSheet @"sheet"
#define kTableSession @"session"
#define kTableSessionSheet @"sessionSheet"

// TABLE SHEET
#define kColumnIdSheet @"id"
#define kColumnServerIdSheet @"serverId"
#define kColumnIdPerson @"personId"
#define kColumnNameSheet @"name"
#define kColumnCategory @"category"
#define kColumnDate @"date"
#define kColumnQuestion1 @"question1"
#define kColumnQuestion2 @"question2"
#define kColumnQuestion3 @"question3"
#define kColumnHint1 @"hint1"
#define kColumnHint2 @"hint2"
#define kColumnHint3 @"hint3"
#define kColumnResourcesName @"resourceName"
#define kColumnResourcesName_TH @"resourceNameTh"
#define kColumnResourcesType @"resourceType"
#define kColumnAction @"action"
#define kColumnType @"type"
#define kColumnDescription @"description"

// TABLE SESSION
#define kColumnIdSession @"id"
#define kColumnServerIdSession @"serverId"
#define kColumnIdPersonSession @"personId"
#define kColumnDateSession @"date"
#define kColumnTimerSession @"timer"
#define kColumnSatisfaction @"satisfaction"
#define kColumnTypeSession @"type"

// TABLE SESSION SHEET
#define kColumnIdSessionSheet @"id"
#define kColumnServerIdSessionSheet @"serverId"
#define kColumnTimerSessionSheet @"timer"
#define kColumnIdOfSheet @"sheetId"
#define kColumnIdOfSession @"sessionId"
#define kColumnLike @"like"
#define kColumnLegend @"legend"


char * const kTableSheet_CREATE = "CREATE TABLE sheet (id INTEGER PRIMARY KEY NOT NULL, serverId INTEGER, personId INTEGER, name TEXT NOT NULL, category TEXT NOT NULL, date, question1 TEXT NOT NULL, question2 TEXT NOT NULL, question3 TEXT NOT NULL, hint1 TEXT NOT NULL, hint2 TEXT NOT NULL, hint3 TEXT NOT NULL, resourceName TEXT NOT NULL,resourceNameTh TEXT NOT NULL, resourceType TEXT NOT NULL, action TEXT, type TEXT, description TEXT)";

char * const kTableSession_CREATE = "CREATE TABLE session (id INTEGER PRIMARY KEY AUTOINCREMENT, serverId INTEGER, personId INTEGER, date, timer TEXT NOT NULL, satisfaction TEXT NOT NULL, type TEXT)";

char * const kTableSessionSheet_CREATE = "CREATE TABLE sessionSheet (id INTEGER PRIMARY KEY AUTOINCREMENT, serverId INTEGER, sessionId INTEGER, sheetId INTEGER, timer TEXT, like TEXT, legend TEXT)";

@implementation DatabaseManager

static DatabaseManager *sharedInstance = nil;

+ (DatabaseManager *)getSharedInstance
{
    NSLog(@"%@ - getSharedInstance",kTAG);
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:nil]init];
        [sharedInstance createDatabase];
    }
    return sharedInstance;
}


- (BOOL) createDatabase {
    [super createDatabase];
    BOOL isSuccess = YES;
    
    if (![self checkATableExist:@"sheet"]) {
        if ([self openDatabase]) {
            char * errorMessage;
            if (sqlite3_exec(database, kTableSheet_CREATE, nil, nil, &errorMessage)
            != SQLITE_OK) {
                isSuccess = NO;
                NSLog(@"%@ ERROR - failed to create table sheet - %s",kTAG,kTableSheet_CREATE);
            }
            if (sqlite3_exec(database, kTableSession_CREATE, nil, nil, &errorMessage)
            != SQLITE_OK) {
                isSuccess = NO;
                NSLog(@"%@ ERROR - failed to create table session - %s",kTAG,kTableSession_CREATE);
            }
            if (sqlite3_exec(database, kTableSessionSheet_CREATE, nil, nil, &errorMessage)
            != SQLITE_OK) {
                isSuccess = NO;
                NSLog(@"%@ ERROR - failed to create table sessionsheet - %s",kTAG,kTableSessionSheet_CREATE);
            }
            [self closeDatabase];
        }
    }
    return isSuccess;
}

- (NSString *) convertString : (NSString * ) string {
    return [string stringByReplacingOccurrencesOfString:@"'" withString:@"''"];
}

- (BOOL) checkATableExist : (NSString *) tableName {
    static sqlite3_stmt *statementChk;
    NSString * querySQL = [NSString stringWithFormat:@"SELECT name FROM sqlite_master WHERE type='table' AND name='%@';", tableName];
    const char *query_stmt = [querySQL UTF8String];
    sqlite3_prepare_v2(database, query_stmt, -1, &statementChk, nil);
    BOOL exist = FALSE;
    if (sqlite3_step(statementChk) == SQLITE_ROW) {
        exist = TRUE;
    }
    sqlite3_finalize(statementChk);
    return exist;
}

- (BOOL) insertSheet: (Sheet *) aSheet {
    NSLog(@"%@ - insertSheet", kTAG);
    BOOL isSuccess = NO;
    
    if ([self openDatabase]) {
        char* errorMessage;
        sqlite3_exec(database, "BEGIN TRANSACTION", NULL, NULL, &errorMessage);
        sqlite3_stmt *compiledStatement = nil;
        NSString * sqlStatementString;
        
        if (compiledStatement == nil) {
            sqlStatementString = [NSString stringWithFormat:@"insert into %@ (%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", kTableSheet,kColumnServerIdSheet, kColumnIdPerson, kColumnNameSheet, kColumnCategory, kColumnDate, kColumnQuestion1, kColumnQuestion2, kColumnQuestion3, kColumnHint1, kColumnHint2, kColumnHint3, kColumnResourcesName, kColumnResourcesName_TH, kColumnResourcesType, kColumnAction, kColumnType, kColumnDescription];
            const char *sqlStatement = [sqlStatementString UTF8String];
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(database));
        }
        sqlite3_bind_int(compiledStatement, 1, aSheet.serverId);
        sqlite3_bind_int(compiledStatement, 2, aSheet.idPerson);
        sqlite3_bind_text(compiledStatement,3, [aSheet.name UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,4, [aSheet.category UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,5, [aSheet.date UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,6, [[aSheet.questions objectAtIndex:0] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,7, [[aSheet.questions objectAtIndex:1] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,8, [[aSheet.questions objectAtIndex:2] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,9, [[aSheet.hints objectAtIndex:0] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,10, [[aSheet.hints objectAtIndex:1] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,11, [[aSheet.hints objectAtIndex:2] UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,12, [aSheet.resourceName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,13, [aSheet.resourceNameTh UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,14, [aSheet.resourceType UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,15, [aSheet.action UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,16, [aSheet.type UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,17, [aSheet.description UTF8String], -1, SQLITE_TRANSIENT);
        
        if (!(sqlite3_step(compiledStatement) == SQLITE_DONE)) {
                NSLog(@"challenge insert error");
                NSString * sqlStatementString1 = [NSString stringWithFormat:@"update %@ SET %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", kTableSheet,kColumnServerIdSheet, kColumnIdPerson, kColumnNameSheet, kColumnCategory, kColumnDate, kColumnQuestion1, kColumnQuestion2, kColumnQuestion3, kColumnHint1, kColumnHint2, kColumnHint3, kColumnResourcesName, kColumnResourcesName_TH, kColumnResourcesType, kColumnAction, kColumnType, kColumnDescription, kColumnServerIdSheet];
                const char *sqlStatement1 = [sqlStatementString1 UTF8String];
                sqlite3_stmt *compiledStatement1;
            
                if(sqlite3_prepare_v2(database, sqlStatement1, -1, &compiledStatement1, NULL) == SQLITE_OK) {
                    NSLog(@"update challenge %@ : ", sqlStatementString1);
                    sqlite3_bind_int(compiledStatement1,1, aSheet.serverId);
                    sqlite3_bind_int(compiledStatement1,2, aSheet.idPerson);
                    sqlite3_bind_text(compiledStatement1,3, [aSheet.name UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement1,4, [aSheet.category UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement1,5, [aSheet.date UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement1,6, [[aSheet.questions objectAtIndex:0] UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement1,7, [[aSheet.questions objectAtIndex:1] UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement1,8, [[aSheet.questions objectAtIndex:2] UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement1,9, [[aSheet.hints objectAtIndex:0] UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement1,10 , [[aSheet.hints objectAtIndex:1] UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement1,11, [[aSheet.hints objectAtIndex:2] UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement,12, [aSheet.resourceName UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement,13, [aSheet.resourceNameTh UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement,14, [aSheet.resourceType UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement,15, [aSheet.action UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement,16, [aSheet.type UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(compiledStatement,17, [aSheet.description UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_int(compiledStatement1,18, aSheet.serverId);
                    sqlite3_step(compiledStatement1);
                    sqlite3_reset(compiledStatement1);
                    sqlite3_finalize(compiledStatement1);
                }
            }
        else {
            isSuccess = YES;
            sqlite3_reset(compiledStatement);
        }
        sqlite3_exec(database, "COMMIT TRANSACTION", NULL, NULL, &errorMessage);
        sqlite3_finalize(compiledStatement);
    }
    [self closeDatabase];
    return isSuccess;
}

- (BOOL) insertSession : (Session *) aSession {
    NSLog(@"%@ - insertSession", kTAG);
    BOOL isSuccess = NO;
    
    if ([self openDatabase]) {
        char* errorMessage;
        sqlite3_stmt *compiledStatement = nil;
        NSString * sqlStatementString;
        
        NSDate * date = [[NSDate alloc] init];
        NSString * aDate = [[STMDateManager getSharedInstance] dateToStringFormatDatabase:date];
        
        if (compiledStatement == nil) {
            sqlStatementString = [NSString stringWithFormat:@"insert into %@ (%@, %@, %@, %@, %@) VALUES (?, ?, ?, ?, ?)", kTableSession,kColumnIdPersonSession, kColumnDateSession, kColumnTimerSession, kColumnSatisfaction, kColumnTypeSession];
            const char *sqlStatement = [sqlStatementString UTF8String];
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(database));
        }
        sqlite3_bind_int(compiledStatement, 1, aSession.sessionPersonId);
        sqlite3_bind_text(compiledStatement,2, [aDate UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,3, [aSession.sessionTimer UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,4, [aSession.satisfaction UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,5, [aSession.type UTF8String], -1, SQLITE_TRANSIENT);
        
        if ((sqlite3_step(compiledStatement) == SQLITE_DONE)) {
            isSuccess = YES;
        }
        sqlite3_reset(compiledStatement);
        sqlite3_finalize(compiledStatement);
    }
    [self closeDatabase];
    return isSuccess;
}

- (BOOL) insertSessionSheet : (SessionSheet *) aSessionSheet {
    NSLog(@"%@ - insertSessionSheet", kTAG);
    BOOL isSuccess = NO;
    
    if ([self openDatabase]) {
        char* errorMessage;
        sqlite3_stmt *compiledStatement = nil;
        NSString * sqlStatementString;
        
        if (compiledStatement == nil) {
            sqlStatementString = [NSString stringWithFormat:@"insert into %@ (%@, %@, %@, %@, %@) VALUES (?, ?, ?, ?, ?)", kTableSessionSheet,kColumnIdOfSession, kColumnIdOfSheet, kColumnTimerSessionSheet, kColumnLike, kColumnLegend];
            const char *sqlStatement = [sqlStatementString UTF8String];
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(database));
        }
        sqlite3_bind_int(compiledStatement, 1, aSessionSheet.sessionId);
        sqlite3_bind_int(compiledStatement, 2, aSessionSheet.sheetId);
        sqlite3_bind_text(compiledStatement,3, [aSessionSheet.sessionSheetTimer UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,4, [aSessionSheet.like UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,5, [aSessionSheet.legend UTF8String], -1, SQLITE_TRANSIENT);
        
        if ((sqlite3_step(compiledStatement) == SQLITE_DONE)) {
            isSuccess = YES;
        }
        sqlite3_reset(compiledStatement);
        sqlite3_finalize(compiledStatement);
    }
    [self closeDatabase];
    return isSuccess;
}


- (BOOL) updateSessionSheet : (SessionSheet *) aSessionSheet {
    NSLog(@"%@ - updateSessionSheet",kTAG);
    BOOL isSuccess = NO;
    
    if ([self openDatabase]) {
        char* errorMessage;
        sqlite3_exec(database, "BEGIN TRANSACTION", NULL, NULL, &errorMessage);
        sqlite3_stmt *compiledStatement = nil;
        NSString * sqlStatementString;
        
        if (compiledStatement == nil) {
            sqlStatementString = [NSString stringWithFormat:@"update %@ SET %@ = ?, %@ = ?, %@ = ? WHERE %@ = ?", kTableSessionSheet,kColumnTimerSessionSheet, kColumnLike, kColumnLegend, kColumnIdSessionSheet];
            const char *sqlStatement = [sqlStatementString UTF8String];
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating update statement. '%s'", sqlite3_errmsg(database));
        }
        sqlite3_bind_text(compiledStatement,1, [aSessionSheet.sessionSheetTimer UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,2, [aSessionSheet.like UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(compiledStatement,3, [aSessionSheet.legend UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(compiledStatement, 4, aSessionSheet.sessionSheetId);
        
        if ((sqlite3_step(compiledStatement) == SQLITE_DONE)) {
            isSuccess = YES;
        }
        sqlite3_reset(compiledStatement);
        sqlite3_finalize(compiledStatement);
    }
    [self closeDatabase];
    return isSuccess;
}

// get Name and FirstName Person
- (NSMutableArray *) getAllPersonPseudo {
    NSLog(@"%@ - getAllPersonPseudo",kTAG);
    NSMutableArray * names;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT %@,%@ FROM %@",kColumnName,kColumnFirstName, kTablePerson];
        const char *query_stmt = [querySQL UTF8String];
        NSLog(@"getAllPersonPseudo - sql = %@",querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            names = [[NSMutableArray alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                char * nameChars = (char *)sqlite3_column_text(statement, 0);
                char *firstNameChars = (char *)sqlite3_column_text(statement, 1);
                NSString * name = @"";
                NSString * firstName = @"";
                if (nameChars != nil) {
                    name = [NSString stringWithUTF8String:nameChars];
                }
                if (firstNameChars != nil) {
                    firstName = [NSString stringWithUTF8String:firstNameChars];
                }
                name = [name stringByAppendingString:firstName];
                [names addObject:name];
            }
        }
        else {
            NSLog(@"%@ - getAllPersonPseudo... - not found",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return names;
}

- (Sheet *) getNameSheetBySheetId : (int) aIdSheet {
    NSLog(@"%@ - getNameSheetBySheetId", kTAG);
    Sheet * aSheet = nil;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = %d", kTableSheet, kColumnIdSheet, aIdSheet];
        
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"getNameSheetBySheetId - sql = %@", querySQL);
        sqlite3_stmt * statement = nil;
        
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                aSheet = [self cursorToSheet:statement];
            }
        }
        else {
            NSLog(@"%@ - getAllPersonPseudo... - not found",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return aSheet;
}

- (NSMutableArray *) getListNameSheetsByListSessionSheets : (NSMutableArray *) sessionSheets {
    NSLog(@"%@ - getListNameSheetsByListSessionSheets", kTAG);
    NSMutableArray * listNameSheets = [[NSMutableArray alloc] init];
    if ([self openDatabase]) {
        
        for (SessionSheet * aSessionSheet in sessionSheets) {
            int sheetId = aSessionSheet.sheetId;
            
            NSString * querySQL = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE %@ = %d",kColumnNameSheet, kTableSheet, kColumnIdSheet, sheetId];
            const char * query_stmt = [querySQL UTF8String];
            NSLog(@"getNameSheetBySheetId - sql = %@", querySQL);
            sqlite3_stmt * statement = nil;
            
            if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
                if (sqlite3_step(statement) == SQLITE_ROW) {
                    char * nameChars = (char *)sqlite3_column_text(statement, 0);
                    [listNameSheets addObject:[NSString stringWithUTF8String:nameChars]];
                }
            }
            else {
                NSLog(@"%@ - getListNameSheetsByListSessionSheets... - not found",kTAG);
            }
            sqlite3_finalize(statement);
        }
    }
    [self closeDatabase];
    return listNameSheets;
}

- (Sheet *) cursorToSheet : (sqlite3_stmt *) statement {
    Sheet * aSheet = nil;
    NSMutableArray * questions = [[NSMutableArray alloc] init];
    NSMutableArray * hints = [[NSMutableArray alloc] init];
    
    char * question1Char = (char *)sqlite3_column_text(statement, 6);
    char * question2Char = (char *)sqlite3_column_text(statement, 7);
    char * question3Char = (char *)sqlite3_column_text(statement, 8);
    NSString * question1 = @"";
    NSString * question2 = @"";
    NSString * question3 = @"";
    if (question1Char != nil) {
        question1 = [NSString stringWithUTF8String:question1Char];
    }
    [questions addObject:question1];
    if (question2Char != nil) {
        question2 = [NSString stringWithUTF8String:question2Char];
    }
    [questions addObject:question2];
    if (question3Char != nil) {
        question3 = [NSString stringWithUTF8String:question3Char];
    }
    [questions addObject:question3];
    
    char * hint1Char = (char *)sqlite3_column_text(statement, 9);
    char * hint2Char = (char *)sqlite3_column_text(statement, 10);
    char * hint3Char = (char *)sqlite3_column_text(statement, 11);
    NSString * hint1 = @"";
    NSString * hint2 = @"";
    NSString * hint3 = @"";
    
    if (hint1Char != nil) {
        hint1 = [NSString stringWithUTF8String:hint1Char];
    }
    [hints addObject:hint1];
    if (hint2Char != nil) {
        hint2 = [NSString stringWithUTF8String:hint2Char];
    }
    [hints addObject:hint2];
    if (hint3Char != nil) {
        hint3 = [NSString stringWithUTF8String:hint3Char];
    }
    [hints addObject:hint3];
    
    char * nameSheetChar = (char *)sqlite3_column_text(statement, 3);
    char * categoryChar = (char *)sqlite3_column_text(statement, 4);
    char * dateChar = (char *)sqlite3_column_text(statement, 5);
    char * resrouceNameChar = (char *)sqlite3_column_text(statement, 12);
    char * resourceNameTHChar = (char *)sqlite3_column_text(statement, 13);
    char * resourceTypeChar = (char *)sqlite3_column_text(statement, 14);
    char * typeChar = (char *)sqlite3_column_text(statement, 16);
    char * descriptionChar = (char *) sqlite3_column_text(statement, 17);
    NSString * nameSheet = @"";
    NSString * category = @"";
    NSString * date = @"";
    NSString * resourceName = @"";
    NSString * resourceNameTH = @"";
    NSString * resourceType = @"";
    NSString * type = @"";
    NSString * description = @"";
    if (nameSheetChar != nil) {
        nameSheet = [NSString stringWithUTF8String:nameSheetChar];
    }
    if (categoryChar != nil) {
        category = [NSString stringWithUTF8String:categoryChar];
    }
    if (dateChar != nil) {
        date = [NSString stringWithUTF8String:dateChar];
    }
    if (resrouceNameChar != nil) {
        resourceName = [NSString stringWithUTF8String:resrouceNameChar];
    }
    if (resourceNameTHChar != nil) {
        resourceNameTH = [NSString stringWithUTF8String:resourceNameTHChar];
    }
    if (resourceTypeChar != nil) {
        resourceType = [NSString stringWithUTF8String:resourceTypeChar];
    }
    if (typeChar != nil) {
        type = [NSString stringWithUTF8String:typeChar];
    }
    if (descriptionChar != nil) {
        description = [NSString stringWithUTF8String:descriptionChar];
    }
    
    int sheetId = sqlite3_column_int(statement, 0);
    int sheetServerId = sqlite3_column_int(statement, 1);
    
    aSheet = [[Sheet alloc] initSheetWithAId:sheetId andServerId:sheetServerId andName:nameSheet andCategory:category andDate:date andQuestions:questions andHints:hints andResourceName:resourceName andResouceNameTh:resourceNameTH andResourceType:resourceType andType:type andDescription:description];
    return aSheet;
}

// get last session Id
- (int) getLastSessionId {
    NSLog(@"%@ - getLastSessionId",kTAG);
    int result = 0;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT %@ FROM %@ ORDER BY %@ DESC  LIMIT 1",kColumnIdSession,kTableSession,kColumnIdSession];
        const char *query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getLastSessionId - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                result = sqlite3_column_int(statement, 0);
            }
        }
        else {
            NSLog(@"%@ - ERROR - getLastSessionId",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return result;
}

// get SHEET by Category
- (NSMutableArray *) getSheetByCategory : (Category *) aCategory {
    NSMutableArray * sheets = nil;
    NSString * condition = nil;
    Person * currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    
    if (aCategory.name == nil) {
        condition = [NSString stringWithFormat:@"%@ >= \'%@\' AND %@ < \'%@\'", kColumnDate, aCategory.start, kColumnDate, aCategory.end];
    }
    else {
        condition = [NSString stringWithFormat:@"%@ = \'%@\'", kColumnCategory, [self convertString:aCategory.name]];
    }
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE (%@ AND (%@ = 'S' or (%@ = 'P' AND %@ = %d))) ORDER BY %@ ASC", kTableSheet, condition, kColumnType, kColumnType, kColumnIdPerson, currentPerson.serverId, kColumnDate];
        const char *query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getSheetByCategory - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
        sheets = [[NSMutableArray alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                Sheet * aSheet = [self cursorToSheet:statement];
                [sheets addObject:aSheet];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getLastSessionId",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return sheets;
}

- (NSMutableArray *) getSheetsByFrieze {
    NSMutableArray * sheets = nil;
    Person * currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = %d AND %@ = 'P' ORDER BY %@ ASC", kTableSheet, kColumnIdPerson, currentPerson.serverId, kColumnType, kColumnDate];
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getSheetsByFrize - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            sheets = [[NSMutableArray alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                Sheet * aSheet = [self cursorToSheet:statement];
                [sheets addObject:aSheet];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getSheetsByFrize",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return sheets;
}

// SESSIONSHEET methodes
- (SessionSheet *) cursorToSessionSheet : (sqlite3_stmt *) statement {
    SessionSheet * aSessionSheet = nil;
    int aIdSessionSheet = sqlite3_column_int(statement, 0);
    int aIdSession = sqlite3_column_int(statement, 2);
    int aIdSheet = sqlite3_column_int(statement, 3);
    char * timerSessionSheetChar = (char *)sqlite3_column_text(statement, 4);
    char * likeChar = (char *)sqlite3_column_text(statement, 5);
    char * legendChar = (char *)sqlite3_column_text(statement, 6);
    NSString * aTimerSessionSheet = @"";
    NSString * aLike = @"";
    NSString * aLegend = @"";
    
    if (timerSessionSheetChar != nil) {
        aTimerSessionSheet = [NSString stringWithUTF8String:timerSessionSheetChar];
    }
    if (likeChar != nil) {
        aLike = [NSString stringWithUTF8String:likeChar];
    }
    if (legendChar != nil) {
        aLegend = [NSString stringWithUTF8String:legendChar];
    }
    aSessionSheet = [[SessionSheet alloc] initSessionSheetWithSessionSheetId:aIdSessionSheet andSessionId:aIdSession andSheetId:aIdSheet andSessionSheetTimer:aTimerSessionSheet andLike:aLike andLegend:aLegend];
    
    return aSessionSheet;
}

- (SessionSheet *) getSessionSheetBySessionIdAndSheetId : (int) aSessionId : (int) aSheetId {
    SessionSheet * aSessionSheet = nil;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = %d AND %@ = %d", kTableSessionSheet, kColumnIdOfSession , aSessionId, kColumnIdOfSheet, aSheetId];
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getSessionSheetBySessionIdAndSheetId - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                aSessionSheet = [self cursorToSessionSheet:statement];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getSessionSheetBySessionIdAndSheetId",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return aSessionSheet;
}

- (NSMutableArray *) getSessionForCurrentPerson {
    NSMutableArray * sessions;
    Person * currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    Session * aSession = nil;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT %@, %@, %@ FROM %@ WHERE (%@ = %d) ORDER BY %@ DESC", kColumnIdSession, kColumnDate, kColumnType, kTableSession, kColumnIdPerson, currentPerson.personId, kColumnIdSession];
        
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getSessionForCurrentPerson - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            sessions = [[NSMutableArray alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                char * dateSessionChar = (char *)sqlite3_column_text(statement, 1);
                char * typeSessionChar = (char *)sqlite3_column_text(statement, 2);
                int aIdSession = sqlite3_column_int(statement, 0);
                NSString * aDateSession = @"";
                NSString * aTypeSession = @"";
                
                if (dateSessionChar != nil) {
                    aDateSession = [NSString stringWithUTF8String:dateSessionChar];
                }
                
                if (typeSessionChar != nil) {
                    aTypeSession = [NSString stringWithUTF8String:typeSessionChar];
                }
                
                aSession = [[Session alloc] initSessionWithAId:aIdSession andDate:aDateSession andType:aTypeSession];
                [sessions addObject:aSession];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getSessionForCurrentPerson",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return sessions;
}

- (NSMutableArray *) getSessionSheetsBySessionId : (int) aSessionId {
    NSMutableArray * sessionSheets = nil;
    SessionSheet * aSessionSheet;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE (%@ = %d)", kTableSessionSheet, kColumnIdOfSession, aSessionId];
        
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getSessionForCurrentPerson - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            sessionSheets = [[NSMutableArray alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                aSessionSheet = [self cursorToSessionSheet:statement];
                [sessionSheets addObject:aSessionSheet];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getSessionForCurrentPerson",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return sessionSheets;
}


// Get Sheets by Taste
- (NSMutableArray *) getSheetsByTaste : (NSString *) aLike {
    NSMutableArray * sheets = nil;
    NSMutableArray * questions = nil;
    NSMutableArray * hints = nil;
    Person * currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    Sheet * aSheet = nil;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT s.%@, s.%@, s.%@, s.%@, s.%@, s.%@, s.%@, s.%@, s.%@, s.%@, s.%@, s.%@, s.%@, s.%@, s.%@, sh.%@, s.%@, GROUP_CONCAT (sh.%@) FROM %@ s, %@ se, %@ sh, %@ p WHERE ((sh.%@ = \'%@\') AND (sh.%@ = se.%@) AND (se.%@ = p.%@) AND (p.%@ = %d) AND (s.%@ = sh.%@)) GROUP BY s.%@ ORDER BY se.%@", kColumnIdSheet, kColumnServerId, kColumnNameSheet, kColumnCategory, kColumnDate, kColumnQuestion1, kColumnQuestion2, kColumnQuestion3, kColumnHint1, kColumnHint2, kColumnHint3, kColumnResourcesName, kColumnResourcesName_TH, kColumnResourcesType, kColumnType, kColumnLike,kColumnDescription, kColumnLegend, kTableSheet, kTableSession, kTableSessionSheet, kTablePerson, kColumnLike, aLike, kColumnIdOfSession, kColumnIdSession, kColumnIdPersonSession, kColumnId, kColumnId, currentPerson.personId, kColumnIdSheet, kColumnIdOfSheet, kColumnIdSheet, kColumnDateSession];
        
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getSheetsByTaste - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            sheets = [[NSMutableArray alloc] init];
            questions = [[NSMutableArray alloc] init];
            hints = [[NSMutableArray alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                char * question1Char = (char *)sqlite3_column_text(statement, 5);
                char * question2Char = (char *)sqlite3_column_text(statement, 6);
                char * question3Char = (char *)sqlite3_column_text(statement, 7);
                NSString * question1 = @"";
                NSString * question2 = @"";
                NSString * question3 = @"";
                if (question1Char != nil) {
                    question1 = [NSString stringWithUTF8String:question1Char];
                }
                [questions addObject:question1];
                if (question2Char != nil) {
                    question2 = [NSString stringWithUTF8String:question2Char];
                }
                [questions addObject:question2];
                if (question3Char != nil) {
                    question3 = [NSString stringWithUTF8String:question3Char];
                }
                [questions addObject:question3];
                
                char * hint1Char = (char *)sqlite3_column_text(statement, 8);
                char * hint2Char = (char *)sqlite3_column_text(statement, 9);
                char * hint3Char = (char *)sqlite3_column_text(statement, 10);
                NSString * hint1 = @"";
                NSString * hint2 = @"";
                NSString * hint3 = @"";
                
                if (hint1Char != nil) {
                    hint1 = [NSString stringWithUTF8String:hint1Char];
                }
                [hints addObject:hint1];
                if (hint2Char != nil) {
                    hint2 = [NSString stringWithUTF8String:hint2Char];
                }
                [hints addObject:hint2];
                if (hint3Char != nil) {
                    hint3 = [NSString stringWithUTF8String:hint3Char];
                }
                [hints addObject:hint3];
                
                char * nameSheetChar = (char *)sqlite3_column_text(statement, 2);
                char * categoryChar = (char *)sqlite3_column_text(statement, 3);
                char * dateChar = (char *)sqlite3_column_text(statement, 4);
                char * resrouceNameChar = (char *)sqlite3_column_text(statement, 11);
                char * resourceNameTHChar = (char *)sqlite3_column_text(statement, 12);
                char * resourceTypeChar = (char *)sqlite3_column_text(statement, 13);
                char * typeChar = (char *)sqlite3_column_text(statement, 14);
                char * likeChar = (char *)sqlite3_column_text(statement, 15);
                char * descriptionChar = (char *) sqlite3_column_text(statement, 16);
                NSString * nameSheet = @"";
                NSString * category = @"";
                NSString * date = @"";
                NSString * resourceName = @"";
                NSString * resourceNameTH = @"";
                NSString * resourceType = @"";
                NSString * type = @"";
                NSString * like = @"";
                NSString * description = @"";
                if (nameSheetChar != nil) {
                    nameSheet = [NSString stringWithUTF8String:nameSheetChar];
                }
                if (categoryChar != nil) {
                    category = [NSString stringWithUTF8String:categoryChar];
                }
                if (dateChar != nil) {
                    date = [NSString stringWithUTF8String:dateChar];
                }
                if (resrouceNameChar != nil) {
                    resourceName = [NSString stringWithUTF8String:resrouceNameChar];
                }
                if (resourceNameTHChar != nil) {
                    resourceNameTH = [NSString stringWithUTF8String:resourceNameTHChar];
                }
                if (resourceTypeChar != nil) {
                    resourceType = [NSString stringWithUTF8String:resourceTypeChar];
                }
                if (typeChar != nil) {
                    type = [NSString stringWithUTF8String:typeChar];
                }
                if (likeChar != nil) {
                    like = [NSString stringWithUTF8String:likeChar];
                }
                if (descriptionChar != nil) {
                    description = [NSString stringWithUTF8String:descriptionChar];
                }
                int sheetId = sqlite3_column_int(statement, 0);
                int sheetServerId = sqlite3_column_int(statement, 1);
                
                aSheet = [[Sheet alloc] initSheetWithAId:sheetId andServerId:sheetServerId andName:nameSheet andCategory:category andDate:date andQuestions:questions andHints:hints andResourceName:resourceName andResouceNameTh:resourceNameTH andResourceType:resourceType andType:type andDescription:description
                                                 andLike:like];
                
                [sheets addObject:aSheet];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getSheetsByTaste",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return sheets;
}

- (NSString *) getLastSessionSheetLegend : (int) aSheetId {
    NSString * aLegend = nil;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE %@ = (SELECT MAX (%@) FROM %@ WHERE %@ = %d AND %@ != \'\')", kColumnLegend, kTableSessionSheet, kColumnIdSessionSheet, kColumnIdSessionSheet, kTableSessionSheet, kColumnIdOfSheet, aSheetId, kColumnLegend];
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getLastSessionSheetLegend - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                char * legendChar = (char *)sqlite3_column_text(statement, 0);
                if (legendChar != nil) {
                    aLegend = [NSString stringWithUTF8String:legendChar];
                }
            }
        }
        else {
            NSLog(@"%@ - ERROR - getLastSessionSheetLegend",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return aLegend;
}

// Get top liked and seen sheets from sessionSheet
- (NSMutableDictionary *) getTopLikedSheets {
    Person * currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    NSMutableDictionary * map = nil;
    if ([self openDatabase]) {
//         [NSString stringWithFormat:@"SELECT %@ , COUNT(*) AS nbLikes FROM (SELECT %@.*, %@, %@.%@ FROM  %@, %@, %@, %@ WHERE %@.%@ = %@.%@ AND %@.%@ = %@.%@ AND %@.%@ = %@.%@ AND %@.%@ = %d AND %@.%@ = \'true\') GROUP BY %@ ORDER BY nbLikes DESC LIMIT 3", kColumnIdOfSheet, kTableSessionSheet, kColumnCategory, kTableSession, kColumnIdPersonSession, kTableSessionSheet, kTableSheet, kTableSession, kTablePerson, kTableSheet, kColumnIdSheet, kTableSessionSheet, kColumnIdOfSheet, kTableSessionSheet, kColumnIdOfSession, kTableSession, kColumnIdSession, kTableSession, kColumnIdPersonSession, kTablePerson, kColumnId, kTablePerson, kColumnId, currentPerson.personId, kTableSessionSheet, kColumnLike, kColumnIdOfSheet];
        
        
       NSString * querySQL = [NSString stringWithFormat:@"SELECT %@, COUNT(*) AS nbLikes FROM (SELECT %@.*, %@.%@ FROM %@ LEFT JOIN %@ ON (%@.%@ = %@.%@) LEFT JOIN %@ ON (%@.%@ = %@.%@) LEFT JOIN %@ ON (%@.%@ = %@.%@) WHERE %@.%@ = %d AND %@.%@ = \'true\') GROUP BY %@ ORDER BY nbLikes DESC LIMIT 3", kColumnIdOfSheet, kTableSessionSheet, kTableSession, kColumnIdPersonSession, kTableSessionSheet, kTableSheet, kTableSheet, kColumnIdSheet, kTableSessionSheet, kColumnIdOfSheet, kTableSession, kTableSession, kColumnIdSession, kTableSessionSheet, kColumnIdOfSession, kTablePerson, kTablePerson, kColumnId, kTableSession, kColumnIdPersonSession, kTableSession, kColumnId, currentPerson.personId, kTableSessionSheet, kColumnLike, kColumnIdOfSheet];
        
        
        
        
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getTopLikedSheets - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            map = [[NSMutableDictionary alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                int aIdOfSheet = sqlite3_column_int(statement, 0);
                int count = sqlite3_column_int(statement, 1);
                [map setValue:[NSNumber numberWithInt:count] forKey:[NSString stringWithFormat:@"%d", aIdOfSheet]];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getTopLikedSheets",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return map;
}

- (NSMutableDictionary *) getTopViewedSheets {
    
    Person * currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    NSMutableDictionary * map = nil;
    
    if ([self openDatabase]) {
//        NSString * querySQL = [NSString stringWithFormat:@"SELECT %@, COUNT(*) AS nbViews FROM (SELECT %@.*, %@.%@ FROM %@, %@, %@, %@ WHERE %@.%@ = %@.%@ AND %@.%@ = %@.%@ AND %@.%@ = %@.%@ AND %@.%@ = %d) GROUP BY %@ ORDER BY nbViews DESC LIMIT 3", kColumnIdOfSheet, kTableSessionSheet, kTableSession, kColumnIdPersonSession, kTableSessionSheet, kTableSheet, kTableSession, kTablePerson, kTableSheet, kColumnIdSheet, kTableSessionSheet, kColumnIdOfSheet, kTableSessionSheet, kColumnIdOfSession, kTableSession, kColumnIdSession, kTableSession, kColumnIdPersonSession, kTablePerson, kColumnId, kTablePerson, kColumnId, currentPerson.personId, kColumnIdOfSheet];
        
        NSString * querySQL = [NSString stringWithFormat:@"SELECT %@, COUNT(*) AS nbViews FROM (SELECT %@.*, %@.%@ FROM %@ LEFT JOIN %@ ON (%@.%@ = %@.%@) LEFT JOIN %@ ON (%@.%@ = %@.%@) LEFT JOIN %@ ON (%@.%@ = %@.%@) WHERE %@.%@ = %d) GROUP BY %@ ORDER BY nbViews DESC LIMIT 3", kColumnIdOfSheet, kTableSessionSheet, kTableSession, kColumnIdPersonSession, kTableSessionSheet, kTableSheet, kTableSheet, kColumnIdSheet, kTableSessionSheet, kColumnIdOfSheet, kTableSession, kTableSession, kColumnIdSession, kTableSessionSheet, kColumnIdOfSession, kTablePerson, kTablePerson, kColumnId, kTableSession, kColumnIdPersonSession, kTablePerson, kColumnId, currentPerson.personId, kColumnIdOfSheet];
        
        
        
        
        
        
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getTopSeenSheets - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            map = [[NSMutableDictionary alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                int aIdOfSheet = sqlite3_column_int(statement, 0);
                int count = sqlite3_column_int(statement, 1);
                [map setValue:[NSNumber numberWithInt:count] forKey:[NSString stringWithFormat:@"%d", aIdOfSheet]];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getTopSeenSheets",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return map;
}

// Get category Like Percentages
- (NSMutableDictionary *) getCategoryLikePercentages {
    Person * currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    NSMutableDictionary * map = nil;
    int total = 0;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT %@ , COUNT(*) AS nbViews FROM (SELECT %@.*, %@, %@.%@ FROM %@, %@, %@, %@ WHERE %@.%@ = %@.%@ AND %@.%@ = %@.%@ AND %@.%@ = %@.%@ AND %@.%@ = %d AND %@.%@ = \'true\') GROUP BY %@", kColumnCategory, kTableSessionSheet, kColumnCategory, kTableSession, kColumnIdPersonSession, kTableSessionSheet, kTableSheet, kTableSession, kTablePerson, kTableSheet, kColumnIdSheet, kTableSessionSheet, kColumnIdOfSheet, kTableSessionSheet, kColumnIdOfSession, kTableSession, kColumnIdSession, kTableSession, kColumnIdPersonSession, kTablePerson, kColumnId, kTablePerson, kColumnId, currentPerson.personId, kTableSessionSheet, kColumnLike, kColumnCategory];
        
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getCategoryLikePercentages - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                int count = sqlite3_column_int(statement, 1);
                total = total + count;
            }
        }
        else {
            NSLog(@"%@ - ERROR - getCategoryLikePercentages",kTAG);
        }
        
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            map = [[NSMutableDictionary alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                float percentage = (float) sqlite3_column_int(statement, 1) * 100.0f / total;
                char * categoryChar = (char *)sqlite3_column_text(statement, 0);
                NSString * aCategory = @"";
                if (categoryChar != nil) {
                    aCategory = [NSString stringWithUTF8String:categoryChar];
                }
                [map setValue:[NSNumber numberWithFloat:percentage] forKey:aCategory];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getCategoryLikePercentages",kTAG);
        }
    }
    [self closeDatabase];
    return map;
}

- (NSMutableDictionary *) getCategoryViewsPercentages {
    Person * currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    NSMutableDictionary * map;
    int total = 0;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT %@ , COUNT(*) AS nbViews FROM (SELECT %@.*, %@, %@.%@ FROM %@, %@, %@, %@ WHERE %@.%@ = %@.%@ AND %@.%@ = %@.%@ AND %@.%@ = %@.%@ AND %@.%@ = %d) GROUP BY %@", kColumnCategory, kTableSessionSheet, kColumnCategory, kTableSession, kColumnIdPersonSession, kTableSessionSheet, kTableSheet, kTableSession, kTablePerson, kTableSheet, kColumnIdSheet, kTableSessionSheet, kColumnIdOfSheet, kTableSessionSheet, kColumnIdOfSession, kTableSession, kColumnIdSession, kTableSession, kColumnIdPersonSession, kTablePerson, kColumnId, kTablePerson, kColumnId, currentPerson.personId, kColumnCategory];
        
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getCategoryViewsPercentages - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                int count = sqlite3_column_int(statement, 1);
                total = total + count;
            }
        }
        else {
            NSLog(@"%@ - ERROR - getCategoryViewsPercentages",kTAG);
        }
        
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            map = [[NSMutableDictionary alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                float percentage = (float) sqlite3_column_int(statement, 1) * 100.0f / total;
                char * categoryChar = (char *)sqlite3_column_text(statement, 0);
                NSString * aCategory = @"";
                if (categoryChar != nil) {
                    aCategory = [NSString stringWithUTF8String:categoryChar];
                }
                [map setValue:[NSNumber numberWithFloat:percentage] forKey:aCategory];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getCategoryViewsPercentages",kTAG);
        }
    }
    [self closeDatabase];
    return map;
}

// Get Decade Percentages
- (NSMutableDictionary *) getDecadeLikePercentages {
    Person * currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    NSMutableDictionary * map;
    int total = 0;
    if ([self openDatabase]) {
//        NSString * querySQL = [NSString stringWithFormat:@"SELECT substr (%@, 1, 3) AS decade, COUNT(*) AS nbViews FROM (SELECT %@.*, %@.%@ FROM %@, %@, %@, %@ WHERE %@.%@ = %@.%@ AND %@.%@ = %@.%@ AND %@.%@ = %@.%@ AND %@.%@ = %d AND %@.%@ = \'true\' AND %@.%@ != \'\') GROUP BY decade", kColumnDate, kTableSessionSheet, kTableSheet, kColumnDate, kTableSessionSheet, kTableSheet, kTableSession, kTablePerson, kTableSheet, kColumnIdSheet, kTableSessionSheet, kColumnIdOfSheet, kTableSessionSheet, kColumnIdOfSession, kTableSession, kColumnIdSession, kTableSession, kColumnIdPersonSession, kTablePerson, kColumnId, kTablePerson, kColumnId, currentPerson.personId, kTableSessionSheet, kColumnLike, kTableSheet, kColumnDate];
        
        
    NSString * querySQL = [NSString stringWithFormat:@"SELECT substr(%@, 1 ,3) AS decade, COUNT(*) as nbViews FROM (SELECT %@.*, %@.%@ FROM %@ LEFT JOIN %@ ON (%@.%@ = %@.%@) LEFT JOIN %@ ON (%@.%@ = %@.%@) LEFT JOIN %@ ON (%@.%@ = %@.%@) WHERE %@.%@ = %d AND %@.%@ = \'true\' AND %@.%@ != \'\') GROUP BY decade", kColumnDate, kTableSessionSheet, kTableSheet, kColumnDate, kTableSessionSheet, kTableSheet, kTableSheet, kColumnIdSheet, kTableSessionSheet, kColumnIdSheet, kTableSession, kTableSession, kColumnIdSession, kTableSessionSheet, kColumnIdOfSession, kTablePerson, kTablePerson, kColumnId, kTableSession, kColumnIdPersonSession, kTablePerson, kColumnId, currentPerson.personId, kTableSessionSheet, kColumnLike, kTableSheet, kColumnDate];
        
        
        
        
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getDecadeLikePercentages - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                int count = sqlite3_column_int(statement, 1);
                total = total + count;
            }
        }
        else {
            NSLog(@"%@ - ERROR - getDecadeLikePercentages",kTAG);
        }
        
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            map = [[NSMutableDictionary alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                float percentage = (float) sqlite3_column_int(statement, 1) * 100.0f / total;
                char * decadeChar = (char *)sqlite3_column_text(statement, 0);
                NSString * aDecade = @"";
                if (decadeChar != nil) {
                    aDecade = [NSString stringWithUTF8String:decadeChar];
                }
                aDecade = [[aDecade componentsSeparatedByString:@"-"] objectAtIndex:0];
                aDecade = [aDecade stringByAppendingString:@"0"];
                [map setValue:[NSNumber numberWithFloat:percentage] forKey:aDecade];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getDecadeLikePercentages",kTAG);
        }
    }
    [self closeDatabase];
    return map;
}

- (NSMutableDictionary *) getDecadeViewPercentages {
    Person * currentPerson = ((AppDelegate *)[[UIApplication sharedApplication]delegate]).currentPerson;
    NSMutableDictionary * map = nil;
    int total = 0;
    
    if ([self openDatabase]) {
//        NSString * querySQL = [NSString stringWithFormat:@"SELECT substr (%@ , 1 , 3) AS decade , count(*) AS nbViews FROM (SELECT %@.* , %@.%@ FROM %@, %@, %@, %@ WHERE %@.%@ = %@.%@ AND %@.%@ = %@.%@ AND %@.%@ = %@.%@ AND %@.%@ = %d AND %@.%@ != \'\') GROUP BY decade", kColumnDate, kTableSessionSheet, kTableSheet, kColumnDate, kTableSessionSheet, kTableSheet, kTableSession, kTablePerson, kTableSheet, kColumnIdSheet, kTableSessionSheet, kColumnIdOfSheet, kTableSessionSheet, kColumnIdOfSession, kTableSession, kColumnIdSession, kTableSession, kColumnIdPersonSession, kTablePerson, kColumnId, kTablePerson, kColumnId, currentPerson.personId, kTableSheet, kColumnDate];
        
        
        NSString * querySQL = [NSString stringWithFormat:@"SELECT substr(%@,1,3) AS decade, COUNT(*) as nbViews FROM (SELECT %@.*, %@.%@ FROM %@ LEFT JOIN %@ ON (%@.%@ = %@.%@) LEFT JOIN %@ ON (%@.%@ = %@.%@) LEFT JOIN %@ ON (%@.%@ = %@.%@) WHERE %@.%@ = %d AND %@.%@ != \'\') GROUP BY decade", kColumnDate, kTableSessionSheet, kTableSheet, kColumnDate, kTableSessionSheet, kTableSheet, kTableSheet, kColumnIdSheet, kTableSessionSheet, kColumnIdOfSheet, kTableSession, kTableSession, kColumnIdSession, kTableSessionSheet, kColumnIdOfSession, kTablePerson, kTablePerson, kColumnId, kTableSession, kColumnIdPersonSession, kTablePerson, kColumnId, currentPerson.personId, kTableSheet, kColumnDate];
        
        
        
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getDecadeViewPercentages - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                int count = sqlite3_column_int(statement, 1);
                total = total + count;
            }
        }
        else {
            NSLog(@"%@ - ERROR - getDecadeViewPercentages",kTAG);
        }
        
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            map = [[NSMutableDictionary alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                float percentage = (float) sqlite3_column_int(statement, 1) * 100.0f / total;
                char * decadeChar = (char *)sqlite3_column_text(statement, 0);
                NSString * aDecade = @"";
                if (decadeChar != nil) {
                    aDecade = [NSString stringWithUTF8String:decadeChar];
                }
                aDecade = [[aDecade componentsSeparatedByString:@"-"] objectAtIndex:0];
                aDecade = [aDecade stringByAppendingString:@"0"];
                [map setValue:[NSNumber numberWithFloat:percentage] forKey:aDecade];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getDecadeViewPercentages",kTAG);
        }
    }
    [self closeDatabase];
    return map;
}

// Functions to synchro data
- (Session *) cursorToSession :(sqlite3_stmt *) statement {
    Session * aSession;
    int aSessionId = sqlite3_column_int(statement, 0);
    int aPersonSessionId = sqlite3_column_int(statement, 2);
    char * dateSessionChar = (char *)sqlite3_column_text(statement, 3);
    char * timerSessionChar = (char *)sqlite3_column_text(statement, 4);
    char * satisfactionChar = (char *)sqlite3_column_text(statement, 5);
    char * typeSessionChar = (char *)sqlite3_column_text(statement, 6);
    NSString * aDateSession = @"";
    NSString * aTimerSession = @"";
    NSString * aSatisfaction = @"";
    NSString * aTypeSession = @"";
    
    if (dateSessionChar != nil) {
        aDateSession = [NSString stringWithUTF8String:dateSessionChar];
    }
    if (timerSessionChar != nil) {
        aTimerSession = [NSString stringWithUTF8String:timerSessionChar];
    }
    if (satisfactionChar != nil) {
        aSatisfaction = [NSString stringWithUTF8String:satisfactionChar];
    }
    if (typeSessionChar != nil) {
        aTypeSession = [NSString stringWithUTF8String:typeSessionChar];
    }
    aSession = [[Session alloc] initSessionWithAId:aSessionId andPersonId:aPersonSessionId andDate:aDateSession andTimer:aTimerSession andSatisfaction:aSatisfaction andType:aTypeSession];
    return aSession;
}


- (NSMutableArray *) getSessionsWithoutServerId {
    NSMutableArray * sessions = [[NSMutableArray alloc] init];
    Session * aSession = nil;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ IS NULL", kTableSession, kColumnServerIdSession];
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getSessionWithoutServerId - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            sessions = [[NSMutableArray alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                aSession = [self cursorToSession:statement];
                [sessions addObject:aSession];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getSessionWithoutServerId",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return sessions;
}

- (int) getPersonServerIdByPersonId : (int) aIdPerson {
    int aPersonServerId = 0;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = %d", kTablePerson, kColumnId, aIdPerson];
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getPersonServerIdByPersonId - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                aPersonServerId = sqlite3_column_int(statement, 1);
            }
        }
        else {
            NSLog(@"%@ - ERROR - getSessionWithoutServerId",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return aPersonServerId;
}

- (BOOL) setServerIdToSession : (int) aId : (int) aSessionServerId {
    BOOL isSuccess = NO;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ = %d WHERE %@ = %d", kTableSession, kColumnServerIdSession, aSessionServerId, kColumnIdSession, aId];
        isSuccess = [self executeQueryByString:querySQL];
    }
    [self closeDatabase];
    return isSuccess;
}

- (NSMutableArray *) getSessionsSheetWithoutServerId {
    NSMutableArray * sessionSheets = [[NSMutableArray alloc] init];
    SessionSheet * aSessionSheet = nil;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ IS NULL", kTableSessionSheet, kColumnServerIdSession];
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getSessionsSheetWithoutServerId - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            sessionSheets = [[NSMutableArray alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                aSessionSheet = [self cursorToSessionSheet:statement];
                [sessionSheets addObject:aSessionSheet];
            }
        }
        else {
            NSLog(@"%@ - ERROR - getSessionsSheetWithoutServerId",kTAG);
        }
    }
    [self closeDatabase];
    return sessionSheets;
}

- (int) getSessionServerIdBySessionId : (int) aIdSession {
    int aSessionServerId = 0;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = %d", kTableSession, kColumnIdSession, aIdSession];
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getSessionServerIdBySessionId - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                aSessionServerId = sqlite3_column_int(statement, 1);
            }
        }
        else {
            NSLog(@"%@ - ERROR - getSessionServerIdBySessionId",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return aSessionServerId;
}

- (int) getSheetServerIdBySheetId : (int) aIdSheet {
    int aSheetServerId = 0;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = %d", kTableSheet, kColumnIdSheet, aIdSheet];
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - getSheetServerIdBySheetId - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                aSheetServerId = sqlite3_column_int(statement, 1);
            }
        }
        else {
            NSLog(@"%@ - ERROR - getSheetServerIdBySheetId",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return aSheetServerId;
}

- (BOOL) setServerIdToSessionSheet : (int) aId : (int) aSessionSheetServerId {
    BOOL isSuccess = NO;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ = %d WHERE %@ = %d", kTableSessionSheet, kColumnServerIdSessionSheet, aSessionSheetServerId, kColumnIdSessionSheet, aId];
        isSuccess = [self executeQueryByString:querySQL];
    }
    [self closeDatabase];
    return isSuccess;
}

- (NSString *) pathNameSheet : (int) aId {
    NSString * returnValue = nil;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = %d", kTableSheet, kColumnServerId, aId];
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - pathNameSheet - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                char * resourceNameChar = (char *)sqlite3_column_text(statement, 12);
                if (resourceNameChar != nil) {
                    returnValue = [NSString stringWithUTF8String:resourceNameChar];
                }
            }
        }
        else {
            NSLog(@"%@ - ERROR - pathNameSheet",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return returnValue;
}

- (NSString *) pathNameThSheet : (int) aId {
    NSString * returnValue = nil;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE %@ = %d", kTableSheet, kColumnServerId, aId];
        const char * query_stmt = [querySQL UTF8String];
        NSLog(@"%@ - pathNameThSheet - sql = %@",kTAG,querySQL);
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                char * resourceNameChar = (char *)sqlite3_column_text(statement, 13);
                if (resourceNameChar != nil) {
                    returnValue = [NSString stringWithUTF8String:resourceNameChar];
                }
            }
        }
        else {
            NSLog(@"%@ - ERROR - pathNameThSheet",kTAG);
        }
        sqlite3_finalize(statement);
    }
    [self closeDatabase];
    return returnValue;
}

- (BOOL) updateSheet : (Sheet *) aSheet {
    BOOL isSuccess = NO;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"UPDATE %@ SET %@ = %d, %@ = %d, %@ = \'%@\', %@ = \'%@\', %@ = \'%@\', %@ = \'%@\', %@ = \'%@\', %@ = \'%@\', %@ = \'%@\', %@ = \'%@\', %@ = \'%@\', %@ = \'%@\', %@ = \'%@\', %@ = \'%@\', %@ = \'%@\' WHERE %@ = %d", kTableSheet, kColumnServerIdSheet, aSheet.serverId, kColumnIdPerson, aSheet.idPerson, kColumnName,[self convertString:aSheet.name], kColumnCategory,[self convertString:aSheet.category], kColumnDate,aSheet.date, kColumnQuestion1,[self convertString:[aSheet.questions objectAtIndex:0]], kColumnQuestion2,[self convertString:[aSheet.questions objectAtIndex:1]], kColumnQuestion3,[self convertString:[aSheet.questions objectAtIndex:2]], kColumnHint1,[self convertString:[aSheet.hints objectAtIndex:0]], kColumnHint2,[self convertString:[aSheet.hints objectAtIndex:1]], kColumnHint3,[self convertString:[aSheet.hints objectAtIndex:2]], kColumnResourcesName,aSheet.resourceName, kColumnResourcesName_TH,aSheet.resourceNameTh, kColumnResourcesType,aSheet.resourceType, kColumnAction,aSheet.action, kColumnServerIdSheet, aSheet.serverId];
        isSuccess = [self executeQueryByString:querySQL];
    }
    [self closeDatabase];
    return isSuccess;
}

- (int) deleteSheet : (Sheet *) aSheet andCheckFileNeed : (BOOL) aCheckFileNeeded {
    NSLog(@"deleteSheet : %d", aSheet.serverId);
    int count = 0;
    if ([self openDatabase]) {
        NSString * querySQL = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ = %d", kTableSheet, kColumnServerId, aSheet.serverId];
        [self executeQueryByString:querySQL];
    }
    
    if (aCheckFileNeeded) {
        NSString * querySQL = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@ = \'%@\'", kTableSheet, kColumnResourcesName, aSheet.resourceName];
        const char * query_stmt = [querySQL UTF8String];
        sqlite3_stmt * statement = nil;
        if (sqlite3_prepare_v2(database,query_stmt,-1,&statement,nil) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                count = sqlite3_column_int(statement, 0);
            }
        }
        else {
            NSLog(@"%@ - ERROR - deleteSheetCheckFileNeeded",kTAG);
        }
    }
    [self closeDatabase];
    return count;
}
- (BOOL)existsInDatabase :(int) serverId inTable : (NSString *) table {
    NSLog(@"%@ - existsInDatabase...",kTAG);
    BOOL existed = NO;
    
    NSString * sql = [NSString stringWithFormat:@"SELECT * FROM %@  WHERE %@ = %d LIMIT 1", table,kColumnServerId,serverId];
    NSLog(@"%@ - existsInDatabase - sql = %@",kTAG,sql);
    const char * stmt = [sql UTF8String];
    sqlite3_stmt * statement = nil;
    if (sqlite3_prepare_v2(database,stmt,-1,&statement,nil) == SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            existed = YES;
            NSLog(@"%@ - existsInDatabase", kTAG);
        }
    }
    sqlite3_finalize(statement);
    return existed;
}

- (BOOL) deleteSheetByListId : (NSString *) listId {
    NSLog(@"deleteSheetByListId");
    NSString * sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE %@ in (%@)", kTableSheet, kColumnServerIdSheet, listId];
    return [self executeQueryByString:sql];
}

@end
