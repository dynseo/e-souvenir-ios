//
//  DatabaseManager.h
//  e-souvenir
//
//  Created by Dynseo on 25/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StimartLib/StimartLib.h>
#import "Sheet.h"
#import "Session.h"
#import "SessionSheet.h"
#import "Category.h"
@interface DatabaseManager : STMDatabaseManager

+ (DatabaseManager *)getSharedInstance;
- (BOOL) createDatabase;
- (BOOL) checkATableExist : (NSString *) tableName;
- (BOOL) insertSheet: (Sheet*) aSheet;
- (BOOL) insertSession : (Session *) aSession;
- (BOOL) insertSessionSheet : (SessionSheet *) aSessionSheet;
- (BOOL) updateSessionSheet : (SessionSheet *) aSessionSheet;
- (NSMutableArray *) getAllPersonPseudo;
- (Sheet *) getNameSheetBySheetId : (int) aIdSheet;
- (NSMutableArray *) getListNameSheetsByListSessionSheets : (NSMutableArray *) sessionSheets;
- (Sheet *) cursorToSheet : (sqlite3_stmt *) statement;
- (int) getLastSessionId;
- (NSMutableArray *) getSheetByCategory : (Category *) aCategory;
- (NSMutableArray *) getSheetsByFrieze;
- (SessionSheet *) cursorToSessionSheet : (sqlite3_stmt *) statement;
- (SessionSheet *) getSessionSheetBySessionIdAndSheetId : (int) aSessionId : (int) aSheetId;
- (NSMutableArray *) getSessionForCurrentPerson;
- (NSMutableArray *) getSessionSheetsBySessionId : (int) aSessionId;
- (NSMutableArray *) getSheetsByTaste : (NSString *) aLike;
- (NSString *) getLastSessionSheetLegend : (int) aSheetId ;
- (NSMutableDictionary *) getTopLikedSheets;
- (NSMutableDictionary *) getTopViewedSheets;
- (NSMutableDictionary *) getCategoryLikePercentages;
- (NSMutableDictionary *) getCategoryViewsPercentages;
- (NSMutableDictionary *) getDecadeLikePercentages;
- (NSMutableDictionary *) getDecadeViewPercentages;
- (Session *) cursorToSession :(sqlite3_stmt *) statement;
- (NSMutableArray *) getSessionsWithoutServerId;
- (int) getPersonServerIdByPersonId : (int) aIdPerson;
- (BOOL) setServerIdToSession : (int) aId : (int) aSessionServerId;
- (NSMutableArray *) getSessionsSheetWithoutServerId;
- (int) getSessionServerIdBySessionId : (int) aIdSession;
- (int) getSheetServerIdBySheetId : (int) aIdSheet;
- (BOOL) setServerIdToSessionSheet : (int) aId : (int) aSessionSheetServerId;
- (NSString *) pathNameSheet : (int) aId;
- (NSString *) pathNameThSheet : (int) aId;
- (BOOL) updateSheet : (Sheet *) aSheet;
- (int) deleteSheet : (Sheet *) aSheet andCheckFileNeed : (BOOL) aCheckFileNeeded;
- (BOOL)existsInDatabase :(int) serverId inTable : (NSString *) table;
- (BOOL) deleteSheetByListId : (NSString *) listId;
@end
