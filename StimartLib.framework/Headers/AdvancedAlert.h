/*
 File: AdvancedAlert.h
 Abstract: An advanced color which can be initialized with an hexadecimal representation
 
 Created by Dominique Sauquet on 30/04/12.
 Copyright It's Sauquet 2009-2010. All rights reserved.
 */
//http://stackoverflow.com/questions/3737911/how-to-display-temporary-popup-message-on-iphone-ipad-ios

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AdvancedAlert : UIAlertView {
	
}

- (id)initWithMessage:(NSString *)message dismissAfter:(NSTimeInterval)interval;
- (void)dismissAfterDelay;

@end

