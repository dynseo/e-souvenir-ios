//
//  STMDialogCreateProfessional.h
//  StimartLib
//
//  Created by Dynseo on 26/06/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STMDialogView.h"
#import "ActionSheetStringPicker.h"
#import "Account.h"
@protocol STMDialogCreateProfessionalDelegate;

@interface STMDialogCreateProfessional : STMDialogView

//@property (nonatomic) id<STMDialogCreateProfessionalDelegate> aDelegate;
@property (nonatomic) UILabel *labelPrincipalInfo;
@property (nonatomic) UILabel *labelContact;
@property (nonatomic) UILabel *labelLogin;

@property (nonatomic) UIScrollView *scrollViewCreateProfessional;

@property (nonatomic) UITextField *textFieldProfessionalNamePro;
@property (nonatomic) UITextField *textFieldProfessionalType;
@property (nonatomic) ActionSheetStringPicker *pickerProfessionalType;

@property (nonatomic) UITextField *textFieldProfessionalName;
@property (nonatomic) UITextField *textFieldProfessionalFirstname;
@property (nonatomic) UITextField *textFieldProfessionalEmail;

@property (nonatomic) UITextField *textFieldProfessionalEmailLogin;
@property (nonatomic) UITextField *textFieldProfessionalPassword;
@property (nonatomic) UITextField *textFieldProfessionalConfirmPassword;

@property (nonatomic) UITextField *textFieldProfessionalTel;
@property (nonatomic) UITextField *textFieldProfessionalAddress;
@property (nonatomic) UITextField *textFieldProfessionalZIPCode;
@property (nonatomic) UITextField *textFieldProfessionalCity;
@property (nonatomic) UITextView *textViewLicenseAgreement;
@property (nonatomic) UISwitch *switchLicenseAgreement;



- (id)initWithStyle:(int)aDialogStyle;
- (void)showErrorDuplicateProfessionalName;
- (void)showErrorDuplicateEmail;

@end

//@protocol STMDialogCreateProfessionalDelegate <NSObject>
//@optional
//
//- (void)createProfessionalAccount:(Account *) account;

//@end
