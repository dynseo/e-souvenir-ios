/*
 File: SKProduct+priceAsString.h
 Abstract: To format the price of an in App product
 
 http://stackoverflow.com/questions/2894321/how-to-access-the-price-of-a-product-in-skpayment
 */

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@interface SKProduct (priceAsString)

@property (nonatomic, readonly) NSString *priceAsString;

@end