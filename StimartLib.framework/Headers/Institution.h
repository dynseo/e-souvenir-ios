//
//  Institution.h
//  Stimart
//
//  Created by Jiaji on 29/09/2015.
//  Copyright © 2015 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Institution : NSObject

@property NSString *name;
@property NSString *type;
@property NSString *contactName;
@property NSString *contactEmail;
@property NSString *contactTel;
@property NSString *address;
@property NSString *zipCode;
@property NSString *city;

@property NSString *loginEmail;
@property NSString *password;

- (id)initWithName:(NSString *)aName andType:(NSString *)aType andContactName:(NSString *)aContactName andContactEmail:(NSString *)aContactEmail andContactTel:(NSString *)aContactTel andAddress:(NSString *)aAddress andZipCode:(NSString *)aZipCode andCity:(NSString *)aCity andLoginEmail:(NSString *)aLoginEmail andPassword:(NSString *)aPassword;

- (void)saveToUserDefaults;

@end
