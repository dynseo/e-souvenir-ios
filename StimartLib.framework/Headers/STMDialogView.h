//
//  DialogView.h
//  Stimart
//
//  Created by Jiaji SUN on 10/09/2014.
//  Copyright (c) 2014 Dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STMButton.h"
#import "AbstractActionSheetPicker.h"
#import "RadioGroupWithScroll.h"
#import "DimensionAdapter.h"
#import "Account.h"

/* dialog with titleUp, buttonMiddle */
extern int const STMDialogTypeBasicWithOneButton;
/* dialog with titleUp, buttonLeft and buttonRight */
extern int const STMDialogTypeBasicWithTwoButtons;
/* dialog with titleUp, titleMiddle, buttonLeft and buttonRight (ex: dialog quit/play again) */
extern int const STMDialogTypeNormal;
/* dialog with titleUp, textView, and buttonMiddle */
extern int const STMDialogTypeTextWithOneButton;
/* dialog with titleUp, textView, buttonLeft, and buttonRight */
extern int const STMDialogTypeTextWithTwoButtons;
/* dialog with buttonUp, buttonDown and buttonMiddle (ex: dialog synchro/update) */
extern int const STMDialogTypeTwoButtons;
/* dialog with buttonUp, buttonDown, buttonMiddle and buttonLeft (ex: dialog setting de e-souvenir)*/
extern int const STMDialogTypeThreeButtons;
/* dialog with buttonUp, buttonDown, buttonMiddle and titleUp */
extern int const STMDialogTypeTwoButtonsWithTitle;
/* dialog with a picker, titleUp, titleMiddle and buttonMiddle */
extern int const STMDialogTypePicker;
/* dialog with a picker, titleUp, titleMiddle, buttonLeft and buttonRight (ex: dialog for profile connection)*/
extern int const STMDialogTypePickerWithTwoButtons;
/* dialog when downloading a file (ex: download resources, app update) */
extern int const STMDialogTypeDownloader;
/* dialog with titleUp, textView and loop buttons (ex: puzzle/quizzle image description) */
extern int const STMDialogTypeTextReader;
/* dialog with titleUp and radio buttons (ex: quizzle -> choose hidden image) */
extern int const STMDialogTypeRadioButtons;
extern int const STMDialogTypeMood;

// for BPCE
extern int const STMDialogTypeTextWithTwoButtonsAndTextfield;
extern int const STMDialogTypeTextWithThreeButtons;

/* dialog style edith (round dialog) */
extern int const STMDialogStyleEhpad;
/* dialog style edith (rectangle dialog) */
extern int const STMDialogStylePapy;

@protocol STMDialogViewDelegate;

@interface STMDialogView : UIView

/* we can create several types of dialogs
 1 - dialog with title1, title2, and buttons yes/no
 2 - dialog with only 2 buttons */

@property (nonatomic) id<STMDialogViewDelegate>delegate;

@property (nonatomic) int dialogType;
/* style is different between app versions */
@property (nonatomic) int dialogStyle;
@property (nonatomic) UIView *backgroundView;
/* button left */
@property (nonatomic) STMButton *buttonLeft;
@property (nonatomic) STMButton *buttonRight;
@property (nonatomic) STMButton *buttonMiddle;
@property (nonatomic) STMButton *buttonUp;
@property (nonatomic) STMButton *buttonDown;

@property (nonatomic) UILabel *titleUp;

/* a label under the title up */
@property (nonatomic) UILabel *titleUp2;

@property (nonatomic) UILabel *titleMiddle;
@property (nonatomic) UILabel *titleLeft;

/* main text zone */
@property (nonatomic) UITextView *textView;

@property (nonatomic) UITextField *textFieldPicker;

/* only used for resource download */
@property (nonatomic) UIProgressView *progressView;
@property (nonatomic) UILabel *progressLabel;

@property (nonatomic) UIScrollView *textZoneScrollView;
@property (nonatomic) UIWebView *textZoneWebView;
@property (nonatomic) UIButton *buttonZoomIn;
@property (nonatomic) UIButton *buttonZoomOut;

@property (nonatomic) NSArray *moodButtons;
@property (nonatomic) NSArray *moodImageNames;
@property (nonatomic) DimensionAdapter * dimensionAdapter1;

@property (nonatomic) RadioGroupWithScroll * radioGroupView;

@property (nonatomic) int currentMood;

- (void)setTitleUpText: (NSString *) aTitle;
- (void)setTitleUpTextSize:(int)aSize;
- (void)setTitleUpText:(NSString *)aTitle andSize:(int)aFontSize;
- (void)setTitleMiddleText: (NSString *) aTitle;
- (void)setTitleMiddleTextSize:(int)aSize;
- (void)setTitleLeftText: (NSString *) aTitle;
- (void)setTextViewText: (NSString *) aTitle;
- (void)setTextFieldPickerPlaceholderText:(NSString *)aText;
- (void)setTextZoneText:(NSString *)aText;

- (NSString *)getStringTextFieldPicker;

- (id)initWithType:(int)aDialogType andStyle:(int)aDialogStyle andBackground:(NSString *)aBackgroundString;

/* initialize buttons with params */
- (void)setupButtonFor:(int)buttonId andText:(NSString *)aTitle andColor:(NSString *) aColorString andHighlight:(NSString *) aHightligntString;

/* setup button with text and images */
- (void)setupButtonFor:(int)buttonId andText:(NSString *)aTitle andBackgroundImageName:(NSString *) aBackgroundImageName andHighlightImageName:(NSString *) aHightligntImageName;

/* initialize button action */
- (void)setTarget:(id)target forButton:(int)buttonId action:(SEL)selector;

- (void)setShowPickerAction:(SEL)action;

- (void)setBackgroundImageWithImageName:(NSString *)imageName;

- (void)createZoomButtonsWithGameName:(NSString *)gameName;

- (void)createRadioGroupViewWithOptions:(NSArray *)options;

- (void)createTitleMiddle;

- (void)createButtonLeft;

- (void)createButtonRight;

- (void)createTitleUp;

- (void)createTextView;

- (int)currentMoodScore;

- (void)resetMoodState;

- (void)setHiddenWithAnimation:(BOOL)hidden;

-(UILabel *) getTitleMidle;

@end

@protocol STMDialogViewDelegate <NSObject>
@optional

- (void)moodSelected:(int)moodNumber;
- (void) createAccount : (Account *) account;

@end
