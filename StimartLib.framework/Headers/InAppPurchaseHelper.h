/*
 File: InAppPurchaseHelper
  
 Created by Dominique Sauquet on 11/10/2012.
 Copyright It's Sauquet 2012-2013. All rights reserved.
*/

//See : http://www.raywenderlich.com/2797/introduction-to-in-app-purchases
//See : http://troybrant.net/blog/2010/01/in-app-purchases-a-full-walkthrough/

#import <Foundation/Foundation.h>
#import "StoreKit/StoreKit.h"
#import "MBProgressHUD.h"

#define kProductsLoadedNotification         @"ProductsLoaded"
#define kProductPurchasedNotification       @"ProductPurchased"
#define kProductPurchaseFailedNotification  @"ProductPurchaseFailed"

typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray * products);

@interface InAppPurchaseHelper : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
    
    NSSet * _productIdentifiers;
    NSArray * _products;
    NSMutableSet * _purchasedProducts;
    SKProductsRequest * _request;
    
    RequestProductsCompletionHandler _completionHandler;
}

@property (retain) NSSet *productIdentifiers;
@property (retain) NSArray * products;
@property (retain) NSMutableSet *purchasedProducts;
@property (retain) SKProductsRequest *request;
@property (retain) MBProgressHUD *hud;

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (void)initProductIdentifiers:(NSSet *)productIdentifiers;

- (BOOL)isPurchased:(NSString *)productIdentifier;

- (void)requestProductData;
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response;

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;
- (BOOL)isProductsListInited;
- (BOOL)canMakePurchases;

- (bool)buyProductIdentifier:(NSString *)productIdentifier;
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue;

- (void)provideContent:(NSString *)productIdentifier;

@end
