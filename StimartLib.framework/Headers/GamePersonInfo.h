//
//  GamePersonInfo.h
//  Stimart
//
//  Created by Dynseo on 26/07/2016.
//  Copyright © 2016 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GamePersonInfo : NSObject



@property  int gamePersonInfoId;
@property  NSString * info1;
@property NSString * info2;

- (id)initWithGamePersonInfo:(int)aId andInfo1:(NSString*)aInfo1 andInfo2:(NSString*)aInfo2;
-(NSString*) toString;

@end