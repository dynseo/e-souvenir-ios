//
//  SizeIphone5.h
//  Stimart
//
//  Created by Dynseo on 31/03/2017.
//  Copyright © 2017 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DimensionAdapter.h"

@interface SizeIphone5 : DimensionAdapter
-(id) initSizeIphone5;
@end
