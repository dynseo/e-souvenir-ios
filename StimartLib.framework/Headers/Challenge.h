//
//  Challenge.h
//  Stimart
//
//  Created by Houssem OUERTANI on 07/06/2016.
//  Copyright © 2016 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Challenge : NSObject

@property int idChallenge;
@property NSString * question;
@property NSMutableArray * options;
@property NSMutableArray * optionsBeforeShuffe;
@property NSString * audioFileName;
@property NSString * imageFileName;
@property int indexAnswer;
@property int chosenAnswer;
@property NSString * category;
@property NSString * belongsTo;
@property int level;
@property int serverId;
@property NSString * lang;

-(id) initChallengeWithOptions:(NSMutableArray *) aOptions andIndexAnswer:(int) aIndexAnswer;
-(id) initChallengeWithQuestion: (NSString *) aQuestion andOptions: (NSMutableArray *) aOptions andIndexAnswer:(int) aIndexAnswer;
-(id) initChallengeWithQuestion:(NSString *)aQuestion andOptions:(NSMutableArray *)aOptions andIndexAnswer:(int)aIndexAnswer andCategory:(NSString *) aCategory;
-(id) initChallengeWithQuestion:(NSString *)aQuestion andOptions:(NSMutableArray *)aOptions andIndexAnswer:(int)aIndexAnswer andCategory:(NSString *) aCategory andAudioFileName: (NSString *) anAudioFileName andImageFileName: (NSString *) anImageFileName;
-(id) initChallengeWithQuestion: (NSString *) aQuestion andOptions: (NSMutableArray *) aOptions andIndexAnswer:(int) aIndexAnswer andCategory:(NSString *) aCategory andAudioFileName: (NSString *) anAudioFileName andImageFileName: (NSString *) anImageFileName andBelongsTo:(NSString *) aBelongsTo andLevel:(int) aLevel andServerId:(int) aServerId andId:(int)aId;
-(id) initChallengeFromJson:(NSDictionary *) jsonChallenge;

-(NSString *) toString;
-(NSString *) toStringWithOptions;
-(NSString *) getAnswer;

+(NSMutableArray *) randomizeAnswers:(NSMutableArray *) challenges;
+(void) shuffleArray:(NSMutableArray *) options;
- (void) setChosenAnswerPosition : (int) index;

@end
