//
//  STMDatabaseManager.h
//  Stimart
//
//  Created by Jiaji SUN on 12/11/2014.
//  Copyright (c) 2014 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Person.h"
#import "Result.h"
#import "Invitation.h"
#import "GamePersonInfo.h"
@interface STMDatabaseManager : NSObject

extern sqlite3 * database;

@property (nonatomic) NSString * databasePath;

+ (STMDatabaseManager *)getSharedInstance;

- (BOOL)createDatabase;

- (void)closeDatabase;

- (BOOL)openDatabase;

-(int)getDatabaseVersion;

- (void)setDatabaseVersion:(int)version;

- (BOOL)insertPerson:(Person *)person;

/* insert a person into the local database */
- (BOOL)insertPersonWithServerId:(int)serverId andConflictId:(int)conflictId andFirstName:(NSString *)firstName andName:(NSString *)name andBirthdate:(NSString *)birthdate andSex:(NSString *)sex andRole:(NSString *)role;

/* update a person */
- (BOOL)updatePersonWithId:(int)aId andServerId:(int)serverId andConflictId:(int)conflictId andFirstName:(NSString *)firstName andName:(NSString *)name andBirthdate:(NSString *)birthdate andSex:(NSString *)sex andRole:(NSString *)role;
- (BOOL) updatePerson : (Person *) person;
- (Person *)getPersonWithFirstName:(NSString *)aFirstName andName:(NSString *)aName;
- (int)getPersonIdWithFirstName:(NSString *)aFirstName andName:(NSString *)aName;
- (Person *)getPersonWithId:(int)aId;

/* get a person from the local database with his server id */
- (Person *)getPersonWithServerId:(int)aServerId;

- (NSArray *)getAllPersons;

- (NSArray *)getAllPersonsWithCondition:(NSString *)condition;

- (BOOL)deleteAllPersons;

- (int)getPersonIdWithServerId:(int)aServerId;

/* server id of a person is -1 by default, but it will be changed after insertion of the person into server database */
- (NSArray *)getAllPersonsWithoutServerId;

- (NSArray *)getAllResults;

- (NSArray *)getAllResultsWithCondition:(NSString *)condition;

- (NSArray *)getAllResultsForPersonsHavingServerId;

- (int)getLastAddedResultId;

- (BOOL)isExistPersonWithFirstName:(NSString *) firstName andName: (NSString *)name;

- (BOOL)insertResult:(Result *)result;

- (BOOL)modifyServerId:(int)serverId forPerson:(Person *)person;

- (BOOL)modifyConflictId:(int)conflictId forPerson:(Person *)person;

/* modify person with a json of format: {"person":{"localName":"NameTest","solved":"Merge","localFristName":"FirstnameTest","name":"NameTest","personId":"120","firstName":"FirstnameTest","serverId":"443"}} */
- (BOOL)updatePerson:(Person *)person withJson:(id)jsonResponse;

- (BOOL)deletePersonWithId:(int)aId;

- (BOOL)deleteResultWithId:(int)aId;

- (BOOL)deleteResultsForPersonId:(int)aId;

- (BOOL)deleteAllResults;

/* check number of results for a game */
- (int)getGameNbTriesWithGameDatabaseId:(NSString *)gameId;
/* get number of profiles created */
- (int)getNumPersons;
/* update old version results */
- (void)treatOldVersionResults;

#pragma added since build 9.1.2

/* insert an invitation to database */
- (BOOL)insertInvitation:(Invitation *)invitation;
- (int)getInvitationIdWithServerId:(int)serverId;
/* change an invitation's state */
- (BOOL)updateInvitationWithId:(int)invitationId andState:(int)aState andNeedSend:(BOOL)needSend;
- (BOOL)updateInvitationWithId:(int)invitationId andNeedSend:(BOOL)needSend;
- (NSMutableArray<Invitation *>*)getAllInvitationsHandled;
- (NSMutableArray<Invitation *>*)getAllInvitationsHandledNeedSend;
/* get all invitations of a person */
- (NSMutableArray<Invitation *>*)getInvitationsForPersonWithId:(int)personId;
/* get new invitations (not read, state received) of a person */
- (NSMutableArray<Invitation *>*)getNewInvitationsForPersonWithId:(int)personId;

- (GamePersonInfo *) setGamePersonInfosWithGpiId :(int)aGpiId andPlayerId:(int)aPlayerId andGame:(NSString*)aGame andLevel:(int)aLevel andInfo1:(NSString*)info1 andInfo2:(NSString*)info2;
- (void) setGamePersonInfos : (GamePersonInfo *) gamePersonInfo;
- (GamePersonInfo*) getGamePersonInfos: (NSString *) aGame andPersonId : (int) aPersonId;
- (GamePersonInfo*) getGamePersonInfos: (NSString*)aGame andPersonId:(int)aPersonId andLevel:(int)aLevel;
- (GamePersonInfo*) getGamePersonInfos: (NSString*)aGame andPersonId:(int)aPersonId andLevel:(int)aLevel andInfo2:(NSString*)aInfo2;
- (BOOL) deleteGamePersonInfoWithId : (int) aId;
- (void) cleanGamePersonInfoByGame :(NSString *) aGame andIdList : (NSString *) anIdList;

-(BOOL) executeQueryByString : (NSString *) sqlQuery;
-(BOOL) executeQueryByChar : (const char *) sqlQuery;
- (NSString *)stringWithApostrophesSolvedForDatabase:(NSString *)aString;
@end
