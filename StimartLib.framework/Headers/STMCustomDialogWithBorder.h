//
//  STMCustomDialogWithBorder.h
//  Stimart
//
//  Created by Jiaji SUN on 22/12/2014.
//  Copyright (c) 2014 Dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioGroupWithScroll.h"
#import "STMButton.h"

@interface STMCustomDialogWithBorder : UIView

typedef NS_ENUM(NSInteger, STMCustomViewWithBorderType) {
    /* type qcm (ex: quizzle) */
    STMCustomViewWithBorderTypeQCM = 1,
    /* type qcm with radio buttons (ex: quizzle) */
    STMCustomViewWithBorderTypeRadioButtons = 2,
    /* type text description with zoom buttons */
    STMCustomViewWithBorderTypeText = 3,
};

@property (nonatomic) UIView *mainView;

/* main title label */
@property (nonatomic) UILabel *labelTitle;

@property (nonatomic) NSArray *optionButtons;

@property (nonatomic) STMButton *buttonBottom;

@property (nonatomic) UIButton *buttonZoomIn;

@property (nonatomic) UIButton *buttonZoomOut;

@property (nonatomic) UIScrollView *textZoneScrollView;

@property (nonatomic) UIWebView *textZoneWebView;

@property (nonatomic) RadioGroupWithScroll * radioGroupView;

- (id)initWithType:(int)aViewType andFrame:(CGRect)frame andBorderColor:(UIColor *)aColor andRadioOptions:(NSArray *)options;

/* set the main title of the dialog */
- (void)setTitle:(NSString *)aTitle;

/* set option buttons titles (for dialog type QCM1) */
- (void)setOptionButtonsTitles:(NSArray *)titles;

- (void)setOptionButtonsBackgroundColor:(NSString *)backgroundColorString andHightlight:(NSString *)highlightColorString;

- (void)setButtonBottomBackgroundColorWithString:(NSString *)backgroundColorString andHighlightColorWithString:(NSString *)highlightColorString;

- (void)setButtonBottomTitle:(NSString *)aTitle;

- (void)setTextZoneText:(NSString *)aText;

@end
