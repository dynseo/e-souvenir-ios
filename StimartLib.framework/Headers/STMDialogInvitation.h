//
//  STMDialogInvitation.h
//  Stimart
//
//  Created by Jiaji on 20/11/2015.
//  Copyright © 2015 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "STMDialogView.h"
#import "Person.h"
#import "Invitation.h"

@interface STMDialogInvitation : STMDialogView

- (id)initWithStyle:(int)aDialogStyle;

- (void)setInvitationSenderName:(NSString *)senderName;

@end
