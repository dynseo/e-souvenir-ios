//
//  XMLGenericParser.h
//
//  Created by Dominique Sauquet on 18/05/11.
//  Copyright 2011 It's Sauquet.com. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol ParserDelegate <NSObject>
- (void)receivedItems:(NSArray *)theItems;
- (void)onDownloadFail;
@end

@interface XMLGenericParser : NSObject <NSXMLParserDelegate> {
    id _delegate;
	
    NSMutableData *responseData;
    NSMutableArray *items;
	
    NSMutableDictionary *item;
    NSString *currentElement;
    NSMutableString *buffer;
    
    NSString *rootTag;
}

@property (nonatomic) NSMutableData *responseData;
@property (nonatomic) NSMutableArray *items;
@property (nonatomic) NSMutableDictionary *item;
@property (nonatomic) NSString *currentElement;
@property (nonatomic) NSMutableString *buffer;
@property (nonatomic) NSString *rootTag;

/* parse from a url */
- (void)parseFile:(NSString *)url rootTag:(NSString *)aTag withDelegate:(id)aDelegate;
/* parse from a url with a specific encoding */
- (void)parseFileEncoding:(NSString *)url rootTag:(NSString *)aTag withDelegate:(id)aDelegate;
/* parse from a local file */
- (void)parseLocalFile:(NSString *)path rootTag:(NSString *)aTag withDelegate:(id)aDelegate;

- (id)delegate;
- (void)setDelegate:(id)new_delegate;
- (void)clearDelegate;

@end