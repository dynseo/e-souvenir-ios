//
//  Score.h
//  Stimart
//
//  Created by itssauquet on 03/06/2016.
//  Copyright © 2016 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Score : NSObject

@property int type;
//@property int duration;
@property NSTimeInterval duration;
@property int isInterrupted;
@property int score1;
@property int score2;
@property int score3;
@property int score4;
@property int score5;
@property int score6;
@property int nbPoints;


- (id)initScoreWithDuration:(NSTimeInterval)aDuration andInterrupted:(BOOL)aInterrupted andScore1:(int)aScore1 andScore2:(int)aScore2 andScore3:(int)aScore3 andScore4:(int)aScore4 andScore5:(int)aScore5 andScore6:(int)aScore6;

- (void)resetScore;

@end