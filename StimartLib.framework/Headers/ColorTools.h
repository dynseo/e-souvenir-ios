//
//  ColorTools.h
//  Stimart
//
//  Created by Jiaji SUN on 06/05/2014.
//  Copyright (c) 2014 Dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorTools : UIColor

+ (UIColor *)colorFromHexString:(NSString *)hexString;

+ (UIColor *)colorFromLocalizedStringKey:(NSString *)key;

+ (UIImage *)imageWithColor:(UIColor *)color;



@end
