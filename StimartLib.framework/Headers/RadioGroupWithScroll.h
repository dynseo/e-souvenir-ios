//
//  RadioGroupWithScroll.h
//  test3
//
//  Created by Jiaji SUN on 25/09/2014.
//  Copyright (c) 2014 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RadioGroupWithScrollDelegate;

@interface RadioGroupWithScroll : UIScrollView {
    NSMutableArray *radioButtons;
}
@property (nonatomic) id<RadioGroupWithScrollDelegate> aDelegate;
@property (nonatomic) NSMutableArray *radioButtons;

@property (nonatomic) int countElement;
@property (nonatomic) int indexElementCurrentSelected;

- (id)initWithFrame:(CGRect)frame andOptions:(NSArray *)options andColumns:(int)columns;
- (void)radioButtonClicked:(UIButton *) sender;
- (void)removeButtonAtIndex:(int)index;
- (void)setSelected:(int) index;
- (void)clearAll;
- (void)setFontSize:(float)aSize;
- (void)setFont:(UIFont *)aFont;
- (void)setColor: (UIColor *)aColor;
- (void)setElementWidth:(int)aWidth andHeight:(float)aHeight;
- (void)resetSelectState;

@end

@protocol RadioGroupWithScrollDelegate <NSObject>
@optional
- (void)filterSelected:(int)index;
@end
