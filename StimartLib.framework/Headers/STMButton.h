//
//  CustomButton.h
//  Stimart
//
//  Created by Jiaji SUN on 06/05/2014.
//  Copyright (c) 2014 Dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ColorTools.h"
#import "STMUtilities.h"

typedef NS_ENUM(NSInteger, STMButtonType) {
    /* button type normal (with background color, hightlight color and selected color) */
    STMButtonTypeNormal = 1,
    /* button type image (with background image and hightlight image) */
    STMButtonTypeImage = 2,
    /* button type rounded (rounded, with background color, hightlight color and selected color) */
    STMButtonTypeRounded = 3,
};

@interface STMButton : UIButton {
    int buttonType;
    CGRect frame;
    NSString *title;
    NSString *backgroundColor;
    NSString *highlightColor;
    NSString *selectedColor;
    NSString *backgroundImageName;
    NSString *highlightImageName;
}

- (id)init;

/* init a button of type normal */
- (id)initWithFrame:(CGRect)aFrame andType:(int)aType andTitle:(NSString *)aTitle andTitleSize: (int)titleSize andBackgroundColor:(NSString *)aBackgroundColor andHighlightColor:(NSString *)aHighlightColor;

/* init a button of type image */
- (id)initWithFrame:(CGRect)aFrame andTitle:(NSString *)aTitle andTitleSize: (int)titleSize andBackgroundImageName:(NSString *)aBackgroundImageName andHighlightImageName:(NSString *)aHighlightImageName;

- (id)initWithFrame:(CGRect)aFrame;

/* set frame and set button colors */
- (void)setFrame:(CGRect)aFrame andType:(int)aType andTitle:(NSString *)aTitle andTitleSize:(int)titleSize andBackgroundColor:(NSString *)aBackgroundColor andHighlightColor:(NSString *)aHighlightColor;

- (void)setBackgroundColorWithString: (NSString *)aColor;

- (void)setHighlightColorWithString: (NSString *)aColor;

- (void)setSelectedColorWithString: (NSString *)aColor;

- (void)setTitleColorWithString: (NSString *)aColor;

- (void)setTitle: (NSString *)aTitle;

- (void)setTitle:(NSString *)aTitle andSize:(int)titleSize;

- (void)setTitle:(NSString *)aTitle andType:(int)aType andBackgroundColor:(NSString *)aColor andHighlightColor:(NSString *)aHighlight;

- (void)setTitle:(NSString *)aTitle andType:(int)aType andBackgroundColor:(NSString *)aColor andHighlightColor:(NSString *)aHighlight andTitleSize:(int)titleSize;

- (void)setTitle:(NSString *)aTitle andBackgroundImageName:(NSString *)aBackgroundImage andHighlightImageName:(NSString *)aHighlightImageName;

- (id)initWithFrame:(CGRect)aFrame andType:(int)aType andBackgroundColor:(NSString *)aColor andHighlightColor:(NSString *)aHighlightColor;

- (id)initWithFrame:(CGRect)aFrame andBackgroundImageName:(NSString *)aBackgroundImageName andHighlightImageName:(NSString *)aHighlightImageName;

/* play a button clicked animation (button zoom in/out) */
- (void)playClickAnimation;

- (void)setBackgroundColorWithString:(NSString *)aColor andHighlightColor:(NSString *)aHighlight;

- (void)setBackgroundImageWithName:(NSString *)aBackgroundImageName andHighlightImageName:(NSString *)aHighlightImageName;

/* add a shadow effect for the button */
- (void)addShadow;

@end
