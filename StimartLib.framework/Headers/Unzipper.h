//
//  Resources.h
//  Stimart
//
//  Created by Dynseo on 03/08/2016.
//  Copyright © 2016 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Unzipper : NSObject

+ (void) upZipFileWithFilePath:(NSString *) filePath;

@end