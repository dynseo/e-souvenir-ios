//
//  StimartLib.h
//  StimartLib
//
//  Created by Dynseo on 19/04/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for StimartLib.
FOUNDATION_EXPORT double StimartLibVersionNumber;

//! Project version string for StimartLib.
FOUNDATION_EXPORT const unsigned char StimartLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <StimartLib/PublicHeader.h>


#import <StimartLib/Challenge.h>
#import <StimartLib/GamePersonInfo.h>
#import <StimartLib/iCarousel.h>
#import <StimartLib/Person.h>
#import <StimartLib/Result.h>
#import <StimartLib/Score.h>
#import <StimartLib/STMDatabaseManager.h>
#import <StimartLib/Invitation.h>
#import <StimartLib/Institution.h>
#import <StimartLib/Subscription.h>
#import <StimartLib/STMConstants.h>
#import <StimartLib/STMDateManager.h>
#import <StimartLib/STMURLFactory.h>
#import <StimartLib/STMUtilities.h>
#import <StimartLib/STMCustomDialogWithBorder.h>
#import <StimartLib/STMDialogCreateInstitution.h>
#import <StimartLib/STMDialogCreateProfessional.h>
#import <StimartLib/STMDialogIdentification.h>
#import <StimartLib/STMDialogInvitation.h>
#import <StimartLib/STMDialogView.h>
#import <StimartLib/STMDialogCreateUser.h>
#import <StimartLib/DimensionAdapter.h>
#import <StimartLib/STMButton.h>

#import <StimartLib/RadioGroupWithScroll.h>

#import <StimartLib/AbstractActionSheetPicker.h>
#import <StimartLib/ActionSheetDatePicker.h>
#import <StimartLib/ActionSheetStringPicker.h>
#import <StimartLib/SWActionSheet.h>

// AFDownloadRequest
#import <StimartLib/AFDownloadRequestOperation.h>
// AFNetworking
#import <StimartLib/AFHTTPRequestOperation.h>
#import <StimartLib/AFHTTPRequestOperationManager.h>
#import <StimartLib/AFHTTPSessionManager.h>
#import <StimartLib/AFNetworking.h>
#import <StimartLib/AFNetworkReachabilityManager.h>
#import <StimartLib/AFSecurityPolicy.h>
#import <StimartLib/AFURLConnectionOperation.h>
#import <StimartLib/AFURLRequestSerialization.h>
#import <StimartLib/AFURLResponseSerialization.h>
#import <StimartLib/AFURLSessionManager.h>

#import <StimartLib/MZTimerLabel.h>
#import <StimartLib/MBProgressHUD.h>
#import <StimartLib/FCUUID.h>
#import <StimartLib/UICKeyChainStore.h>

// inApp
#import <StimartLib/InAppPurchaseHelper.h>
#import <StimartLib/SKProduct+priceAsString.h>

#import <StimartLib/Unzipper.h>
#import <StimartLib/ColorTools.h>

#import <StimartLib/XMLGenericParser.h>
#import <StimartLib/SSZipArchive.h>

#import <StimartLib/ACSimulatorRemoteNotificationsService.h>
#import <StimartLib/AdvancedAlert.h>
#import <StimartLib/crypt.h>
#import <StimartLib/mztools.h>
#import <StimartLib/Reachability.h>
#import <StimartLib/SizeIpad.h>
#import <StimartLib/SizeIphone5.h>
#import <StimartLib/SizeIphone6.h>
#import <StimartLib/SizeIphone6Plus.h>
#import <StimartLib/SynchroResources.h>
#import <StimartLib/UIApplication+SimulatorRemoteNotifications.h>
#import <StimartLib/zip.h>
