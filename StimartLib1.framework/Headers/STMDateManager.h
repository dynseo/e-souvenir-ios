//
//  STMDateManager.h
//  Stimart
//
//  Created by Jiaji on 14/12/2015.
//  Copyright © 2015 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STMDateManager : NSObject

+ (STMDateManager *)getSharedInstance;

/* convert NSDate to NSString with format "yyyy-MM-dd HH:mm:ss" */
- (NSString *)dateToStringFormatDatabase:(NSDate *)date;
/* convert NSDate to NSString with simple date format given in Localizable.strings */
- (NSString *)dateToString:(NSDate *) date;
/* convert NSDate to NSString with format given in Localizable.strings */
- (NSString *)dateTimeToString:(NSDate *) date;
- (NSString *)dateToSimpleString:(NSDate *)date;

/* date that ignore time, ex: 2015-02-15 14:33 -> 2015-20-15 00:00 */
- (NSDate *)dateToSimpleDate:(NSDate *)date;

/* 2015-01-01 + 3 months => 2015-04-01 */
- (NSDate *)dateFrom:(NSDate *)dateBegin byAddNumberOfMonths:(int)nbMonths;
- (NSDate *)dateFrom:(NSDate *)dateBegin byAddNumberOfDays:(int)nbDays;

/* convert NSString of format "yyyy-MM-dd" to NSDate */
- (NSDate *)stringToDate:(NSString *)stringDate;

- (NSDateFormatter *)getDateFormatter;

-(NSString *)formatPassedDate:(NSString *)aDate prefix:(NSString *)prefix;
+(int) getDayFromNSDate : (NSDate *) aDate;
+(int) getMonthFromNSDate : (NSDate *) aDate;
+(int) getYearFromNSDate : (NSDate *) aDate;
+(NSDate *) setDayForNSDate : (NSDate *) aDate : (int) aDay;
+(NSDate *) setMonthForNSDate : (NSDate *) aDate : (int) aMonth;
+(NSDate *) setYearForNSDate : (NSDate *) aDate : (int) aYear;
@end

