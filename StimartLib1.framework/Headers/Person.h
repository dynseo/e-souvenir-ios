//
//  Person.h
//  Stimart
//
//  Created by Jiaji SUN on 10/04/2014.
//  Copyright (c) 2014 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString * const kWoman;
FOUNDATION_EXPORT NSString * const kMan;

@interface Person : NSObject

@property int personId;
@property int serverId;
@property int conflictId;
@property NSString *firstName;
@property NSString *name;
/* pseudo is used for the profile connection picker, don't save it in database */
@property NSString *pseudo;
@property NSString *sex;
@property NSString *birthdate;
/* "A" for animator, "P" for normal users */
@property NSString *role;

- (id)initWithId:(int)aPersonId serverId:(int) aServerId conflictId:(int) aConflictId firstName:(NSString *)aFirstName name:(NSString *)aName birthdate:(NSString *)aBirthdate sex:(NSString *)aSex role:(NSString *)aRole;

- (id)initWithFirstName:(NSString *)aFirstName name:(NSString *)aName birthdate:(NSString *)aBirthdate sex:(NSString *)aSex role:(NSString *)aRole;

- (id)initWithServerId:(int) aServerId conflictId:(int)aConflictId firstName:(NSString *)aFirstName name:(NSString *)aName birthdate:(NSString *)aBirthdate sex:(NSString *)aSex role:(NSString *)aRole;

+ (BOOL)isVisitor:(Person *)aPerson;

@end
