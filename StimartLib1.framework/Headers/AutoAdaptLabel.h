//
//  AutoAdaptLabel.h
//  Stimart
//
//  Created by Jiaji SUN on 29/12/2014.
//  Copyright (c) 2014 Dynseo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, AutoAdaptLabelStyle) {
    AutoAdaptLabelStyleWhiteBackground = 1,
    AutoAdaptLabelStyleBlackBackground = 2,
};

@interface AutoAdaptLabel : UILabel
{
    float paddingLeft, paddingRight, paddingTop, paddingBottom;
}

@property (nonatomic) float maxWidth;

- (void)showMessage:(NSString *)message dismissAfter:(NSTimeInterval)interval;

- (void)showInCenterOfScreen;

- (void)setStyle:(int)aStyle;

- (void)setStyle:(int)aStyle andTransparence:(float)transparence;

@end
