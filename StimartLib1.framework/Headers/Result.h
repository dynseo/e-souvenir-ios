//
//  Result.h
//  Stimart
//
//  Created by Jiaji SUN on 14/11/2014.
//  Copyright (c) 2014 Dynseo. All rights reserved.
//
//  For most of the games, score1 corresponds to the number of good answers, and score2 corresponds to the number of bad answers.

//  Specially, for "Puzzle": we give score1 = 1, and score2 = 1 when user finish the round, else score1 and score2 = 0

//  score3 and score4 is currently only used by game "Quizzle": score3 corresponds to the number of tries for finding the image hidden, and score4 = 1 if the user find the image hidden, else score4 = 0

//  For certain games, we don't need scores, such as "a text a day" and "a poem a day", etc.

#import <Foundation/Foundation.h>
#import "Score.h"

@interface Result : NSObject

@property int resultId;
@property int personId;
@property int personServerId;
@property NSString * date;
@property NSString * game;
@property int level;

@property (nonatomic,retain) Score* score;

- (id)initWithResultId:(int)aId andPersonId:(int)aPersonId andDate:(NSString *)aDate andGame:(NSString *)aGame andLevel:(int)aLevel andPersonServerId:(int)aPersonServerId andScore:(Score *)aScore;

- (id)initWithResultId:(int)aId andPersonId:(int)aPersonId andDate:(NSString *)aDate andGame:(NSString *)aGame andLevel:(int)aLevel andScore:(Score *)aScore;

- (id)initWithPersonId:(int)aPersonId andDate:(NSString *)aDate andGame:(NSString *)aGame andLevel:(int)aLevel andScore:(Score*)aScore;

@end
