//
//  Invitation.h
//  Stimart
//
//  Created by Jiaji on 12/11/2015.
//  Copyright © 2015 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Invitation : NSObject

/* id of the invitation in local database */
@property int invitationId;

/* id of the person who received the invitation */
@property int personId;
/* server id of the person who received the invitation */
@property int personServerId;
/* server id of invitation */
@property int serverId;
/* name of the person who sent the invitation */
@property NSString *name;

@property NSDate *date;

@property NSString *message;

@property int state;

@property int type;

#define kInvitationStates @"received", @"read", @"ignored", @"accepted", @"rejected", nil

typedef NS_ENUM(NSInteger, InvitationState) {
    InvitationStateReceived = 1,
    InvitationStateRead = 2,
    InvitationStateIgnored = 3,
    InvitationStateAccepted = 4,
    InvitationStateRejected = 5,
};

typedef NS_ENUM(NSInteger, InvitationType) {
    InvitationTypeMoodMe = 1,
    InvitationTypeStat = 2,
};

- (id)initWithInvitationId:(int)anInvitationId andType:(int)aType andPersonId:(int)aPersonId andPersonServerId:(int)aPersonServerId andServerId:(int)aServerId andName:(NSString *)aName andDate:(NSDate *)aDate andMessage:(NSString *)aMessage andState:(int)aState;

- (id)initWithType:(int)aType andPersonId:(int)aPersonId andPersonServerId:(int)aPersonServerId andServerId:(int)aServerId andName:(NSString *)aName andDate:(NSDate *)aDate andMessage:(NSString *)aMessage andState:(int)aState;

- (id)initFromJsonString:(NSString *)stringJson;

+ (NSString*)getInvitationStateString:(int)aState;

@end