//
//  Header.h
//  Stimart
//
//  Created by Jiaji SUN on 05/05/2015.
//  Copyright (c) 2015 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Account : NSObject

@property NSString *email;
@property NSString *pseudo;
@property NSString *password;
@property NSString *name;
@property NSString *firstName;
@property NSString *contactName;
@property NSString *address;
@property NSString *institutionName;
@property NSString *institutionType;
@property NSString *city;
@property NSString *zip;
@property NSString *phoneNumber;
@property BOOL receiveNewsletter;

- (id)initWithEmail:(NSString *)aEmail andPseudo:(NSString *)aPseudo andPassword:(NSString *)aPassword;

- (id)initWithEmail:(NSString *)aEmail andPseudo:(NSString *)aPseudo andPassword:(NSString *)aPassword andReceiveNewsletter:(BOOL)aReceiveNewsletter;

- (id)initWithInstitutionType : (NSString *) anInstitutionType andName : (NSString *) aName andFirstName : (NSString *) aFirstName andEmail : (NSString *) anEmail andPassword : (NSString *) aPassword andInstitutionName : (NSString *) anInstitutionName andAddress : (NSString *) anAddress andCity : (NSString *) aCity andZip : (NSString *) aZip andPhoneNumber : (NSString *) aPhoneNumber;

- (id)initWithInstitutionType : (NSString *) anInstitutionType andContactName : (NSString *) aContactName andInstitutionName : (NSString *) anInstitutionName andEmail : (NSString *) anEmail andPassword : (NSString *) aPassword andAddress : (NSString *) anAddress andCity : (NSString *) aCity andZip : (NSString *) aZip andPhoneNumber : (NSString *) aPhoneNumber;

- (NSString *) toStringURL;
- (id)initFromJsonString:(NSString *)stringJson;

/* save the new account information to user defaults */
- (void)saveToUserDefaults;

/* get the current account pseudo */
+ (NSString *)getPseudo;

/* get the current account email */
+ (NSString *)getEmail;

+ (BOOL)isVisitMode;

+ (void)setVisitMode:(BOOL)isVisitMode;

/* a user who purchased but no account created */
+ (BOOL)isValidUserWithoutAccount;

+ (BOOL)isAccountExists;
/* a user who purchased application but didn't create an account (institution Stimart Vistors) */
+ (BOOL)isVisitorAccount;
+ (BOOL)isVisitorInstitution:(NSString *)institutionName;

+ (void)print;

@end
