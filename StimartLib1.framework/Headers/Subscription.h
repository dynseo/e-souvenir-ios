//
//  StimartSubscription.h
//  Stimart
//
//  Created by Jiaji SUN on 11/12/2014.
//  Copyright (c) 2014 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>

///* profile for a free trial */
//int const kSubscriptionTypeTrial;
///* profile for an individual */
//int const kSubscriptionTypeIndividual;
///* profile for an institution */
//int const kSubscriptionTypeInstitution;

typedef NS_ENUM(NSInteger, SubscriptionType) {
    /* profile for an individual */
    SubscriptionTypeIndividual     = 1,
    /* profile for an institution */
    SubscriptionTypeInstitution    = 2,
    
    SubscriptionTypeProfessional   = 3,
};

@interface Subscription : NSObject

/* type of the subscription */
@property int type;
/* subscription state */
@property NSString * subscriptionState;
/* number of months for the subscription */
@property int duration;
/* begin date of subscription, value convert to time zone 0 */
@property NSDate * dateBegin;
/* end date of subscription, value convert to time zone 0 */
@property NSDate * dateEnd;

/* name of institution */
@property NSString * institutionName;

// for Code BPCE
@property NSString * code;


- (id)initFromUserDefaults;
/* init subscription with Json response of "device identification service" */
- (id)initFromJsonStringIdentification:(NSString *)stringJson;
/* init subscription with Json response of "verify subscription service" */
- (id)initFromJsonStringSubscription:(NSString *)stringJson;
- (void)saveToUserDefaults;

/* static function, call without create a subscription object, find dateEnd from UserDefaults */
+ (BOOL)isSubscriptionValid;
/* return number of remaining days left for subscription */
+ (int)daysRemainingOnSubscription;
+ (int)getSubscriptionType;
+ (BOOL)isIndividualSubscription;
+ (BOOL)isInstitutionSubscription;
+ (BOOL)isProfessionalSubscription;
//+ (int)getDuration;
+ (NSDate *)getDateBegin;
+ (NSDate *)getDateEnd;
+ (NSDate *)getPreviousDateEnd;
+ (NSString *)getInstitutionName;
// get Code BPCE
+ (NSString *)getCode;

/* create subscription with number of months */
+ (void)subscriptionPurchasedWithDuration:(int)duration;
+ (BOOL)updateSubscriptionEndDateWithString:(NSString *)stringDateEnd;
/* when a purchase is rejected by the server, we restore the subscription end date */
+ (BOOL)rejectLastSubscriptionUpdate;
+ (void)print;

+ (void)setSubscriptionType:(int)subscriptionType;
+ (void) updateSubscriptionState:(NSString * )aSubsState;

@end
