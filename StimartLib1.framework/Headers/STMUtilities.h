//
//  GameUtilities.h
//  Stimart
//
//  Created by Jiaji SUN on 23/09/2014.
//  Copyright (c) 2014 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "AutoAdaptLabel.h"
#import "GamePersonInfo.h"
#import "STMButton.h"

/* sound effects */
extern int const SOUND_EFFECT_DEFAULT;
extern int const SOUND_EFFECT_GOOD_ANSWER;
extern int const SOUND_EFFECT_GOOD_ANSWER2;
extern int const SOUND_EFFECT_BAD_ANSWER;
extern int const SOUND_EFFECT_BAD_ANSWER2;
extern int const SOUND_EFFECT_TENNIS;
extern int const SOUND_EFFECT_EXPLOSION;
extern int const SOUND_EFFECT_WIN;
extern int const SOUND_EFFECT_HURRY_UP;

extern NSString * const USER_DEFAULTS_KEY_LAST_SYNCHRONIZATION_RESOURCES ;
extern NSString * const USER_DEFAULTS_KEY_LAST_SYNCHRONIZATION_DATE;
extern NSString * const USER_DEFAULTS_KEY_FIRST_LAUNCH_DATE;

@interface STMUtilities : NSObject

+ (NSString *)getAppVersion;

/* full name of application, ex: Stim'Art EDITH */
+ (NSString *)getAppName;

/* sub name of this version of application, ex: EHPAD, PAPY */
+ (NSString *)getAppSubName;

/* sub type name of this version of application, ex: EHPAD, PAPY */
+ (NSString *)getAppSubTypeName;

/* device unique identifier */
+ (NSString *)getUUID;

/* application language: ex: français for french */
+ (NSString *)getAppLanguageText;

/* application language: ex: fr for French */
+ (NSString *)getAppLanguage;

+ (NSString *)getLangAlter;

/* language of current system iOS */
+ (NSString *)getSystemLanguage;

/* version of current system iOS */
+ (NSString *)getSystemVersion;

/* name of the device given by user */
+ (NSString *)getDeviceName;

/* model of the device */
+ (NSString *)getDeviceModel;

/* added since build 9.1.2 */
+ (NSString *)getAppToken;

/* check internet connection */
+ (BOOL)isInternetConnected;

/* current resources version of application */
+ (NSString *)getResourcesVersion;

+ (NSString *)getAppSpecificName;

+ (NSString *)getAppExtension;

/* save resources version to UserDefaults */
+ (void)setResourcesVersion:(NSString *)resourcesVersion;

+ (NSArray *)getLocalizableStringArrayWithName:(NSString *)arrayName fromIndex:(int)indexFrom toIndex:(int)indexTo;

+ (NSArray *)getLocalizableStringArrayWithName: (NSString *) arrayName fromIndex:(int)indexFrom toIndex:(int)indexTo andAnotherValue:(int)anotherValue;

+ (NSArray *)getGameInstructionWithGameName:(NSString *)gameName;

+ (NSArray *)getGameInstructionWithGameName:(NSString *) gameName andLevel:(int) level andMode:(NSString *) mode;

+ (void)playSoundEffect:(int)soundEffectType;

+ (BOOL)getBoolValueWithKey:(NSString *)key;

+ (int)getIntValueWithKey:(NSString *) key;

+ (NSString *) getStringValueWithKey : (NSString *) key;


+ (NSMutableArray *)getRandomIntegersFrom:(int)min To:(int)max AndNumber:(int)num;

+ (NSMutableArray *)getRandomUniqueIntegersFrom:(int)min To:(int)max AndNumber:(int)num;

+ (NSMutableArray *)getRandomIntegersFrom: (int)min To: (int)max AndNumber: (int)num andDrop : (int) drop;

+ (NSMutableArray *)mixArray:(NSMutableArray *)array;

//+ (void)showUnBlockedAlertDialogWithMessage:(NSString *)aMessage ToView:(UIView *)view andDuration:(int)duration;
//
//+ (void)showUnBlockedAlertDialogWithMessageKey:(NSString *)key ToView:(UIView *)view andDuration:(int)duration;

+ (void)showAlertDialogWithMessage:(NSString *)aMessage andDuration:(NSTimeInterval)duration;

+ (void)showAlertDialogWithMessageKey:(NSString *)key andDuration:(NSTimeInterval)duration;

+ (void)showAlertMessage:(UIView *) view : (AutoAdaptLabel *) labelAlert : (NSString *)message dismissAfter:(NSTimeInterval)interval;

+ (BOOL)getBoolFromLocalizedStringWithKey:(NSString *)key;

+ (BOOL)getBoolValueWithKey:(NSString *)key;

+ (NSString *) getStringValueWithKey : (NSString *) key;

+ (void)playFadeOutAnimationForView:(UIView *)view andDuration:(NSTimeInterval)duration;

+ (void)playFadeInAnimationForView:(UIView *)view andDuration:(NSTimeInterval)duration;

+ (void)playBlinkAnimationForView:(UIView *)view andBlickDuration:(NSTimeInterval)duration andRepeatCount:(int)repeatCount;

+ (void)playRotationY_180_AnimationForView:(UIView *)view andDuration:(NSTimeInterval)duration;
+ (void)playRotationY_180_AnimationForViews:(NSArray *)views andDuration:(NSTimeInterval)duration;

+ (CGRect)getCurrentScreenBoundsDependOnOrientation;

+ (CGRect)getScreenBoundsDependOnOrientation:(UIInterfaceOrientation)toInterfaceOrientation;

+ (NSDate *)getLastSynchronizationDate;
+ (NSString *)getLastSynchronizationDateString;
+ (NSDate *)getFirstLaunchDate;
+ (NSString *)getFirstLaunchDateString;
+ (NSString *)getLastSynchronizationResDate;
+ (NSString *)getLastSynchronizationResDateString;

+ (BOOL)isSynchronizationNeeded;

+ (void)setFirstLaunchDateWithCurrentDate;

+ (BOOL)isAtLeastIOS7;
+ (BOOL)isAtLeastIOS8;
+ (BOOL)isAtLeastIOS9;
+ (BOOL)isAtLeastIOS10;

+ (BOOL)isNotificationsEnabled;

//+ (BOOL)isAuthorizedForGameWithId:(NSString *)gameId;

// for BPCE
+ (BOOL) authorizedSynchronizeBPCE;
+ (BOOL) isBPCE;

+ (void)replaceString :(NSMutableAttributedString *) aString withImage :(UIImage *) aImage andColor : (UIColor *) aColor;
+ (void)replaceString :(NSMutableAttributedString *) aString withImage :(UIImage *) aImage andColor : (UIColor *) aColor andPosx : (float) posX andPosY : (float) posY andWidth : (float) aWidth andHeight : (float) aHeight;

+ (void) showResultTwoPlayer : (UIView *) aView : (int) scoreP1 : (int) scoreP2 : (BOOL) isLandscape;
+ (void) setMoodDate;
+ (NSDate *) getLastMoodDate;

+(BOOL)iphoneDevice;
+(BOOL)iPhone6PlusDevice;
+(BOOL)iPhone6Device;
+(BOOL)iPhone5Device;
+(BOOL)iPadDevice;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (NSString *) loadContentFromFile : (NSString *) filePath;
+ (NSString *)removeIdsFromIdList : (NSString *) idList : (NSString *) idsToRemove;
+ (void) deleteFileWithPath :(NSString *) aPath;

+ (NSString *) getGameScoreTypeWithGameName : (NSString *) aGameName;
+ (NSString *) timeToString : (int) time;
+ (void) positionListButtons : (NSMutableArray *) listButtons inAView : (UIView *) view;

+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (UIColor *)colorFromStringKey:(NSString *)key;
+ (UIImage *)imageWithColor:(UIColor *)color;
@end
