//
//  StimartURLFactory.h
//  Stimart
//
//  Created by Jiaji on 12/12/2014.
//  Copyright (c) 2014 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Result.h"
#import "Account.h"
#import "Institution.h"
#import "Invitation.h"

@interface STMURLFactory : NSObject

extern NSString * BASE_URL;
extern NSString * PURCHASE_VALIDATION_URL;
extern NSString * BASE_RESOURCES_URL;
extern NSString * DEFAULT_BASE_URL_RES;

extern NSString * const VAL_CHECK_UPDATE;
extern NSString * const VAL_DOWNLOAD_UPDATE;
extern NSString * const VAL_CHECK_RES_UPDATE;
extern NSString * const VAL_DOWNLOAD_RES_UPDATE;
extern NSString * const VAL_VERIFY_DEVICE;
extern NSString * const VAL_CREATE_RESULT;
extern NSString * const VAL_ADD_RESULT;
extern NSString * const VAL_ADD_PERSON;
extern NSString * const VAL_SYNCHRO_PERSONS;
extern NSString * const VAL_SYNCHRO_END;
extern NSString * const VAL_CREATE_ACCOUNT;
extern NSString * const VAL_CREATE_INSTITUTION;
extern NSString * const VAL_LOG_IN;
extern NSString * const VAL_ATTACH_INSTITUTION;
extern NSString * const VAL_LOG_TO_INSTITUTION;
extern NSString * const VAL_RESTORE_PURCHASE;
extern NSString * const VAL_ADD_NOTIFICATION_DEVICE;
extern NSString * const VAL_DELETE_NOTIFICATION_DEVICE;
extern NSString * const VAL_GET_RESOURCES_FOR_APP;
extern NSString * const VAL_SYNCHRO_EXTRA_DATA;
extern NSString * const VAL_SET_LINK_SERVICE_URL;
extern NSString * const VAL_TRUE;
extern NSString * const VAL_FALSE;
extern NSString * const VAL_APPLE;
extern NSString * const PARAM_APP;
extern NSString * const PARAM_SERVICE;
extern NSString * const PARAM_DEVICE_TYPE;
extern NSString * const PARAM_DEVICE_FORMAT;
extern NSString * const PARAM_TOKEN;
extern NSString * const PARAM_APP_TOKEN;
extern NSString * const PARAM_ID;
extern NSString * const PARAM_PSEUDO;
extern NSString * const PARAM_NAME;
extern NSString * const PARAM_FIRSTNAME;
extern NSString * const PARAM_EMAIL;
extern NSString * const PARAM_PASSWORD ;
extern NSString * const PARAM_CITY;
extern NSString * const PARAM_CODE;
extern NSString * const PARAM_SERIAL_NUMBER;
extern NSString * const PARAM_APP_LANG;
extern NSString * const PARAM_APP_LANG_TYPE ;
extern NSString * const PARAM_APP_VERSION;
extern NSString * const PARAM_CURRENT_VERSION;
extern NSString * const PARAM_TARGET_VERSION;
extern NSString * const PARAM_OS_VERSION ;
extern NSString * const PARAM_SEX ;
extern NSString * const PARAM_BIRTHDATE;
extern NSString * const PARAM_EXIST_LOCAL;
extern NSString * const PARAM_SERVERID;
extern NSString * const PARAM_GAME ;
extern NSString * const PARAM_DATE ;
extern NSString * const PARAM_SYNCHRO_DATE;
extern NSString * const PARAM_DURATION;
extern NSString * const PARAM_LEVEL;
extern NSString * const PARAM_SCORE1;
extern NSString * const PARAM_SCORE2;
extern NSString * const PARAM_SCORE3;
extern NSString * const PARAM_SCORE4;
extern NSString * const PARAM_SCORE5;
extern NSString * const PARAM_SCORE6;
extern NSString * const PARAM_CONFLICT;
extern NSString * const PARAM_FORMAT_OUTPUT_JSON;
extern NSString * const PARAM_INTERRUPTED;
extern NSString * const PARAM_TYPE_RESOURCES;
extern NSString * const PARAM_MODE;
extern NSString * const PARAM_APP_TYPE;
extern NSString * const PARAM_APP_EXTENSION;
extern NSString * const PARAM_APP_SPEC;
extern NSString * const PARAM_FORCE_DOWNLOAD_PHOTOS_PAINTINGS;
extern NSString * const PARAM_FORCE_TO_ROLE;
extern NSString * const PARAM_INSTITUTION_TYPE;
extern NSString * const PARAM_INSTITUTION_CONTACT_EMAIL;
extern NSString * const PARAM_INSTITUTION_CONTACT_NAME;
extern NSString * const PARAM_INSTITUTION_NAME;
extern NSString * const PARAM_INSTITUTION_ADDRESS;
extern NSString * const PARAM_INSTITUTION_ZIPCODE;
extern NSString * const PARAM_INSTITUTION_CITY;
extern NSString * const PARAM_INSTITUTION_TEL;
extern NSString * const PARAM_INSTITUTION_EMAIL_LOGIN;
extern NSString * const PARAM_INSTITUTION_PASSWORD;;
extern NSString * const PARAM_BRAND;
extern NSString * const PARAM_MODEL;

extern NSString * const VAL_ADD_PERSON_TRUE;
extern NSString * const VAL_ADD_PERSON_FALSE;
extern NSString * const VAL_DEVICE_TYPE_IOS;
extern NSString * const PARAM_USER_ID;
extern NSString * const PARAM_OBJET_MESSAGE;
extern NSString * const PARAM_MESSAGE;
extern NSString * const PARAM_PERSON_ID ;
extern NSString * const PARAM_FROM_USER_TO_PERSON;
extern NSString * const PARAM_IDMESSAGE;
extern NSString * const PARAM_CHECKED ;
extern NSString * const PARAM_APP_STATUS;

extern NSString * const VAL_SYNCHRO_MESSAGES_AND_USERS;
extern NSString * const VAL_DOWNLOAD_IMAGE ;
extern NSString * const VAL_ADD_MESSAGE ;
extern NSString * const VAL_UPDATE_MESSAGE;
extern NSString * const VAL_CHECK_OR_CONSUME_CODE ;
extern NSString * const PARAM_CONSUME_TRUE;
extern NSString * const PARAM_CONSUME_RENEW;
extern NSString * const VAL_RETRIEVE_CODE ;
extern NSString * const PARAM_NUM_CLIENT;

extern NSString * const PARAM_NEWS_LETTER;




/* identify and check subscription of a device */
+ (NSString *)getURLVerifyDevice;

/* check subscription of a device */
+ (NSString *)getURLVerifySubscription;

/* check there is an update for application resources */
+ (NSString *)getURLCheckResourcesUpdate;

/* download application resources update (may need to be modified to make resources version different from app version) */
+ (NSString *)getURLDownloadResourcesUpdate;

/* param "existLocal", NO  -> if person exist on the server, return his serverId
 YES -> if person exist on the server, return conflictId
 */
+ (NSString *)getURLAddPerson:(Person *)person existLocal:(BOOL)existLocal;

/* add result to server database */
+ (NSString *)getURLAddResult:(Result *)result;

/* synchronize with server to get persons */
+ (NSString *)getURLSynchroPersons;

/* modify person */
+ (NSString *)getURLUpdatePerson : (Person *) aPerson : (BOOL) isDelete;

/* notfiy synchronization end */
+ (NSString *)getURLSynchroEnd : (BOOL) dataType;

+ (NSString *)getURLCreateAccount:(Account *)account;

+ (NSString *)getURLCreateInstitution:(Institution *)institution;

+ (NSString *)getURLLogInWithEmail:(NSString *)email andPassword:(NSString *)password;

/* attach to an establishment */
+ (NSString *)getURLAttachToEstablishmentWithEmail:(NSString *)email andName:(NSString *)name andCity:(NSString *)city andMessages:(NSString *)messages;

+ (NSString *)getURLNotifyServerPurchase;
+ (NSString *)getURLConnectToInstitutionWithEmail:(NSString *)email andPassword:(NSString *)password;
+ (NSString *)getURLRestorePurchaseWithEmail:(NSString *)email andPassword:(NSString *)password;

#pragma added since build 9.1.2

+ (NSString *)getURLAddNotificationDeviceWithToken:(NSString *)aToken;
+ (NSString *)getURLDeleteNotificationDeviceWithToken:(NSString *)aToken;
//+ (NSString *)getURLCreatAppUserWithToken:(NSString *)notifToken;
+ (NSString *)getURLSynchroExtraData;
+ (NSString *)getURLSendInvitationStateChange:(Invitation *)anInvitation;
// for BPCE
+ (NSString *)getURLCheckOrConsumeCode:(NSString *) aCode :(NSString *) aPassword;
+ (NSString *)getURLRenewSubcription:(NSString *)aCode;
+ (NSString *)getURLRetrieveCode:(NSString *) email andNumClient: (NSString *) numClient;

// get resources from server
+ (NSString *)getURLResourcesForApp :(BOOL) isVisitMode :(BOOL) downloadPhotosPaintings;

@end
