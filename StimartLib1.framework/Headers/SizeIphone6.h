//
//  SizeIphone6.h
//  Stimart
//
//  Created by Dynseo on 10/03/2017.
//  Copyright © 2017 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DimensionAdapter.h"

@interface SizeIphone6 : DimensionAdapter
-(id) initSizeIphone6;
@end
