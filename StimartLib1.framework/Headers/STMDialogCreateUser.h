//
//  STMDialogCreateUser.h
//  StimartLib
//
//  Created by Dynseo on 31/05/2017.
//  Copyright © 2017 dynseo. All rights reserved.
//

#import <StimartLib/StimartLib.h>

@interface STMDialogCreateUser : STMDialogView

@property (nonatomic) UITextField *firstNameTextField;
@property (nonatomic) UITextField *nameTextField;
@property (nonatomic) UITextField *birthdateTextField;
@property (nonatomic) UISegmentedControl * sexSegmentedControl;
@property (nonatomic) STMButton * buttonSup;

- (id)initWithStyle:(int)aDialogStyle;
@end
