//
//  STMDialogIdentification.h
//  Stimart
//
//  Created by Jiaji SUN on 06/05/2015.
//  Copyright (c) 2015 Dynseo. All rights reserved.
//

#import "STMDialogView.h"
#import "Account.h"

@protocol STMDialogIdentificationDelegate;

@interface STMDialogIdentification : STMDialogView

@property (nonatomic) id<STMDialogIdentificationDelegate> aDelegate;
@property (nonatomic) UITextField *textField1;
@property (nonatomic) UITextField *textField2;
@property (nonatomic) UITextField *textField3;
@property (nonatomic) UITextField *textField4;
/* error labels */
@property (nonatomic) UILabel *labelError1;
@property (nonatomic) UILabel *labelError2;
@property (nonatomic) UILabel *labelError3;
@property (nonatomic) UILabel *labelError4;
@property (nonatomic) UILabel *labelError5;

@property (nonatomic) UITextView *textViewLicenseAgreement;
@property (nonatomic) UISwitch *switchLicenseAgreement;

/* this dialog is the launch dialog for visit mode
 it is shown also when click on the settings -> subscribe */
@property (nonatomic) BOOL isLaunchDialog;

- (id)initWithStyle:(int)aDialogStyle;
- (void)showErrorPseudoDuplicate;
- (void)showErrorEmailDuplicate;
- (void)showErrorLoginNotFound;
- (void)showErrorLoginPasswordIncorrect;
- (void)showErrorSerialNumberDuplicate;

- (void)changeToPageChooseTypeAccountAction;
- (void)changeToPageCreateAccountAction;
- (void)changeToPageConnectAccountAction;
- (void)changeToPageAskForAttachmentAction;
- (void)changeToPageMainAction;
- (void)changeToPageMain;
- (void)changeToPageAccountTypeDescriptionAction:(UIButton *)sender;
//BPCE
- (void)changeToPageRetrieveCode;
- (void) changeToPageEnterCode : (BOOL) needPassWord;

@end


@protocol STMDialogIdentificationDelegate <NSObject>
@optional

- (void)createAccount:(Account *)account;
- (void)attachToEstablishmentWithEmail:(NSString *)email andName:(NSString *)name andCity:(NSString *)city andMessages:(NSString *)messages;
- (void)restorePurchaseWithEmailOrPseudo:(NSString *)emailOrPseudo andPassword:(NSString *)password;
- (void)restorePurchaseOrLogToInsitutionWithEmailOrPseudo:(NSString *)emailOrPseudo andPassword:(NSString *)password;
- (void)retrieveCodeWithEmail:(NSString *) email andNUmClient:(NSString *) numClient;
- (void)checkOrConsumeCodeWithCode : (NSString *) aCode andPassword : (NSString *) aPassword;
/* begin a visit */
- (void)beginVisitMode;
- (void)showProductActionFor:(int)subscriptionType;



@end
