//
//  STMDialogCreateInstitution.h
//  Stimart
//
//  Created by Jiaji on 21/09/2015.
//  Copyright © 2015 Dynseo. All rights reserved.
//

#import "STMDialogView.h"
#import "ActionSheetStringPicker.h"
#import "Account.h"

@protocol STMDialogCreateInstitutionDelegate;

@interface STMDialogCreateInstitution : STMDialogView

//@property (nonatomic) id<STMDialogCreateInstitutionDelegate> aDelegate;
@property (nonatomic) UILabel *labelPrincipalInfo;
@property (nonatomic) UILabel *labelContact;
@property (nonatomic) UILabel *labelLogin;

@property (nonatomic) UIScrollView *scrollViewCreateInstitution;

@property (nonatomic) UITextField *textFieldInstitutionName;
@property (nonatomic) UITextField *textFieldInstitutionType;
@property (nonatomic) UITextField *textFieldInstitutionGroup;
@property (nonatomic) UITextField *textFieldInstitutionEmailLogin;
@property (nonatomic) UITextField *textFieldInstitutionPassword;
@property (nonatomic) UITextField *textFieldInstitutionConfirmPassword;
@property (nonatomic) UITextField *textFieldInstitutionContactName;
@property (nonatomic) UITextField *textFieldInstitutionEmail;
@property (nonatomic) UITextField *textFieldInstitutionTel;
@property (nonatomic) UITextField *textFieldInstitutionAddress;
@property (nonatomic) UITextField *textFieldInstitutionZIPCode;
@property (nonatomic) UITextField *textFieldInstitutionCity;
@property (nonatomic) UITextView *textViewLicenseAgreement;
@property (nonatomic) UISwitch *switchLicenseAgreement;

@property (nonatomic) ActionSheetStringPicker *pickerInstitutionType;

- (id)initWithStyle:(int)aDialogStyle;
- (void)showErrorDuplicateInstitutionName;
- (void)showErrorDuplicateEmail;

@end

//@protocol STMDialogCreateInstitutionDelegate <NSObject>
//@optional
//
//- (void)createAccountInstitution:(Account *) account;

//@end
