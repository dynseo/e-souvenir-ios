//
//  SizeIpad.h
//  Stimart
//
//  Created by Dynseo on 10/03/2017.
//  Copyright © 2017 Dynseo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DimensionAdapter.h"

@interface SizeIpad : DimensionAdapter

-(id) initSizeIpad;
@end
